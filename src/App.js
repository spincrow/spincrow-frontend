import React from 'react';
import './App.scss';
import {
	BrowserRouter as Router, useHistory
} from 'react-router-dom';
import Routes from "./routes"
// import { onError } from "apollo-link-error";
// Redux
import {Provider} from "react-redux"
import {configureStoreDev} from './store';
import { PersistGate } from "redux-persist/integration/react";
import initialStore from "./store/Reducers/inital-state";
import { ToastProvider, useToasts } from 'react-toast-notifications'
// import Segment from "react-segment"
// analysis and tracking
import analytics from "./modules/analytics"

// if(SEGMENTKEY){
//   Segment.default.load(SEGMENTKEY)
// }

// SEGMENT INITIALIZED
const App = () => {

  analytics.page()
  
  let storeValues = configureStoreDev(initialStore);

  return (
      <Provider store={storeValues.store}>
        <PersistGate loading={null} persistor={storeValues.persistor}>
          <ToastProvider>
              <Router>
                  <Routes />
              </Router>
          </ToastProvider>
        </PersistGate>
      </Provider>
  );
}

export default App;
