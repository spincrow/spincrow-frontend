//  setting up action types
import keymirror from "keymirror";
export default keymirror({
  // user and Auth related
  SET_CURRENT_USER: null,
  GET_USER_DETAILS: null,
  LOGGED_IN: null,
  LOGGED_OUT: null,
  SET_REDIRECT_URL: null,
  USER_DETAILS_UPDATE: null,

  // CONTRACTS related
  CONTRACT_UPDATE: null,
  GET_CONTRACTS: null,

  // CONTRACTS related
  UPDATE_CARDS: null,
  DELETE_CARD: null,
  GET_CARDS: null,

  // CONTRACTS related
  UPDATE_ACCOUNTS: null,
  GET_ACCOUNTS: null,
  DELETE_ACCOUNT: null,

  MILESTONE_UPDATE: null,
  WALLET_UPDATE: null,

  // SECURITY
  SET_SECURITY_PERSONAL: null,
  SET_SECURITY_SECURITY: null,
  SET_SECURITY_PAYMENTS: null,

  
});
