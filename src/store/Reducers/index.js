import reduceReducers from "../../utils/reduce-reducers";
import initialState from './inital-state';
import actionType from '../action-type';
import _ from 'lodash';
import { insertItem, removeItem } from '../../utils/helpers';
// import { combineReducers } from "redux";

const INITIAL_STATE = initialState;


let authReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case actionType.LOGGED_IN:
			return {
				...state,
				isAuthenticated: true
			};
			break;
		case actionType.LOGGED_OUT:
			return {
				...INITIAL_STATE,
				isAuthenticated: false,
			}
			break;
		case actionType.USER_DETAILS_UPDATE:
			return {
				...state,
				currentUser: action.data
			}
			break;
		case actionType.SET_REDIRECT_URL:
			return {
				...state,
				redirectUrl: action.payload
			}
			break;
		default: return state;
	}
};



let contractReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {	
		case actionType.CONTRACT_UPDATE:
			return {
				...state,
				newContract: {
					...state.newContract,
					...action.data
				}
			}
			break;
		case actionType.GET_CONTRACTS:
			return {
				...state,
				contracts: action.data
			}
			break;
		default:
			return state
			break;
	}
}

let cardReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {	
		case actionType.UPDATE_CARDS:
			return {
				...state,
				cards: [...state.cards, action.data]
			}
		case actionType.GET_CARDS:
			return {
				...state,
				cards: action.data
			}
		case actionType.DELETE_CARD:
			let cards = state.cards.filter(item => item.id != action.data.id)
			return {
				...state,
				cards: cards
			}
		default:
			return state
	}
}

let bankaccountReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {	
		case actionType.UPDATE_ACCOUNTS:
			return {
				...state,
				bankaccounts: [...state.bankaccounts, action.data]
			}
		case actionType.GET_ACCOUNTS:
			return {
				...state,
				bankaccounts: action.data
			}
		case actionType.DELETE_ACCOUNT:
			let bankaccounts = state.bankaccounts.filter(item => item.id != action.data.id)
			return {
				...state,
				bankaccounts: bankaccounts
			}			
		default:
			return state
	}
}

let walletReducer = (state = INITIAL_STATE, action) => {
	
	switch (action.type) {	
		case actionType.WALLET_UPDATE:
			return {
				...state,
				wallet: action.data
			}
		default:
			return state
			break;
	}
}

let settingsReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {	
		case actionType.SET_SECURITY_SECURITY:
			return {
				...state,
				settings: {
					...state.settings,
					...action.data
				}
			}
			break;
		default:
			return state
			break;
	}
}

const rootReducer = reduceReducers(
	authReducer,
	contractReducer,
	settingsReducer,
	cardReducer,
	bankaccountReducer,
	walletReducer
);


export default rootReducer;
