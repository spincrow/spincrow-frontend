export default {
	currentUser: null,
	isAuthenticated: false,
	redirectUrl: "/home",
	newContract: null,
	contracts: [],
	cards: [],
	bankaccounts: [],
	wallet: null,
	// SETTINGS.
	settings: {
		
	}
};
