import actionType from '../action-type';
import cookies from "js-cookie"


/**
 * Login for the application
 * @param {*} params 
 */
export const login = (params) => (dispatch, getstate) => new Promise(async (resolve, reject)=>{
	let userData = params.user
	await dispatch({
		type: actionType.USER_DETAILS_UPDATE,
		data: userData
	})
	await dispatch({
		type: actionType.LOGGED_IN
	})
	resolve()
}).catch(err=>{
	throw err
})

export const updateUser = (params) => (dispatch, getstate) => new Promise(async (resolve, reject)=>{
	await dispatch({
		type: actionType.USER_DETAILS_UPDATE,
		data: params
	})
	resolve()
}).catch(err=>{
	throw err
})


/**
 * Logout of the application
 */
export const logout = () => (dispatch, getstate) => new Promise( async (resolve, reject) => {
	dispatch({
		type: actionType.LOGGED_OUT
	})
	cookies.remove("uat")
	window.location.href = "/login"
	resolve();
}).catch(err=>{
	throw err
});


/**
 * Set the redirect path of the app
 * @param {*} params 
 */
export const setRedirectUrl = (params) =>(dispatch, getState) => {  
	dispatch({
		type: actionType.SET_REDIRECT_URL,
		payload: params
	})
}


/**
 * Automatically login application
 */
export const autoLogin = () => (dispatch, getState) => new Promise((resolve,reject)=>{
	// get the cookie uat and work based off it.
	const token = cookies.get('uat');
	if(token){
		dispatch({
			type: actionType.LOGGED_IN
		})
		// this.
		// window.href = "/home"
	}else{
		dispatch({
			type: actionType.LOGGED_OUT
		})
	}
}).catch(err=>{
	
	throw err
});

/**
 * Get a the User data
 * @param {*} dispatch 
 */
export const getUserData = (dispatch) => {

}



/**
 * Signup action
 * @param {*} params 
 */

 export const signup = (params) => (dispatch, getState) => new Promise(async (resolve, reject)=> {

});


export const updateUserProfile = (params) => (dispatch, getState) => new Promise(async (resolve, reject) => {
	let currentUser = getState().currentUser;
	
	try {
		dispatch({
			type: actionType.USER_DETAILS_UPDATE, 
			data: {
				...currentUser,
				...params
			}
		})
		resolve("success")
	}catch(err){
		reject(err)
	}
})
