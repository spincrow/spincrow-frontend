import actionType from "../action-type"

export const getCards = ({cards}) => (dispatch, getstate) => new Promise( async (resolve, reject) => {
    
	await dispatch({
        type: actionType.GET_CARDS,
        data: cards
	})
	resolve();
}).catch(err=>{
	throw err
});

export const updateCards = ({card}) => (dispatch, getstate) => new Promise( async (resolve, reject) => {
    
	await dispatch({
        type: actionType.UPDATE_CARDS,
        data: card
	})
	resolve();
}).catch(err=>{
	throw err
});

export const deleteCard = ({card}) => (dispatch, getstate) => new Promise( async (resolve, reject) => {
    
	await dispatch({
        type: actionType.DELETE_CARD,
        data: card
	})
	resolve();
}).catch(err=>{
	throw err
});
