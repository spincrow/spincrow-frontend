import actionType from '../action-type';
import cookies from "js-cookie"

 export const updateSettingsSecurity = (params) => (dispatch, getState) => new Promise(async (resolve, reject)=> {
    dispatch({
        type: actionType.SET_SECURITY_SECURITY,
        data: params
    })
    resolve();
});

export const walletUpdate  = (params) => (dispatch, getState) => new Promise(async (resolve, reject) => {
    
    dispatch({
        type: actionType.WALLET_UPDATE,
        data: params
    })
    resolve()
})