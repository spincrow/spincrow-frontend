import actionType from '../action-type';
import cookies from "js-cookie"
import update from "react-addons-update"

export const updateContract = (params) => (dispatch, getState) => new Promise(async (resolve, reject)=> {
    dispatch({
        type: actionType.CONTRACT_UPDATE,
        data: params
    })
    resolve();
});

export const getContracts = (contracts) => (dispatch, getState) => new Promise(async (resolve, reject)=> {
    dispatch({
        type: actionType.GET_CONTRACTS,
        data: contracts
    })
    resolve();
});

export const updateMilestones = (params) => (dispatch, getState)=> new Promise(async (resolve, reject) => {
    const updatedSate = update()
    dispatch({
        type: actionType.MILESTONE_UPDATE,
        data: {
            contract: params.contract_id,
            milestone: params.milestone_id,
            ...params
        }
    })
})