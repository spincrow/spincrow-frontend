import React from "react"
import ContentLoader from "react-content-loader"


const CardLoaderElement = () => {
    return <ContentLoader viewBox="0 0 380 70">
    <rect x="0" y="17" rx="4" ry="4" width="300" height="150" />
</ContentLoader>
        
}

export default CardLoaderElement