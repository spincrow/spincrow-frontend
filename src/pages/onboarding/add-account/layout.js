import React, {Component, useState} from "react"
import {Link, useHistory} from "react-router-dom"
import {useMutation, useQuery} from "@apollo/react-hooks"
import cookies from "js-cookie"
import Sidebar from "../../../components/sidebar"
import {withToastManager} from "react-toast-notifications"
import {ADD_BANK_ACCOUNT, BANK_CODES} from "../../dashboard/settings/utils/queries"
import CardLoading from "../../../components/card-loading"


const Layout = (props) => {

    const [addAccount, {loading: accountsLoading}] = useMutation(props.addAccountMutation);
    let history = useHistory()
    let {toastManager} = props

    const [state, updateState ] = useState({
        agencies: [],
        passwordVisible: false,
        params: {},
        links: [
            {
                anchor: "/onboarding/addbank",
                name: "Add your bank account",
                active: true
            },
            {
                anchor: "/onboarding/addcard",
                name: "Add your card"
            },
            {
                anchor: "/onboarding/create-contract",
                name: "Create your own contract"
            }
        ],
        loading: false
    })

    const {loading, data } = useQuery(BANK_CODES, {onCompleted: async resp => {
        if(resp.error) {
            toastManager.add("Can't retrive babk details", {
                appearance: "error"
            })
        }
    }});

    const updateParams = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value == "" ? null : e.target.value
            }
        })
    }

    const updateParamsSelectBank = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                bank_name : e.target.options[e.target.selectedIndex].dataset.bankName,
                bank_code : e.target.value
            }
        })
    }

    const addAccountAction = async () => {
        // const { toastManager } = props
        try {
            let resp = await addAccount({ variables: { ...state.params } });
            if(resp){
                toastManager.add("Added bank account", {
                    appearance: 'success',
                    autoDismiss: true
                })
                history.push("/onboarding/addcard")
            }
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true
            // })
            
        }
    }

    const onChange = (e) => {
        
        
        let target = e.target
        updateState({
            ...state,
            params: {
                ...state.params,
                [target.name] : target.value
            }
        })
    }

    const skip =() =>{
        history.push("/onboarding/addcard")
    }
    
    return(
        <div className="row full-height bg-body-alt">
            <Sidebar image={require('../../../assets/img/header-phone.svg')} links={state.links} />
            <div className="col-md-8 col-12 h-100">
                <div className="container-fluid pt-5">
                    <div className="row mt-5 justify-content-center">
                    <div className="px-md-auto col-md-7 col-12 mt-5">
                            <div className="row mt-5">
                                <div className="col-12">
                                    <h1 className="display-4">Add your account number</h1>
                                    <p className="text-secondary">Enter your bank account details.</p>
                                </div>
                            </div>
                    </div>
                    <div className="px-md-auto col-md-7 col-12">
                            {
                                loading ?
                                <CardLoading/>
                                : 
                                    <>
                                        {
                                            data? 
                                                <>
                                                    <div className="row">
                                                        <div className="col form-group">
                                                            <label>Account number</label>
                                                            <input className="form-control" type="text" name="account_number" onChange={updateParams}/>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col form-group">
                                                            <label>Account name</label>
                                                            <input className="form-control" type="text" name="account_name" onChange={updateParams}/>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col form-group">
                                                            <label>Bank Name</label>
                                                            <select className="form-control" type="text" name="bank_name" onChange={updateParamsSelectBank}>
                                                                <option> -- SELECT BANK -- </option>
                                                                { data.bankcodes.map((item, index) => <option key={index} data-bank-name={item.bankname} value={item.bankcode}>{item.bankname}</option>) }
                                                            </select>
                                                        </div>
                                                    </div>
                                                </>
                                                : 
                                                <p> Couldn't retrieve bank details, possibly network issues</p>
                                        }
                                    </>
                            }
                            <div className="row mt-4 mb-4">
                                    <div className="col-auto">
                                        <button className="btn btn-light" onClick={skip}>Skip</button>
                                    </div>
                                    <div className="col">
                                        
                                    </div>
                                    <div className="col-auto">
                                        <button className="btn btn-primary" onClick={addAccountAction}>
                                            {
                                                accountsLoading ? 
                                                <div className="spinner-grow" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                                : 
                                                <span>Verify</span>
                                            }
                                        </button>
                                    </div>
                            </div>
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withToastManager(Layout);