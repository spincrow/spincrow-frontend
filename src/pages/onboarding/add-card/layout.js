import React, {Component, useState} from "react"
import {Link, useHistory} from "react-router-dom"
import {useMutation} from "@apollo/react-hooks"
import {withRouter} from "react-router-dom"
import cookies from "js-cookie"
import {ADD_CARD} from "../../dashboard/settings/utils/queries"
import Sidebar from "../../../components/sidebar"
import {withToastManager} from "react-toast-notifications"
import {generateUUID, integrityValue} from "../../../utils/helpers"

// FLuterwave details
const FL_PUB = process.env.REACT_APP_RAVE_PUBLIC_KEY
const FL_SEC = process.env.REACT_APP_RAVE_SECRET_KEY
const FL_ENC = process.env.REACT_APP_RAVE_ENCRYPT_KEY



const Layout = (props) => {

    const [state, updateState ] = useState({
        agencies: [],
        passwordVisible: false,
        params: {},
        links: [
            {
                anchor: "/onboarding/addbank",
                name: "Add your bank account",
            },
            {
                anchor: "/onboarding/addcard",
                active: true,
                name: "Add your card"
            },
            {
                anchor: "/onboarding/create-contract",
                name: "Create your own contract"
            }
        ],
        loading: false
    })

    const updateParams = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value == "" ? null : e.target.value
            }
        })
    }

    const {toastManager, currentUser} = props

    const [addCard, {loading: addCardLoading}] = useMutation(ADD_CARD, {onCompleted: (resp) => {
        
    }})

    let history = useHistory()

    const fundAccount = async () => {
        // add all Milestones.
        // var PBFKey = "FLWPUBK-aa82cac8ee08f5bb206f937db274081a-X";
        // if( !state.params.card_number || !state.params.expirymonth || !state.params.expiryyear || !state.params.cvv){
        //     toastManager.add("Fill in your card details", {
        //         appearance: 'error',
        //         autoDismiss: true,
                
        //     })
        //     return;
        // }
        let transactionCode = generateUUID();
        const integrity_hash = integrityValue({
            PBFPubKey: FL_PUB,
            customer_email: currentUser.email,
            customer_firstname: currentUser.first_name,
            customer_lastname: currentUser.last_name,
            custom_description: "Adding your card",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Spincrow Contract card",
            amount: 100,
            customer_phone: currentUser.phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
        });
        // 
        window.getpaidSetup({
            PBFPubKey: FL_PUB,
            customer_email: currentUser.email,
            customer_firstname: currentUser.first_name,
            customer_lastname: currentUser.last_name,
            custom_description: "Adding your card",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Spincrow Contract card",
            amount: 100,
            customer_phone: currentUser.phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
            integrity_hash: integrity_hash,
            onclose: function() {},
            callback: async function(response) {
                var flw_ref = response.tx.flwRef; // collect flwRef returned and pass to a 					server page to complete status check.
                
                if (
                response.tx.chargeResponseCode == "00" ||
                response.tx.chargeResponseCode == "0"
                ) {
                // redirect to a success page, payment was successfully done, initiate call to update the current contract base and move value to 
                    
                    try{
                        await addCard({
                            variables: {
                                txRef: transactionCode,
                                // last_number: state.params.card_number.substr(state.params.card_number.length - 4),
                                // expirymonth: state.params.expirymonth,
                                // expiryyear: state.params.expiryyear,
                                // card_name: `${currentUser.first_name} ${currentUser.last_name}`,
                                // cvv: state.params.cvv
                            }
                        })
                        toastManager.add("Succcessfully added your card 🎉 ", {
                            appearance: 'success',
                            autoDismiss: true,
                            
                        })
                        history.push("/onboarding/create-contract")
                    }catch(err){
                        toastManager.add("Something went wrong 🎉 ", {
                            appearance: 'error',
                            autoDismiss: true,
                            
                        })
                    }
                } else {
                // redirect to a failure page.
                }
            }
        });
    }

    const onChange = (e) => {
        
        
        let target = e.target
        updateState({
            ...state,
            params: {
                ...state.params,
                [target.name] : target.value
            }
        })
    }

    const skip =() =>{
        history.push("/onboarding/create-contract")
    }
    
    return(
        <div className="row bg-body-alt">
            <Sidebar image={require('../../../assets/img/header-phone.svg')} links={state.links} />
            <div className="col-md-8 col-12">
                <div className="container-fluid pt-5">
                    <div className="row mt-5 justify-content-center">
                    <div className="px-md-auto col-md-7 col-12 mt-5">
                            <div className="row mt-5">
                                <div className="col-12">
                                    <h1 className="display-4">Add your card</h1>
                                    <p className="text-secondary">Enter your bank account details.</p>
                                </div>
                            </div>
                    </div>
                    <div className="px-md-auto col-md-7 col-12">
                            <div className="row mt-4 mb-4">
                                    <div className="col-12">
                                        <div className="row">
                                            <div className="col form-group">
                                                <h3>To add and verify your card ₦ 100 will be charged and saved into your wallet </h3>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div className="row mt-4 mb-4">
                                    <div className="col-auto">
                                        <button className="btn btn-light" onClick={skip}>Skip</button>
                                    </div>
                                    <div className="col">
                                        
                                    </div>
                                    <div className="col-auto">
                                        <button className="btn btn-primary" onClick={fundAccount} disabled={addCardLoading}>
                                            {
                                                addCardLoading? <div className="spinner-grow" role="status">
                                                <span className="sr-only">Loading...</span>
                                            </div>: <span>Proceed</span>
                                            }
                                        </button>
                                    </div>
                            </div>
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Layout;