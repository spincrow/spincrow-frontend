import React, {Component, useState} from "react"
import {Link, useHistory, withRouter} from "react-router-dom"
import {useMutation} from "@apollo/react-hooks"
import cookies from "js-cookie"
import Sidebar from "../../../components/sidebar"
import Index from "./components"


const Layout = (props) => {

    let history = useHistory()

    const links = [
        {
            anchor: "/onboarding/addbank",
            name: "Add your bank account",
        },
        {
            anchor: "/onboarding/addcard",
            name: "Add your card"
        },
        {
            anchor: "/onboarding/create-contract",
            name: "Create your own contract",
            active: (history.location.pathname == "/onboarding/create-contract")
        },
        {
            anchor: "/onboarding/create-contract/pay-structure",
            name: "Set your payment structure",
            active: (history.location.pathname == "/onboarding/create-contract/pay-structure")
        },
        {
            anchor: "/onboarding/create-contract/seller-info",
            name: "Whose your client.",
            active: (history.location.pathname == "/onboarding/create-contract/seller-info")
        },
    ]
    const [state, updateState ] = useState({
        agencies: [],
        passwordVisible: false,
        params: {},
        loading: false
    })

    const [addAccount, { data, loading }] = useMutation(props.addAccountMutation);

    const addAccountAction = async () => {
        const { toastManager } = props
        try {
            let resp = await addAccount({ variables: { ...state.params } });
            if(resp){
                toastManager.add("Added bank account", {
                    appearance: 'success',
                    autoDismiss: true
                })
                if(props.redirectUrl){
                    history.push(props.redirectUrl)
                }else{
                    history.push("home")
                }
            }
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true
            // })
            
        }
    }

    const onChange = (e) => {
        
        
        let target = e.target
        updateState({
            ...state,
            params: {
                ...state.params,
                [target.name] : target.value
            }
        })
    }

    const skip =() =>{
        history.push("/onboarding/addcard")
    }
    
    return(
        <div className="row bg-body-alt">
            <Sidebar image={require('../../../assets/img/header-phone.svg')} links={links} />
            <div className="col-md-8 col-12">
                <div className="container-fluid pt-5">
                    <div className="row mt-5 justify-content-center">
                        <div className="px-md-auto col-md-7 col-12">
                            <Index />
                            {/* <div className="row mt-4 mb-4">
                                    <div className="col-auto">
                                        <button className="btn btn-light" onClick={skip}>Skip</button>
                                    </div>
                                    <div className="col">
                                        
                                    </div>
                                    <div className="col-auto">
                                        <button className="btn btn-primary" onClick={addAccountAction}>Verify</button>
                                    </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withRouter(Layout);