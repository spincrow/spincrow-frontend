import React, { Component } from "react";
import {Switch, Route, NavLink} from "react-router-dom"
import BasicInformation from "./basicinformation"
import PaymentStructure from "./paymentstructure"
import SellerInformation from "./sellerinformation"

export default class SelectRole extends Component{

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="col-12">
                <div className="row">
                    <Switch>
                        <Route exact path="/onboarding/create-contract">
                            <BasicInformation/>
                        </Route>
                        <Route exact path="/onboarding/create-contract/pay-structure">
                            <PaymentStructure/>
                        </Route>
                        <Route exact path="/onboarding/create-contract/seller-info">
                            <SellerInformation/>
                        </Route>
                    </Switch>         
                </div>
            </div>
        )
    }
}