import React, { Component, useState, useEffect} from "react";
import Form from "../../../../components/form"
import classnames from "classnames"
import {updateContract, getContracts} from "../../../../store/Actions/Contract"
import {connect} from "react-redux"
import {withRouter, useHistory} from "react-router-dom"
import PhoneInput from 'react-phone-input-2'
import {withToastManager} from "react-toast-notifications"
import {useMutation, useLazyQuery} from "@apollo/react-hooks"
import gql from "graphql-tag";
import {CREATE_CONTRACT, GET_CONTRACTS} from "../../../dashboard/contracts/utils/queries"
// import '../../../../assets/styles/phone-input.css'


const mapStateToProps = (state) => ({
    newContract: state.newContract, 
    createContractMutation: CREATE_CONTRACT, 
    getContractsQuery: GET_CONTRACTS
})

const mapDispatchToProps = {
    updateContract,
    getContracts
}

const SellerInfo  = (props) => {

    const [state, updateState] = useState({
        client: "",
        client_email:"",
        client_phone: ""
    })

    useEffect(() => {
        updateState({
            ...state,
            ...props.newContract
        })
    }, []);

    const updateInternalState = (e) => {
        updateState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    // defining all props and axillary methods to import
    const [createContract, {data, loading}] = useMutation(props.createContractMutation);

    // re loading all contracts
    const [ fetchContracts ] = useLazyQuery(props.getContractsQuery, { variables: {}, onCompleted: data => {
        
        updateState({
            ...state,
            contracts: data.contracts
        })
        props.getContracts(data.contracts).then(() => {
            // reload all the contracts
            // 
            history.push('/home')
        })
    }});
    let history = useHistory();
    const {toastManager} = props

    const nextMove = async () => {
        try {
            let resp = await createContract({variables: {...state}})
            if(resp){
                await fetchContracts()
                toastManager.add("Successfully created contract", {
                    appearance: 'success',
                    autoDismiss: true
                })
            }
        } catch (e) {
            // toastManager.add("Something went wrong", {
            //     appearance: "error", 
            //     autoDismiss: true
            // })
        }
    }

    const skip = () => {
        history.push('/home')
    }

    return (
        <div className="col-12">
            <div className="card-header border-0">
                <div className="card-header-title mt-3">
                    <h1 className="font-weight-bold">Whose your buyer / seller</h1>
                    <h4>Who are you buying from / selling to.</h4>
                </div>
            </div>
            <div className="card-body">
                <div className="row">
                    <div className="col-12 form-group">
                        <label> Client name</label>
                        <input name="client" value={state.client} onChange={updateInternalState} className="form-control"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 form-group">
                        <label> Client email</label>
                        <input name="client_email" value={state.client_email} onChange={updateInternalState} className="form-control"/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 form-group">
                        <label> Client phone no</label>
                        <PhoneInput
                            containerClass=""
                            inputClass="form-control"
                            inputStyle={{width: '100%'}}
                            country={'ng'}
                            value={state.client_phone}
                            onChange={phone => updateState({ 
                                ...state,
                                client_phone: phone
                                })}
                        />
                        {/* <input name="client_phone" value={state.client_phone} onChange={updateInternalState} className="form-control"/> */}
                    </div>
                </div>
            </div>
            <div className="card-footer">
                <div className="row">
                    <div className="col-auto">
                        <button className="btn btn-light" onClick={skip}>
                            <span className="fe fe-chevron-left"></span> Skip
                        </button>
                    </div>
                    <div className="col">

                    </div>
                    <div className="col-auto">
                        <button className="btn btn-primary" disabled={loading} onClick={nextMove}>
                            {
                                loading ? <div className="spinner-border" role="status">
                                    <span className="sr-only">Loading...</span>
                                </div>: <> 
                                    Create Contract <span className="fe fe-chevron-right"></span>
                                </>
                            }
                        </button>
                    </div>
                </div>
            </div>
        </div>   
    )
}

export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(SellerInfo)))