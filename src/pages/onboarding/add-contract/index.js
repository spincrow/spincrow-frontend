import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag'


const AddACCOUNT = gql`
    mutation addBankAccount($account_number: String!, $bank_name: String!, $account_name: String!, $bank_code: String){
        addBankAccount(input: {
            account_number:$account_number,
          	bank_name:$bank_name,
            account_name: $account_name,
          	bank_code: $bank_code
        }){
            id
        		account_number
      			bank_name
        		bank_code
        		owner{
              first_name
              last_name
              id
            }
        }
    }
`;


const mapStateToProps = (state) => ({
    addAccountMutation: AddACCOUNT,
    redirectUrl: state.redirectUrl
})

const mapDispatchToProps = {
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))