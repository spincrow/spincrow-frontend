import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
// import {getContracts} from "../../../../store/Actions/Contract";
import gql from 'graphql-tag';
// import {GET_SINGLE_CONTRACT} from "../utils/queries"
import {getCards} from "../../../store/Actions/Card"
import {getBank} from "../../../store/Actions/Bank"
import {updateUser} from "../../../store/Actions/Auth"
import {walletUpdate} from "../../../store/Actions/Settings"
import {UPDATE_WALLET_TOUR} from "../utils/queries"

const mapStateToProps = (state => {
    return {
        // getContractQuery: GET_SINGLE_CONTRACT,
        updateWalletTourQuery: UPDATE_WALLET_TOUR,
        // contracts: state.contracts,
        user: state.currentUser,
        bankaccounts: state.bankaccounts,
        cards: state.cards,
        wallet: state.wallet
    }
});

const mapDispatchToProps = {
    // getContracts: getContracts
    getCards: getCards,
    getBank: getBank,
    walletUpdate: walletUpdate,
    updateUser: updateUser
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))