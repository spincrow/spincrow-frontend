import gql from "graphql-tag"

export const GET_BANK_ACCOUNT = gql`
    query bankacounts{
        bankaccounts {
            id
            account_number
            account_name
            bank_name
            owner {
                first_name
                last_name
            }
            bank_code
        }
    }
`
export const BANK_CODES = gql`
    query BankCodes{
        bankcodes{
            bankname
            bankcode
            internetbanking
        }
    }
`

export const GET_CARDS = gql`
    query cards {
        cards {
            id
            card_name
            type
            expirymonth
            expiryyear
            last_number
        }
    }
`

export const TOP_UP_WALLET = gql`
mutation TopUpwallet(
    $tx_ref:String
    $card_type:TopUpCardTypeEnum!
    $amount: Float!
    $card_id:ID
){
  topupWallet(input: {
    card_type: $card_type,
    amount: $amount,
  	tx_ref: $tx_ref,
    card_id: $card_id
  }){
    id
    va_frequency
    va_expirydate
    va_order_ref
    va_account_number
    va_tx_ref
    va_bank_name
    va_account_name
    va_created_at
    va_note
    transactions {
      id
      amount
      flow
      details
      status
      created_at
    }
    amount
    owner{
    	id
    }
  }
}
`

export const WITHDRAWAL_MUTATION = gql`
mutation withdrawFunds(
  $bank_account_id: ID, 
  $amount:Float!, 
  $withdrawal_type: WithdrawalTypeEnum!, 
  $bank_name:String,
  $bank_code:String,
  $account_name:String,
  $account_number:String
  ){withdrawFunds(input: {
    bank_account_id:$bank_account_id,
    amount:$amount,
    withdrawal_type: $withdrawal_type,
  	bank_name: $bank_name,
    bank_code: $bank_code,
    account_name:$account_name,
    account_number:$account_number
}){
    amount
    va_flw_ref
    va_frequency
    va_expirydate
    va_order_ref
    va_account_number
    va_account_name
    va_tx_ref
    va_bank_name
    va_created_at
    va_note
    transactions {
        created_at
        amount
        details
    }
}
}
`