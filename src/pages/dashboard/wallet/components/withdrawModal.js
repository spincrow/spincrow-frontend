import React, {useState, useEffect} from "react"
import {useMutation, useQuery} from "@apollo/react-hooks"
import {formatCurrency} from "../../../../utils/helpers"
import LoadingText from "../../../../components/loadingtext"
import gql from "graphql-tag"
import { withToastManager } from 'react-toast-notifications';
import {WITHDRAWAL_MUTATION, BANK_CODES} from "../utils/queries"
import CardLoading from "../../../../components/card-loading"
import analytics from "../../../../modules/analytics"

const Modal = (props) => {

    const [state, updateState] = useState({
        progress: 0,
        wallet: null,
        params: {}
    })

    const {toastManager} = props
    const {loading, data: bankCodes } = useQuery(BANK_CODES);
    const [withdrawFunds, {data, loading: withdrawalLoading}] = useMutation(WITHDRAWAL_MUTATION, { onCompleted: async (data) => {
        analytics.track("Wallet withdrawal procesing", {
            amount: parseFloat(state.params.amount)
        })
        updateState({
            ...state,
            wallet: data.withdrawFunds,
        })

        props.updateWallet(data.withdrawFunds)
    }})


    const updateParams = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value
            }
        })
    }

    const updateParamsSelectBank = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                bank_name : e.target.options[e.target.selectedIndex].dataset.bankName,
                bank_code : e.target.value
            }
        })
    }

    const withdrawFundsUpdate = async () => {
        // props.updateMilestone(state)
        analytics.track("Wallet withdrawal initiated", {
            amount: parseFloat(state.params.amount)
        })
        try{
            if(props.bankaccounts.length > 0){
                await withdrawFunds({variables: {
                   bank_account_id: state.params.bank_account_id,
                   amount: parseFloat(state.params.amount),
                   withdrawal_type: "NORMAL_WITHDRAWAL"
                }})
                toastManager.add("Your withdrawal is being processed", {
                    appearance: 'success',
                })
            }else{
                await withdrawFunds({variables: {
                    bank_account_id: state.params.bank_account_id,
                    amount: parseFloat(state.params.amount),
                    withdrawal_type: "ADD_ACCOUNT",
                    account_number: state.params.account_number,
                    bank_name: state.params.bank_name,
                    account_name: state.params.account_name,
                    bank_code: state.params.bank_code,
                }})
                toastManager.add("Your withdrawal is being processed", {
                    appearance: 'success',
                })
            }
        }catch(err){
            analytics.track("Wallet withdrawal failed", {
                amount: parseFloat(state.params.amount),
                error: err.message()
            })
            // toastManager.add(err.message, {
            //     appearance: "error",
            //     autoDismiss: true
            // })
        }
    }

    return (
        <div className="modal fade show" id="withdraw-wallet" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        <React.Fragment>
                            <div className="card-header">
                                <div className="row">
                                    <div className="col">
                                        <h4 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                            Withdraw Funds
                                        </h4>
                                    </div>
                                    <div className="col-auto">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <form>
                                    <div className="row">
                                        <div className="form-group col-12">
                                            <label htmlFor="form_amount">
                                                Amount
                                            </label>
                                            <input onChange={updateParams} name="amount" id="form_amount" type="number" className="form-control" placeholder="Enter the amount you want to top up with" />
                                        </div>
                                    </div>
                                    {
                                        props.bankaccounts.length > 0 ? 
                                        <div className="row">
                                            <div className="form-group col-12">
                                                <label htmlFor="form_select">
                                                Select a bank account
                                                </label>
                                                <select id="form_select" className="form-control" name="bank_account_id" onChange={updateParams}>
                                                    <option> --SELECT YOUR BANK ACCOUNT-- </option>
                                                    {
                                                        props.bankaccounts.map((bankaccount, index) => <option value={bankaccount.id} key={index}>{bankaccount.account_name} - {bankaccount.bank_name} ({bankaccount.account_number})</option>)
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                        :
                                        <>
                                            {
                                                !loading ?
                                                    <>
                                                        <div className="row">
                                                            <div className="col form-group">
                                                                <label>Account number</label>
                                                                <input className="form-control" type="text" name="account_number" onChange={updateParams}/>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col form-group">
                                                                <label>Account name</label>
                                                                <input className="form-control" type="text" name="account_name" onChange={updateParams}/>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col form-group">
                                                                <label>Bank Name</label>
                                                                <select className="form-control" type="text" name="bank_name" onChange={updateParamsSelectBank}>
                                                                    <option> -- SELECT BANK -- </option>
                                                                    { bankCodes.bankcodes.map((item, index) => <option key={index} data-bank-name={item.bankname} value={item.bankcode}>{item.bankname}</option>) }
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </>
                                                : <CardLoading/>
                                            }
                                        </>
                                    }
                                </form>
                            </div>
                            <div className="card-footer">
                                <div className="row">
                                    <div className="col">
                                        <button className="btn btn-primary btn-block"  onClick={withdrawFundsUpdate}>
                                            <LoadingText loading={withdrawalLoading} text="Withdraw funds"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withToastManager(Modal)