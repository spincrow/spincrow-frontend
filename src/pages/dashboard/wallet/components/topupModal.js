import React, {useState, useEffect} from "react"
import {useMutation} from "@apollo/react-hooks"
import {connect} from "react-redux"
import {formatCurrency, generateUUID, integrityValue} from "../../../../utils/helpers"
import {TOP_UP_WALLET} from "../utils/queries"
import LoadingText from "../../../../components/loadingtext"
// import LoadingText from "@components/loadingtext"
import { withToastManager } from 'react-toast-notifications';

import analytics from "../../../../modules/analytics"

const mapStateToProps = (state) => ({currentUser: state.currentUser})
const mapDispatchToProps = {}

const FL_PUB = process.env.REACT_APP_RAVE_PUBLIC_KEY
const FL_SEC = process.env.REACT_APP_RAVE_SECRET_KEY
const FL_ENC = process.env.REACT_APP_RAVE_ENCRYPT_KEY


const Modal = (props) => {

    const {toastManager, currentUser, cards} = props
    const [state, updateState] = useState({
        wallet: null,
        params: {}
    })

    const [topupwalletMutation, {data, loading: topuploading}] = useMutation(TOP_UP_WALLET, { onCompleted: (data) => {
        analytics.track("Wallet topup", {
            amount: parseFloat(state.params.amount)
        })
        updateState({
            ...state,
            wallet: data.topupWallet
        })
        toastManager.add("Successfully topped up the wallet", {
            appearance: 'success',
        })
        props.updateWallet(data.topupWallet)
    }})


    const updateParams = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value
            }
        })
    }

    const topUpWallet = async () => {
        if(state.params.amount == null || state.params.amount == 0){
            toastManager.add("Fill all fields before trying to top up", {
                appearance: "error",
                autoDismiss: true
            })
        }else{
            analytics.track("Wallet topup initiated", {
                amount: parseFloat(state.params.amount)
            })
            try{
                await topupwalletMutation({variables: {
                   card_id: state.params.card_id,
                   amount: parseFloat(state.params.amount),
                   card_type: "CARD"
                }})
            }catch(err){
                analytics.track("Wallet topup failed", {
                    amount: parseFloat(state.params.amount),
                    error: err.message()
                })
                // toastManager.add(err.message, {
                //     appearance: 'error',
                //     autoDismiss: true
                // })
            }
        }
    }

    const fundWalletWithCard = async () => {
        try {
            if(state.params.amount == null || state.params.amount == ""){
                throw new Error("Fill in the transaction amount")
            }

            let transactionCode = generateUUID();
            const integrity_hash = integrityValue({
                PBFPubKey: FL_PUB,
                customer_email: currentUser.email,
                customer_firstname: currentUser.first_name,
                customer_lastname: currentUser.last_name,
                custom_description: "Adding your card",
                custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
                custom_title: "Spincrow Contract card",
                amount: state.params.amount,
                customer_phone: currentUser.phone,
                country: "NG",
                currency: "NGN",
                txref: transactionCode,
            });
            window.getpaidSetup({
                PBFPubKey: FL_PUB,
                customer_email: currentUser.email,
                customer_firstname: currentUser.first_name,
                customer_lastname: currentUser.last_name,
                custom_description: "Adding your card",
                custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
                custom_title: "Spincrow Contract card",
                amount: state.params.amount,
                customer_phone: currentUser.phone,
                country: "NG",
                currency: "NGN",
                txref: transactionCode,
                integrity_hash: integrity_hash,
                onclose: function() {},
                callback: async function(response) {
                    var flw_ref = response.tx.flwRef; // collect flwRef returned and pass to a 					server page to complete status check.
                    if (
                    response.tx.chargeResponseCode == "00" ||
                    response.tx.chargeResponseCode == "0"
                    ) {
                    // redirect to a success page, payment was successfully done, initiate call to update the current contract base and move value to 
                        analytics.track("Wallet topup initiated", {
                            amount: parseFloat(state.params.amount)
                        })
                        try{
                            await topupwalletMutation({variables: {
                               tx_ref: transactionCode,
                               amount: parseFloat(state.params.amount),
                               card_type: "NEW_CARD"
                            }})
                        }catch(err){
                            analytics.track("Wallet topup failed", {
                                amount: parseFloat(state.params.amount),
                                error: err.message()
                            })
                        }
                    } else {
                    // redirect to a failure page.
                    }
                }
            });
        } catch (e) {
            // toastManager.add(e.message, {
            //     appearance: 'error',
            //     autoDismiss: true
            // })
            analytics.track("Wallet topup error on rave modal", {
                amount: parseFloat(state.params.amount),
                error: e.message()
            })
        }
    }

    return (
        <div className="modal fade show" id="topup-wallet" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        <React.Fragment>
                            <div className="card-header">
                                <div className="row">
                                    <div className="col">
                                        <h4 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                            Top up wallet
                                        </h4>
                                    </div>
                                    <div className="col-auto">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <form>
                                    <div className="form-group col-12">
                                        <label htmlFor="form_amount">
                                            Amount
                                        </label>
                                        <input onChange={updateParams} name="amount" id="form_amount" type="number" className="form-control" placeholder="Enter the amount you want to top up with" />
                                    </div>
                                    <div className="form-group col-12">
                                        {
                                            cards.length > 0 ? 
                                            <>
                                                <label htmlFor="form_select">
                                                    Select a card
                                                </label>
                                                <select id="form_select" className="form-control" name="card_id" onChange={updateParams}>
                                                    <option> --SELECT YOUR CARD -- </option>
                                                    {
                                                        cards.map((card, index) => <option value={card.id} key={index}>Card {index+1} (**** **** **** {card.last_number})</option>)
                                                    }
                                                </select>
                                            </>:
                                            <>
                                                <div className="col form-group">
                                                    <h2 className="font-weight-bolder text-center"> Your have no cards saved with us, Pay with a debit card</h2>
                                                    <h3 className="text-center">Click the pay with card button below to fund your wallet using a new debitcard <br/> We'll also save the your card for future transactions. </h3>
                                                </div>
                                            </>
                                        }
                                    </div>
                                </form>
                            </div>
                            <div className="card-footer">
                                <div className="row">
                                    <div className="col">
                                        <button className="btn btn-primary btn-block" disabled={topuploading} onClick={()=> {
                                            cards.length > 0? topUpWallet() : fundWalletWithCard()
                                        }}>
                                            <LoadingText loading={topuploading} text="Top up wallet"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withToastManager(connect(mapStateToProps, mapDispatchToProps)(Modal))