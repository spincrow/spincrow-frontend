import React, {Component, useState, useEffect, useRef} from "react"
import {Link, useHistory, useParams} from "react-router-dom"
import {TransitionGroup, CSSTransition, Transition} from 'react-transition-group'
import Header from "../../../components/header"
import moment from "moment"
import {useQuery, useLazyQuery, useMutation} from "@apollo/react-hooks"
import classnames from "classnames"
import {formatCurrency, momentInTime, milestoneCalls, percentageOfSuccess} from "../../../utils/helpers"
import {GET_BANK_ACCOUNT, GET_CARDS} from "./utils/queries"
import { withToastManager } from "react-toast-notifications"
import WithdrawalModal from "./components/withdrawModal"
import TopupModal from "./components/topupModal"
import gql from "graphql-tag"
import Empty from "../../../components/empty"
import DebitSign from "../../../assets/img/debit-sign.svg"
import CreditSign from "../../../assets/img/credit-sign.svg"
import BankCard from "../../../components/card-component"
import BankAccount from "../../../components/bank-component"
import Skelenton from "react-loading-skeleton"
import CardLoading from "../../../components/card-loading"
import {Spring} from "react-spring/renderprops"
import TourGuide from "../../../components/tour-guide"
import buttonCircleSvg from "../../../assets/img/button-circle.svg"


const tourConfig = [
    {
        selector: `[data-tut="no_one"]`,
        content: () => (
            <div className="text-center">
                <h1>It's time to fund your wallet</h1>
                <p>Here you can manage funds you earn from your transactions, perform top-ups and withdrawals</p>
            </div>
        ),
        style: {
            minWidth: 400
        }
    },
    {
      selector: '[data-tut="reactour__wallet_1"]',
      content: () => (
        <div className="text-center">
                <h1>Withdraw funds</h1>
                <p>Click on the button to withdraw funds from the wallet to your bank ccount</p>
            </div>
        )
    },
    {
      selector: '[data-tut="reactour__wallet_2"]',
      content: () => (
        <div className="text-center">
            <h1>Top-up your wallet</h1>
            <p>Click the button to top up your wallet balance by bank transfer</p>
        </div>
    )
    },
    {
      selector: '[data-tut="reactour__wallet_3.0"]',
      content: () => (
        <div className="text-center">
            <h1>Top-up by wallet account no</h1>
            <p>Your wallet also has an account no, so you can easily fund your wallet by transfer, you can view it by clicking here.</p>
        </div>
        )
    },
    {
      selector: '[data-tut="reactour__wallet_3.1"]',
      content: () => (
        <div className="text-center">
            <h1>View your bank acounts</h1>
            <p>Personal bank acounts you add to your profile, are visible from here</p>
        </div>
        )
    },
    {
      selector: '[data-tut="reactour__wallet_4"]',
      content: () => (
        <div className="text-center">
            <h1>View your debit cards</h1>
            <p>You can also view your debit cards here.</p>
        </div>
      )
    },
    {
      selector: '[data-tut="reactour__wallet_5"]',
      content: () => (
        <div className="text-center">
            <h1>Keep track of transactions</h1>
            <p>here you can see all of your transactions so far with respect to your wallet, payments, topups and withdrawals</p>
        </div>
      )
    }
];

const GET_WALLET = gql`
    query ($first: Int, $last: Int, $skip: Int, $orderBy: WalletTransactionOrderByInput, $before: String, $after: String){
        wallet {
            va_frequency
            va_expirydate
            va_order_ref
            va_account_number
            va_tx_ref
            va_bank_name
            va_created_at
            va_account_name
            va_note
            amount
            transactions (first: $first, last: $last, skip: $skip, orderBy: $orderBy, before: $before, after: $after){
                id
                flow
                created_at
                details
                amount
            }
        }
    }
`

// define the internals
const Layout = (props)  => {
    
    let mountedRef = useRef(null)
    const {user, toastManager} = props
    const [state, updateState ] = useState({
        currentMilestone: null,
        loading: false,
        wallet: null,
        accountView: "bank",
        bank_accounts: [],
        cards: [],
        transactions: [],
        isTourOpen: false,
        isShowingMore: false,
    })

    const updateStateCall = (updates) => {
        if(mountedRef.current){
            updateState(state => ({
                ...state,
                ...updates
            }))
        }
    }

    const closeTour = async () => {
        updateStateCall({ isTourOpen: false });
        await updateWalletTourMutation()
        // send a request to update HomePage tour for user
    }
    

    const openTour = () => {
        updateStateCall({ isTourOpen: true });
    }
     
    let history = useHistory()

    const [updateWalletTourMutation, {loading: updateWalletTourQueryLoading}] = useMutation(props.updateWalletTourQuery, {
        onCompleted: async (data) => {
            props.updateUser(data.walletTourUpdate)
        }
    })

    const [ fetchBankAccounts, {loading: bankLoading} ] = useLazyQuery(GET_BANK_ACCOUNT, {onCompleted: async (response) => {
        // updateStateCall({
        //     bank_accounts: response.bankaccounts
        // })
        await props.getBank({bankaccounts: response.bankaccounts})
    }})


    const [getWallet, {loading: walletLoading, data: walletData}] = useLazyQuery(GET_WALLET, {
        onCompleted: async (response) => {
            updateStateCall({
                wallet: response.wallet
            })
            await props.walletUpdate(response.wallet)
        }
    })

    const [fetchCards, {loading: cardLoading}] = useLazyQuery(GET_CARDS, {onCompleted: async response => {
        // updateStateCall({
        //                 cards: response.cards
        // })
        await props.getCards({cards: response.cards})
    }})

    const updateWallet = (data) => {
        
        updateStateCall({
           wallet: data
        })
    }

    const callBankAndCardsAndWallet = async () => {
        await fetchCards()
        await fetchBankAccounts()
        await getWallet({
            variables: {
                first: 10,
                orderBy: "created_at_DESC"
            }
        })
    }

    const paginationPrevCalls = async () => {
        // we are limited to 10 transactions per call.
        await getWallet({
            variables: {
                last: 10,
                before: state.wallet.transactions[0].id,
                orderBy: "created_at_DESC"
            }
        })
    }

    const paginationNextCalls = async () => {
        // we are limited to 10 transactions per call.
        await getWallet({
            variables: {
                first: 10,
                after: state.wallet.transactions[9].id,
                orderBy: "created_at_DESC"
            }
        })
    }

    /**
     * Lifecycle hook
     */
    useEffect(() => {
        // getWallet()
        mountedRef.current = true
        if(props.wallet != null){
            updateStateCall({
                // cards: props.cards,
                // bank_accounts: props.bankaccounts,
                wallet: props.wallet
            })
        }else{
            callBankAndCardsAndWallet()
        }

        setTimeout(() => {
            openTour()
        }, 500)

        return () => {
            mountedRef.current = false
        }
    },[state.wallet])

    const renderSectionView = ()=> {
        switch(state.accountView){
            case "wallet_account":
                return (<>
                {
                    walletLoading ?  null : <>
                        {
                            state.wallet.va_account_number?
                            <BankAccount bankaccount={{
                                bank_name: state.wallet.va_bank_name,
                                account_number: state.wallet.va_account_number,
                                expiry: state.wallet.va_expirydate,
                                account_name: state.wallet.va_account_name
                            }}/>:  <div className="zoom card bg-light justify-content-center text-center" style={{height: 150}}>
                                    <p className="text-center mt-4">Click to generate your wallet account</p>
                                    <button className="btn rounded-circle btn-light">+</button>
                                </div>
                        }
                    </>
                }
                </>)
                
                break;
            case "bank":
                return (<>
                    {
                        cardLoading ? null: <>
                            {
                                props.bankaccounts.length > 0 ?
                                <>
                                    { props.bankaccounts.map((bankaccount, index) => {
                                        return <div className="row">
                                                <div className="col-12">
                                                    <BankAccount bankaccount={bankaccount} key={index} />
                                                </div>
                                        </div>
                                    })}
                                </>
                                :
                                    <div className="zoom card bg-light justify-content-center text-center" style={{height: 150}}>
                                        <p  className="text-center mt-4"> 
                                            <Link to="/settings/payments/add-bank">Add your bank account account</Link>
                                            <Link to="/settings/payments/add-bank" className="btn rounded-circle btn-light">+</Link>
                                        </p>
                                </div>
                            }
                        </>
                    }
                    </>)
                break;
            case "card":
                return (
                    <>
                        {
                            bankLoading ? null :
                            <>
                                {
                                    props.cards.length > 0 ?
                                    <>
                                        { props.cards.map((card, index) => {
                                            return  <div className="row">
                                                        <div className="col-12">
                                                            <BankCard deleteable={()=>{}} card={card} key={index} />
                                                        </div>
                                                    </div>
                                        })}
                                    </>
                                    :
                                    <div className="zoom card bg-light justify-content-center text-center" style={{height: 150}}>
                                            <p  className="text-center mt-4"> 
                                                <Link to="/settings/payments/add-card">Add your card</Link>
                                            </p>
                                            <Link to="/settings/payments/add-card" className="btn rounded-circle btn-light">+</Link>
                                    </div>
                                }
                            </>
                        }
                    </>
                )
                break;
            default:
                break;
        }
    }

    return(
        <React.Fragment>
            {
                props.user.wallet_tour? null : 
                <TourGuide
                    closeTour={closeTour}
                    tourConfig={tourConfig}
                    isTourOpen={state.isTourOpen}
                />
            }
            {/* <Header /> */}
            <WithdrawalModal updateWallet={updateWallet} bankaccounts={props.bankaccounts}/>
            <TopupModal updateWallet={updateWallet} cards={props.cards}/>
            <div className="container">
                <Spring from={{ opacity: 0, marginTop: -1000 }} to={{ opacity: 1, marginTop: 0 }}>
                    {
                        styles => {
                            return (
                                <div style={styles}>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="header border-0">
                                                <div className="header-body">
                                                    <div className="row align-items-end">
                                                        <div className="col">
                                                            <h1 className="header-title">
                                                                Wallet
                                                            </h1>
                                                        <p>
                                                            Manage your funds
                                                        </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="card">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col">
                                                                    {
                                                                        walletLoading ? <Skelenton /> : <>
                                                                            <span className="card-header-pretitle">Balance</span>
                                                                            {
                                                                                state.wallet ? 
                                                                                <h2 className="card-header-title font-weight-bolder"> {formatCurrency( state.wallet.amount)} </h2>:
                                                                                null
                                                                            }
                                                                        </>
                                                                    }
                                                                </div>
                                                            </div>
                                                            <hr/>
                                                            <div className="row">
                                                                <div className="col-md-6 mt-1">
                                                                    {
                                                                        walletLoading ? <Skelenton />:<> 
                                                                            <button data-tut="reactour__wallet_1"  data-target="#withdraw-wallet" data-toggle="modal"   className="btn btn-light btn-block btn-sm">
                                                                                <span className="text-primary"> <img src={buttonCircleSvg} width="15"/> Withdraw funds</span>
                                                                            </button>
                                                                        </>
                                                                    }
                                                                </div>
                                                                <div className="col-md-6 mt-1">
                                                                    {
                                                                        walletLoading ? <Skelenton />:<> 
                                                                            <button data-tut="reactour__wallet_2" data-target="#topup-wallet" data-toggle="modal" className="btn btn-light btn-block btn-sm">
                                                                                <span className="text-primary"> <img src={buttonCircleSvg} width="15"/> Top up</span>
                                                                            </button>
                                                                        </>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mt-3">
                                                    <div className="card">
                                                        <div className="card-header">
                                                            <h4 className="card-header-title mr-auto">Bank details</h4>
                                                            <ul className="nav nav-tabs nav-tabs-sm card-header-tabs">
                                                                <li className="nav-item" data-tut="reactour__wallet_3.0">
                                                                    <a onClick={()=>updateStateCall({accountView: "wallet_account"})} className={classnames({'nav-link': true, 'active': state.accountView == "wallet_account"})}>Wallet</a>
                                                                </li>
                                                                <li className="nav-item" data-tut="reactour__wallet_3.1">
                                                                    <a onClick={()=>updateStateCall({accountView: "bank"})} className={classnames({'nav-link': true, 'active': state.accountView == "bank"})}>Bank</a>
                                                                </li>
                                                                <li className="nav-item" data-tut="reactour__wallet_4">
                                                                    <a onClick={()=>updateStateCall({accountView: "card"})} className={classnames({'nav-link': true, 'active': state.accountView =="card"})}>Card</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    
                                                    {
                                                        cardLoading ?
                                                        <>
                                                            <CardLoading />
                                                        </>:
                                                            <>
                                                            <div className="card-body">
                                                                {renderSectionView()}
                                                            </div>
                                                        </>
                                                    }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="card overflow-hidden" data-tut="reactour__wallet_5">
                                                {
                                                    walletLoading ? 
                                                        <CardLoading />
                                                    :
                                                    <>
                                                        <div className="card-header">
                                                            <h3 className="card-header-title">All Transactions</h3>
                                                        </div>
                                                        <table className="table table-sm table-nowrap">
                                                            <thead>
                                                                <tr>
                                                                <th scope="col">Type</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Amount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {
                                                                    state.wallet ? 
                                                                    <>
                                                                        {
                                                                        state.wallet.transactions.length > 0 ?
                                                                            <>
                                                                                {
                                                                                    state.wallet.transactions.map((item, index) => {
                                                                                        let sign = null
                                                                                        switch (item.flow) {
                                                                                            case "CREDIT":
                                                                                                sign = CreditSign
                                                                                                break;
                                                                                            case "DEBIT":
                                                                                                sign = DebitSign
                                                                                                break;
                                                                                            default:
                                                                                                break;
                                                                                        }
                                                                                        return <tr key={index}>
                                                                                        <td><img src={sign} width={20}/></td>
                                                                                        <td>{momentInTime(item.created_at)}</td>
                                                                                        <td>{item.details}</td>
                                                                                        <td>{formatCurrency(item.amount)}</td>
                                                                                    </tr>
                                                                                        
                                                                                    })
                                                                                }
                                                                            </>
                                                                        :
                                                                                <tr >
                                                                                    <td colspan="100%">
                                                                                        <Empty message="There are no transactions on this wallet"></Empty>
                                                                                    </td>
                                                                                </tr>
                                                                        }
                                                                    </>
                                                                    :null

                                                                }
                                                            </tbody>
                                                        </table>
                                                        <div className="card-footer">
                                                        <ul className="pagination pagination-sm">
                                                            <li className="page-item"><a className="page-link" onClick={paginationPrevCalls}>Previous</a></li>
                                                            <li className="page-item"><a className="page-link" onClick={paginationNextCalls}>Next</a></li>
                                                        </ul>
                                                        </div>
                                                    </>
                                                }
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            )
                        }
                    }
                </Spring>
            </div>
        </React.Fragment>
     )
}

export default withToastManager(Layout);