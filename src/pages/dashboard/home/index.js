import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag'
import {getContracts} from "../../../store/Actions/Contract"
import {updateUser} from "../../../store/Actions/Auth"
import {UPDATE_HOME_TOUR} from "../utils/queries"

const GET_CONTRACTS = gql`
    query {
        contracts{
            id
            name
            amount
            created_at
            description
            paid
            client
            client_code
            payment_type
            owner_type
            client_phone
            disburseStatus
            status
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            client_account_name

            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_bank_name
            va_created_at
            va_note
            
            milestones {
                id
                target
                status
                disburseStatus
                progress
                status
                state
            }
            transactions {
                id
                details
                flow
                status
                amount
                created_at
            }
        }
    }
`

const mapStateToProps = (state => {
    return {
        getContractQuery: GET_CONTRACTS,
        updateHomeTourQuery: UPDATE_HOME_TOUR,
        contracts: state.contracts,
        user: state.currentUser
    }
});
const mapDispatchToProps = {
    getContracts,
    updateUser
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))