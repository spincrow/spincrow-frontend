import React, { useState } from "react"
import {Link} from "react-router-dom"
import {formatCurrency, percentageOfSuccess, milestoneCalls, momentInTime } from "../../../../utils/helpers"
import {Spring, Transition, animated} from "react-spring/renderprops"

const ContractView = ({contract, tag}) => {

    const [state, setState] = useState(false)

    return <div className="container">
                <div className="row">
                    <div className="col">
                        <Link to={`contracts/${contract.id}`}><h4 className="font-weight-bolder text-dark">{contract.name}</h4></Link>
                        <small className="text-secondary">{momentInTime(contract.created_at)}</small>
                    </div>
                    <div className="col">
                        {
                            contract.payment_type == "whole" ? 
                            <>
                                <small className="text-secondary">
                                    { formatCurrency(contract.amount)} to be transacted in whole payment
                                </small>
                                {/* <div className="progress progress-sm">
                                    <div className="progress-bar bg-success" role="progressbar" style={{width: `${percentageOfSuccess(contract.milestones)}%`}} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> */}
                            </>: 
                            <>
                                <small className="text-secondary">{milestoneCalls(contract.milestones)} milestones</small>
                                <div className="progress progress-sm">
                                    <div className="progress-bar bg-success" role="progressbar" style={{width: `${percentageOfSuccess(contract.milestones)}%`}} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </>
                        }
                    </div>
                    <div className="col-auto pt-3" data-toggle="collapse">
                        {
                            !state ? 
                                <span className="fe fe-chevron-down" onClick={() => setState(!state)}></span>
                            :
                                <span className="fe fe-chevron-up" onClick={() => setState(!state)}></span>
                        }
                    </div>
                </div>
                <div className="row pt-5 ">
                    <Transition
                    items={state}
                    from={{ opacity: 0, height: 0}}
                    enter={{ opacity: 1, height:"auto"}}
                    leave={{ opacity: 0, height: 0}}>
                    {show => show && (props => (
                        <div style={props}>
                            <div className="col-12">
                                    <h1 className="text-success font-weight-bolder">{formatCurrency(contract.amount)}</h1>
                                </div>
                                <div className="col-12">
                                    <p>
                                        {contract.description}
                                    </p>
                                </div>
                        </div>
                    ))}
                    </Transition>
                </div>
            </div>
}

export default ContractView