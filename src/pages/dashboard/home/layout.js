import React, {Component, useState, useEffect} from "react"
import {Link} from "react-router-dom"
import Header from "../../../components/header"
import {useQuery, useLazyQuery, useMutation} from "@apollo/react-hooks"
import cookies from "js-cookie"
import {Spring} from "react-spring/renderprops"
import ContractView from "./components/contractView"
import CardLoading from "../../../components/card-loading"
import Skeleton from "react-loading-skeleton"
import gql from "graphql-tag"
import TourGuide from "../../../components/tour-guide"
// import {addToast} from "react-toast-notifications"
// import "./confirm.scss"

import walletAmountSvg from "../../../assets/img/wallet-amount.svg"
import releasableContractSvg from "../../../assets/img/releasable-contracts.svg"
import pendingContractSvg from "../../../assets/img/pending-contract-icon.svg"
import { momentInTime, formatCurrency } from "utils/helpers"

const image = require('../../../assets/img/empty.svg')
const blueCard = require("../../../assets/img/svgs/blue-card.svg")
const greenCard = require("../../../assets/img/svgs/green-card.svg")

const statsQuery = gql`
    query stats{
        stats {
            active_contracts
            wallet_balance
            completed_contracts
        }
    }
`

const activitiesQuery = gql `
    query activities{
        activities {
            details
            amount
            created_at
        }
    }
`


const Layout = (props) =>  {


    const {user} = props
    const tourConfig = [
        {
            selector: `[data-tut="no_one"]`,
            content: () => (
                <div className="text-center">
                    <h3>Hey {user.first_name}</h3>
                    <h1>Welcome to spincrow</h1>

                    <p>We are really glad to have you here, let's get you up to speed with creating and running your own contracts</p>
                </div>
            ),
            style: {
                minWidth: 400
            }
        },
        {
          selector: '[data-tut="reactour__2"]',
          content: () => (
                <div className="text-center">
                    <h1>Monitor your contracts and wallet.</h1>
                    <p>This shows you the current state of your contracts and the amount of money in your wallet</p>
                </div>
            )
        },
        {
          selector: '[data-tut="reactour__3"]',
          content: () => (
                <div className="text-center">
                    <h1>Quickly create contracts.</h1>
                    <p>Get quick access to creating your contracts either as a buyer or seller</p>
                </div>
            )
        },
        {
          selector: '[data-tut="reactour__4"]',
          content: () => (
                <div className="text-center">
                    <h1>Your active contracts</h1>
                    <p>View your most active contracts here</p>
                </div>
            )
        },
        {
          selector: '[data-tut="reactour__5"]',
          content: () => (
                <div className="text-center">
                    <h1>Activities</h1>
                    <p>A quick preview of your most recent activites using the platform</p>
                </div>
            )
        }
      ];
    
    const [state, updateState] = useState({
        contracts: [],
        passwordVisible: false,
        params: {},
        isTourOpen: false,
        isShowingMore: false,
        loading: false
    })

    const onChange = (e) => {
        
        
        let target = e.target
        updateState({
            ...state,
            params: {
                ...state.params,
                [target.name] : target.value
            }
        })
    }

    const {data: stats, loading: statsLoading} = useQuery(statsQuery)
    const {data: activities, loading: activitiesLoading} = useQuery(activitiesQuery)

    const [ fetchContracts, {loading: contractLoading}] = useLazyQuery(props.getContractQuery, { variables: {}, onCompleted: data => {
        updateState({
            ...state,
            contracts: data.contracts
        })
        props.getContracts(data.contracts)
    }});

    const [updateHomeTour, {loading: updateHomeTourLoading}] = useMutation(props.updateHomeTourQuery, {
        onCompleted: async (data) => {
            props.updateUser(data.homeTourUpdate)
        }
    })

    const fetchState = async () => {
        await fetchContracts()
        setTimeout(() => {
            // openTour()
        }, 5000)
    }

    useEffect(() => {
        
        if(props.contracts.length < 1){
            fetchState()
        }else{
            updateState({
                ...state,
                contracts: props.contracts
            })
        }
        openTour()
    },[props.user])

    const closeTour = () => {
        updateState({ ...state, isTourOpen: false });
        updateHomeTour()
        // send a request to update HomePage tour for user
    }

    const openTour = () => {
        updateState(state => ({ ...state, isTourOpen: true }));
    }

    const toggleShowMore = () => {
        updateState(state => ({
            ...state,
            isShowingMore: !state.isShowingMore
        }));
    }

    return (
        <React.Fragment>
                {
                    props.user.home_tour == true? null : 
                    <TourGuide
                        closeTour={closeTour}
                        tourConfig={tourConfig}
                        isTourOpen={state.isTourOpen}
                    />
                }
                {/* <Header /> */}
                <div className="container mt-3">
                    <Spring from={{ opacity: 0, marginTop: -1000 }} to={{ opacity: 1, marginTop: 0 }}>
                        {
                            styles => {
                                return (
                                    <div className="row" style={styles}>
                                        <>
                                            <div className="col-12">
                                                <div className="row">
                                                    <div className="col-md-8 col-12 h-100 mb-3">
                                                        <div className="row">
                                                            <div className="col-12 mb-3">
                                                                <div className="card mb-0 " data-tut="reactour__2">
                                                                    <div className="card-body my-3">
                                                                        <div className="row p-3">
                                                                            <div className="col-12 col-md-4 border-right text-center">
                                                                                {
                                                                                    statsLoading ? 
                                                                                        <Skeleton /> : 
                                                                                    <>
                                                                                        <div className="row">
                                                                                            <div className="col-auto">
                                                                                                <img src={walletAmountSvg} />
                                                                                            </div>
                                                                                            <div className="col text-left">
                                                                                                <span className="font-weight-light h2 text-secondary">&#8358; {stats.stats.wallet_balance}.00</span><br/>
                                                                                                <span className="font-weight-light text-secondary small text-uppercase mt-3">wallet balance</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </>
                                                                                }
                                                                            </div>
                                                                            <div className="col-12 col-md-4 text-center">
                                                                                {
                                                                                    statsLoading ? 
                                                                                        <Skeleton /> : 
                                                                                    <>
                                                                                        <div className="row">
                                                                                            <div className="col-auto">
                                                                                                <img src={pendingContractSvg} />
                                                                                            </div>
                                                                                            <div className="col text-left">
                                                                                                <span className="font-weight-light h2 text-secondary">{stats.stats.active_contracts}</span><br/>
                                                                                                <span className="font-weight-light text-secondary small text-uppercase mt-3">active contracts</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                    </>
                                                                                }
                                                                            </div>
                                                                            <div className="col-12 col-md-4  border-left text-center">
                                                                                {
                                                                                    statsLoading ? 
                                                                                        <Skeleton /> : 
                                                                                    <>
                                                                                        <div className="row">
                                                                                            <div className="col-auto">
                                                                                                <img src={releasableContractSvg} />
                                                                                            </div>
                                                                                            <div className="col text-left">
                                                                                                <span className="font-weight-light h2 text-secondary">{stats.stats.completed_contracts}</span><br/>
                                                                                                <span className="font-weight-light text-secondary small text-uppercase mt-3">completed releasable</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                    </>
                                                                                }
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="row">
                                                                    <div className="col-12">
                                                                        <div className="card" data-tut="reactour__4">
                                                                            <div className="card-header">
                                                                                <div className="row">
                                                                                    <div className="col-auto">
                                                                                        <h4 className="card-header-title text-secondary">
                                                                                            Active contracts
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div className="col"></div>
                                                                                    <div className="col-auto">
                                                                                        <Link to="/contracts"><span className="small text-underline">View all</span></Link>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                                <>
                                                                                    {
                                                                                        contractLoading ? 
                                                                                            <ul className="list-group list-group-flush">
                                                                                                <li className="list-group-item px-3">
                                                                                                    <CardLoading />
                                                                                                </li>
                                                                                                <li className="list-group-item px-3">
                                                                                                    <CardLoading />
                                                                                                </li>
                                                                                                <li className="list-group-item px-3">
                                                                                                    <CardLoading />
                                                                                                </li>
                                                                                            </ul>
                                                                                        :
                                                                                        <>
                                                                                            {
                                                                                                state.contracts.length > 0 ?
                                                                                                <ul className="list-group list-group-flush">
                                                                                                    {
                                                                                                        state.contracts.slice(0, 5).map((item, index) => (
                                                                                                            <li key={index} className="list-group-item px-3">
                                                                                                                <ContractView contract={item} tag={index} />
                                                                                                            </li>
                                                                                                        ))
                                                                                                    } 
                                                                                                </ul>
                                                                                                :
                                                                                                <div className="card-body">
                                                                                                    <div className="row">
                                                                                                        <div className="col-12 justify-content-center d-flex">
                                                                                                            <div className="d-block">
                                                                                                                <img src={image} width="140" className="d-block"/>
                                                                                                                <p>No contracts available</p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            }
                                    
                                                                                        </>
                                                                                    }
                                                                                </>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4 col-12 mb-3">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="row" data-tut="reactour__3">
                                                                    <div className="col-md-6 col-12">
                                                                        <Link to="/contracts/new">
                                                                            <div className="card zoom bg-primary" style={{background: `url(${blueCard})`, backgroundRepeat: "no-repeat", backgroundSize: "cover"}}>
                                                                                <div className="card-body">
                                                                                    <div className="row">
                                                                                        <div className="col-md-12">
                                                                                            <h3 className="text-white font-weight-bolder">Buy</h3>
                                                                                            <small className="text-white">Are you selling goods or services?</small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </Link>
                                                                    </div>
                                                                    <div className="col-md-6 col-12">
                                                                        <Link to="/contracts/new">
                                                                            <div className="card zoom bg-success" style={{background: `url(${greenCard})`, backgroundRepeat: "no-repeat", backgroundSize: "cover"}}>
                                                                                <div className="card-body">
                                                                                    <div className="row">
                                                                                        <div className="col-md-12">
                                                                                            <h3 className="text-white font-weight-bolder">Sell</h3>
                                                                                            <small className="text-white">Are you selling goods or services?</small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </Link>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="row">
                                                                    <div className="col-12" data-tut="reactour__5">
                                                                        <div className="row">
                                                                            <div className="col">
                                                                                <h3 className="text-secondary">Recent activities</h3>
                                                                            </div>
                                                                            <div className="col-auto">
                                                                                {/* <h3 className="text-primary small">view all</h3> */}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-12">
                                                                        {
                                                                            activitiesLoading ? null : <>
                                                                            {
                                                                                activities.activities.length > 0 ? 
                                                                                <>
                                                                                    {activities.activities.slice(0, 5).map(activity => {
                                                                                            return <>
                                                                                                <div className="row">
                                                                                                    <div className="col-12">
                                                                                                        <div className="card rounded-0">
                                                                                                            <div className="card-body">
                                                                                                                <div className="row">
                                                                                                                    <div className="col">
                                                                                                                        <div className="row">
                                                                                                                            {/* <div className="col-auto">
                                                                                                                                <i className="text-danger fe fe-arrow-left"></i>
                                                                                                                            </div> */}
                                                                                                                            <div className="col">
                                                                                                                                <p className="font-weight">{activity.details}</p>
                                                                                                                                <span>{momentInTime(activity.created_at)}</span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div className="col-auto">
                                                                                                                        <h4 className="font-weight-bold">{
                                                                                                                            activity.amount ? 
                                                                                                                            formatCurrency(activity.amount) : null
                                                                                                                        }</h4>
                                                                                                                        {/* <span className="font-weight-light text-secondary">Debit</span> */}
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </>
                                                                                        })}
                                                                                </>: 
                                                                                <>
                                                                                    <div className="row">
                                                                                        <div className="col-12">
                                                                                            <div className="card rounded-0">
                                                                                                <div className="card-body">
                                                                                                    <div className="row">
                                                                                                        <div className="col">
                                                                                                            <div className="row">
                                                                                                                <div className="col-auto">
                                                                                                                    {/* <i className="fe fe-arrow-left"></i> */}
                                                                                                                </div>
                                                                                                                <div className="col">
                                                                                                                    <h4 className="font-weight text-center">No activities yet</h4>
                                                                                                                    {/* <span>12th, Aug 2019</span> */}
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div className="col-auto">
                                                                                                            {/* <h4 className="text-success font-weight-bold">+$230,000</h4>
                                                                                                            <span className="font-weight-light text-secondary">Debit</span> */}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </>
                                                                            }
                                                                            </>
                                                                        }
                                                                        {/* <div className="row">
                                                                            <div className="col-12">
                                                                                <div className="card rounded-0">
                                                                                    <div className="card-body">
                                                                                        <div className="row">
                                                                                            <div className="col">
                                                                                                <div className="row">
                                                                                                    <div className="col-auto">
                                                                                                        <i className="fe fe-arrow-left"></i>
                                                                                                    </div>
                                                                                                    <div className="col">
                                                                                                        <h4 className="font-weight-bolder">Build website</h4>
                                                                                                        <span>12th, Aug 2019</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="col-auto">
                                                                                                <h4 className="text-success font-weight-bold">+$230,000</h4>
                                                                                                <span className="font-weight-light text-secondary">Debit</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-12">
                                                                                <div className="card rounded-0">
                                                                                    <div className="card-body">
                                                                                        <div className="row">
                                                                                            <div className="col">
                                                                                                <div className="row">
                                                                                                    <div className="col-auto">
                                                                                                        <i className="fe fe-arrow-left"></i>
                                                                                                    </div>
                                                                                                    <div className="col">
                                                                                                        <h4 className="font-weight-bolder">Build website</h4>
                                                                                                        <span>12th, Aug 2019</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="col-auto">
                                                                                                <h4 className="text-success font-weight-bold">+$230,000</h4>
                                                                                                <span className="font-weight-light text-secondary">Debit</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-12">
                                                                                <div className="card rounded-0">
                                                                                    <div className="card-body">
                                                                                        <div className="row">
                                                                                            <div className="col">
                                                                                                <div className="row">
                                                                                                    <div className="col-auto">
                                                                                                        <i className="fe fe-arrow-left"></i>
                                                                                                    </div>
                                                                                                    <div className="col">
                                                                                                        <h4 className="font-weight-bolder">Build website</h4>
                                                                                                        <span>12th, Aug 2019</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div className="col-auto">
                                                                                                <h4 className="text-success font-weight-bold">+$230,000</h4>
                                                                                                <span className="font-weight-light text-secondary">Debit</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div> */}
                                                                    </div>
                                                                </div>
                                                                {/* <div className="row">
                                                                    <div className="col-12">
                                                                        <div className="card text-pink rounded-0 bg-pink">
                                                                            <div className="card-body">
                                                                                <div className="row text-center">
                                                                                    <div className="col text-center text-white">
                                                                                    <h3>Invite your friends</h3>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> */}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <div className="col-12">
                                                <div className="row">
                                                    <div className="col-md-8 col-12">
                                                    </div>
                                                    <div className="col-md-4 col-12">
                                                    </div>
                                                </div>
                                            </div> */}
                                        </>
                                    </div>
                                )
                            }
                        }
                    </Spring>
                    {/* <Fade top duration={500}> */}

                    {/* </Fade> */}
                </div>
           </React.Fragment>
    );
}

export default Layout;