import React, { Component } from "react";
import {Switch, Route, NavLink} from "react-router-dom"
import PersonalInformation from "./personalinformation"
import Security from "./security"
import PaymentInfo from "./payments"
import AddCard from "./components/add-card"
import AddBank from "./components/add-bank"

export default class SelectRole extends Component{

    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="col-12 mt-md-3 col-md-9">
                <div className="row">
                    <div className="col-3 d-none d-md-block">
                        <div className="col-12 mt-5">
                            <ul className="list-group list-group-flush mt-4">
                                <li className="list-group-item mt-3 border-0">
                                    <NavLink exact={true} activeClassName="text-primary" className="font-weight-bold" to="/settings"><span className="fe fe-circle"></span>  Personal information </NavLink>
                                </li>
                                <li className="list-group-item mt-3 border-0">
                                    <NavLink exact={true} activeClassName="text-primary" className="font-weight-bold" to="/settings/security"><span className="fe fe-circle"></span>  Security </NavLink>
                                </li>
                                <li className="list-group-item mt-3 border-0">
                                    <NavLink exact={true}  activeClassName="text-primary" className="font-weight-bold" to="/settings/payments"><span className="fe fe-circle"></span>  Payments </NavLink>
                                </li>
                                {/* <li className="list-group-item mt-3 border-0">
                                    <NavLink exact={true} activeClassName="text-primary" className="font-weight-bold" to="/contracts/new/payment-information"><span className="fe fe-circle"></span>  Payment information </NavLink>
                                </li> */}
                            </ul>
                        </div>
                    </div>     
                    <div className="col-12 d-block d-md-none">
                        <div className="header ">
                            <div className="header-body">
                                <div className="row align-items-end">
                                    <div className="col">
                                        <h6 className="header-pretitle">
                                        Adjust your settings
                                        </h6>

                                        <h1 className="header-title">
                                        Settings
                                        </h1>
                                        
                                    </div>
                                    <div className="col-auto">
                                            <ul className="nav nav-tabs header-tabs  nav-tabs-sm">
                                                <li className="nav-item">
                                                    <NavLink exact={true} activeClassName="active" className="nav-link" to="/settings">
                                                    Personal Information
                                                    </NavLink>
                                                </li>
                                                <li className="nav-item">
                                                    <NavLink exact={true} activeClassName="active" className="nav-link" to="/settings/security">
                                                    Security
                                                    </NavLink>
                                                </li>
                                                <li className="nav-item">
                                                    <NavLink exact={true} activeClassName="active" className="nav-link" to="/settings/payments">
                                                    Payments
                                                    </NavLink>
                                                </li>
                                            </ul>

                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <Switch>
                        <Route exact path="/settings">
                            <PersonalInformation/>
                        </Route>
                        <Route exact path="/settings/security">
                            <Security/>
                        </Route>
                        <Route exact path="/settings/payments">
                            <PaymentInfo/>
                        </Route>
                        <Route exact path="/settings/payments/add-card">
                            <AddCard />
                        </Route>
                        <Route exact path="/settings/payments/add-bank">
                            <AddBank />
                        </Route>
                    </Switch>         
                </div>
            </div>
        )
    }
}