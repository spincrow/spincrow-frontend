import React, {useState} from "react"
import {connect} from "react-redux"
import {useMutation} from "@apollo/react-hooks"
import {withRouter} from "react-router-dom"
import {ADD_CARD} from "../../../utils/queries"
import {withToastManager} from "react-toast-notifications"
import {generateUUID, integrityValue} from "../../../../../../utils/helpers"
import {updateCards} from "../../../../../../store/Actions/Card"

const mapStateToProps = (state) => ({currentUser: state.currentUser})
const mapDispatchToProps = { updateCards }

const FL_PUB = process.env.REACT_APP_RAVE_PUBLIC_KEY
const FL_SEC = process.env.REACT_APP_RAVE_SECRET_KEY
const FL_ENC = process.env.REACT_APP_RAVE_ENCRYPT_KEY

const AddCard = (props) => {
    // adding card
    const [state, updateState] = useState({
        params: {

        }
    })

    const {toastManager, history, currentUser} = props
    const updateParams = (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value == "" ? null : e.target.value
            }
        })
    }

    const [addCard, {loading: addCardloading}] = useMutation(ADD_CARD, {onCompleted: (resp) => {
        
        props.updateCards({card: resp.addCard})
        toastManager.add("Successfully added your card 🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
        setTimeout(()=>{
            history.push("/settings/payments")
        }, 500)
    }})


    const fundAccount = async () => {
        // add all Milestones.
        // var PBFKey = "FLWPUBK-aa82cac8ee08f5bb206f937db274081a-X";
        // if( !state.params.card_number || !state.params.expirymonth || !state.params.expiryyear || !state.params.cvv){
        //     toastManager.add("Fill in your card details", {
        //         appearance: 'error',
        //         autoDismiss: true,
                
        //     })
        //     return;
        // }
        let transactionCode = generateUUID();
        const integrity_hash = integrityValue({
            PBFPubKey: FL_PUB,
            customer_email: currentUser.email,
            customer_firstname: currentUser.first_name,
            customer_lastname: currentUser.last_name,
            custom_description: "Adding your card",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Add your card",
            amount: 100,
            customer_phone: currentUser.phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
        });
        // 
        window.getpaidSetup({
            PBFPubKey: FL_PUB,
            customer_email: currentUser.email,
            customer_firstname: currentUser.first_name,
            customer_lastname: currentUser.last_name,
            custom_description: "Adding your card",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Add your card",
            amount: 100,
            customer_phone: currentUser.phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
            integrity_hash: integrity_hash,
            onclose: function() {},
            callback: async function(response) {
                var flw_ref = response.tx.flwRef; // collect flwRef returned and pass to a 					server page to complete status check.
                
                if (
                response.tx.chargeResponseCode == "00" ||
                response.tx.chargeResponseCode == "0"
                ) {
                // redirect to a success page, payment was successfully done, initiate call to update the current contract base and move value to 
                    
                    try{
                        await addCard({
                            variables: {
                                txRef: transactionCode,
                            }
                        })
                    }catch(err){
                        toastManager.add("Something went wrong 🎉 ", {
                            appearance: 'error',
                            autoDismiss: true,
                            
                        })
                    }
                } else {
                // redirect to a failure page.
                }
            }
        });
    }

    return (
        <div className="col-12 col-md-9">
        <div className="card">
            <div className="card-header border-0">
                <div className="card-header-title text-secondary mt-3">
                <span className="pointer" onClick={() => history.goBack()}>
                        <i className="fe fe-chevron-left"> </i> <span>Back</span>
                    </span>
                </div>
            </div>
            <div className="card-header border-0">
                <div className="card-header-title mt-3">
                    <h2 className="font-weight-bold">Add Card</h2>
                    <small>We need this information in order to charge you.</small>
                </div>
            </div>
            <div className="card-body">
                <div className="row mt-4 mb-4">
                        <div className="col-12">
                            <div className="row">
                                <div className="col form-group">
                                    <h3>To add and verify your card ₦ 100 will be charged and saved into your wallet </h3>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div className="card-footer">
                <div className="row">
                    <div className="col-auto">
                        <button className="btn btn-light" onClick={() => history.goBack()}>
                            <span className="fe fe-chevron-left"></span> Back 
                        </button>
                    </div>
                    <div className="col">

                    </div>
                    <div className="col-auto">
                        <button className="btn btn-primary" disabled={addCardloading} onClick={fundAccount}>
                            Proceed
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    )
}

export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCard)))