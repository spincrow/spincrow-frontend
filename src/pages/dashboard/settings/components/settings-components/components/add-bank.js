import React, {useState, useEffect} from "react"
import {connect} from "react-redux"
import {useMutation, useQuery} from "@apollo/react-hooks"
import {withRouter} from "react-router-dom"
import {ADD_CARD, ADD_BANK_ACCOUNT, BANK_CODES, RESOLVE_ACCOUNT} from "../../../utils/queries"
import {withToastManager} from "react-toast-notifications"
import {generateUUID, integrityValue} from "../../../../../../utils/helpers"
import {updateBanks} from "../../../../../../store/Actions/Bank"
import CardLoading from "../../../../../../components/card-loading"

const mapStateToProps = (state) => ({currentUser: state.currentUser})
const mapDispatchToProps = {updateBanks}

const AddBank = (props) => {
    // adding card
    const [state, updateState] = useState({
        params: {
            account_name: null
        }
    })

    const {toastManager, history, currentUser} = props

    const updateParams = async (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value == "" ? null : e.target.value
            }
        })

        // await queryToGetAccount()
    }

    const updateParamsSelectBank = async (e) => {
        updateState({
            ...state,
            params: {
                ...state.params,
                bank_name : e.target.options[e.target.selectedIndex].dataset.bankName,
                bank_code : e.target.value
            }
        })
    }

    const [addBank] = useMutation(ADD_BANK_ACCOUNT, {onCompleted: (resp) => {
        
        props.updateBanks({bankaccount: resp.addBankAccount})
        toastManager.add("Successfully add a bank account 🎉 ", {
            appearance: 'success',
            autoDismiss: true,
        })
        setTimeout(()=>{
            history.push("/settings/payments")
        }, 500)
    }})

    const [resolveAccount, {loading: loadingResolveAccount}] = useMutation(RESOLVE_ACCOUNT, {onCompleted: async (resp) => {
        
        // props.updateBanks({bankaccount: resp.addBankAccount})
        setTimeout(() => {
            if(resp.resolveBankAccount){
                updateState({
                    ...state,
                    params: {
                        ...state.params,
                        account_name: resp.resolveBankAccount.account_name
                    }
                })
            }else{
                toastManager.add("Your bank account doesn't exist, check your entry",{
                    appearance: 'error',
                    autoDismiss: true
                } )
            }
        }, 1000)
    }})

    const queryToGetAccount = async () => {
        if(state.params.account_number && state.params.bank_code){
            await resolveAccount({
                variables:{
                    account_no: state.params.account_number,
                    account_bank: state.params.bank_code
                }
            })
        }
    }

    const {loading, data } = useQuery(BANK_CODES);

    const disableFields = () => {
        if(state.params.account_name && state.params.bank_name && state.params.account_number){
            return false
        }else{
            return true
        }
    }

    const addBankAccount = async () => {
        try{
            await addBank({
                variables: {
                    account_number: state.params.account_number,
                    bank_name: state.params.bank_name,
                    account_name: state.params.account_name,
                    bank_code: state.params.bank_code
                }
            })
        }catch(err){
            toastManager.add("Something went wrong 🎉 ", {
                appearance: 'error',
                autoDismiss: true,
                
            })
        }
    }


    useEffect(() => {
        queryToGetAccount()
    },[state])

    return (
        <div className="col-12 col-md-9">
        <div className="card">
            <div className="card-header border-0">
                <div className="card-header-title text-secondary mt-3">
                    <span className="pointer"  onClick={() => history.goBack()}>
                        <i className="fe fe-chevron-left"> </i> <span>Back</span>
                    </span>
                </div>
            </div>
            <div className="card-header border-0">
                <div className="card-header-title mt-3">
                    <h2 className="font-weight-bold">Add Bank Account</h2>
                    <small>we need these information to add your bank account</small>
                </div>
            </div>
            <div className="card-body">
                {
                    (!loading && data !== null) ?
                        <>
                            <div className="row">
                                <div className="col form-group">
                                    <label>Account number</label>
                                    <input className="form-control" type="text" name="account_number" onChange={updateParams}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col form-group">
                                    <label>Bank Name</label>
                                    <select className="form-control" type="text" name="bank_name" onChange={updateParamsSelectBank}>
                                        <option> -- SELECT BANK -- </option>
                                        { data.bankcodes.map((item, index) => <option key={index} data-bank-name={item.bankname} value={item.bankcode}>{item.bankname}</option>) }
                                    </select>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col form-group">
                                    <label>Account name</label>
                                    <input className="form-control" type="text" name="account_name" value={state.params.account_name} disabled/>
                                </div>
                            </div>
                        </>
                    : <CardLoading />
                }
            </div>
            <div className="card-footer">
                <div className="row">
                    <div className="col-auto">
                        <button className="btn btn-light" onClick={() => history.goBack()}>
                            <span className="fe fe-chevron-left"></span> Back 
                        </button>
                    </div>
                    <div className="col">

                    </div>
                    <div className="col-auto">
                        <button className="btn btn-primary" disabled={disableFields()} onClick={addBankAccount}>
                            Confirm
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    )
}

export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(AddBank)))