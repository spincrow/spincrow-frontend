import React, { Component, useState, Fragment } from "react";
import Form from "../../../../../components/form"
import classnames from "classnames"
import {updateUserProfile} from "../../../../../store/Actions/Auth"
import {connect} from "react-redux"
import { useMutation, useLazyQuery} from "@apollo/react-hooks"

import {withRouter} from "react-router-dom"
import {withToastManager} from "react-toast-notifications";
import {UPDATE_PASSWORD} from "../../utils/queries"

const mapStateToProps = (state) => ({newContract: state.newContract})

const mapDispatchToProps = {
    updateUserProfile
}

const Security = (props) => {

    const [state, updateState] = useState({
        params: {
            currentPassword:  "",
            newPassword: "",
            confirmNewPassword: ""
        },
        error: null
     })

     const {toastManager, updateUserProfile} = props

     const [updatePassword] = useMutation(UPDATE_PASSWORD, {onCompleted: async (result) => {
        try {
            
            // use the result to update the variable.
            await updateUserProfile({ ...result.passwordUpdate.path})
            toastManager.add("Password updated Successfully", {
                appearance: "success"
            })
        }catch(err){
            toastManager.add(err.message, {
                appearance:"error"
            })
        }
    }})

    const updateParams = (e) => {
        
        if(e.target.name != "confirmNewPassword"){
            const newState = {
                ...state,
                params: {
                    ...state.params,
                    [e.target.name]: e.target.value
                }
            }
            updateState(newState)
        }else{
            
            const error = e.target.value === state.params.newPassword ? false: true
            
            const newState = {
                ...state,
                params: {
                    ...state.params,
                    [e.target.name]: e.target.value
                },
                error: error
            }
            updateState(newState)
        }
    }

    const updatePasswordCall = async () => {
        await updatePassword({
            variables: {
                ...state.params
            }
        })
    }


    return (
        <div className="col-md-9 col-12">
            <div className="card">
                <div className="card-header border-0">
                    <div className="card-header-title mt-3">
                        <h2 className="font-weight-bold">Security</h2>
                        <small>Secure your account</small>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row mt-4">
                        <div className="col-12 form-group">
                            <label>Old password</label>
                            <input className="form-control" type="password" name="currentPassword" onChange={updateParams}/>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-12 form-group">
                            <label>New Password</label>
                            <input className={classnames({'form-control': true, 'is-valid': state.error === false, 'is-invalid': state.error === true})} type="password"  name="newPassword" onChange={updateParams}/>
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-12 form-group">
                            <label>Confirm password</label>
                            <input className={classnames({'form-control': true, 'is-valid': state.error === false, 'is-invalid': state.error === true})} type="password"  name="confirmNewPassword" onChange={updateParams}/>
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <div className="row">
                        <div className="col-auto">
                            {/* <button className="btn btn-light">
                                <span className="fe fe-chevron-left"></span> Back 
                            </button> */}
                        </div>
                        <div className="col">

                        </div>
                        <div className="col-auto">
                            <button className="btn btn-primary" disabled={!(state.params.confirmNewPassword !== "" && state.params.currentPassword !== "" && state.params.newPassword !=="")} onClick={updatePasswordCall}>
                                Save <span className="fe fe-chevron-right"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    )
}


export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(Security)))