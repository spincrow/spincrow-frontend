import React, { Component, useState, useEffect} from "react";
import Form from "../../../../../components/form"
import classnames from "classnames"
import {updateContract} from "../../../../../store/Actions/Contract"
import {connect} from "react-redux"
import {withRouter, useHistory} from "react-router-dom"
import {addToast, withToastManager} from "react-toast-notifications"
import {useMutation} from "@apollo/react-hooks"
import gql from "graphql-tag";

// construct the graphql query
const CREATE_CONTRACT = gql`
    mutation createContract(
            $name: String!, 
            $amount: Float!, 
            $client: String!, 
            $client_email: String!, 
            $client_phone: String!, 
            $description: String, 
            $duration_start: String!, 
            $duration_end: String!, 
            $owner_type: String!, 
            $payment_type: String!, 
            $spread_type: String!, 
            $periodic_amount: Float!, 
            $whole_amount: Float!, 
            $periodic_type: String!, 
            $milestones: [MileStoneInput!]
        ) {
        createContract (input: {
            name: $name,
            amount: $amount,
            client: $client,
            client_email: $client_email,
            client_phone: $client_phone
            duration_start: $duration_start,
            description: $description,
            duration_end: $duration_end,
            owner_type: $owner_type,
            payment_type: $payment_type,
            spread_type: $spread_type,
            periodic_amount: $periodic_amount,
            whole_amount: $whole_amount,
            periodic_type: $periodic_type,
            milestones: $milestones
        }){
            name
            amount
            created_at
        }
    }
`;

const mapStateToProps = (state) => ({newContract: state.newContract, createContractMutation: CREATE_CONTRACT})

const mapDispatchToProps = {
    updateContract
}

const SellerInfo  = (props) => {

    const [state, updateState] = useState({
        client: "",
        client_email:"",
        client_phone: ""
    })

    useEffect(() => {
        updateState({
            ...state,
            ...props.newContract
        })
    }, []);

    const updateInternalState = (e) => {
        updateState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    // defining all props and axillary methods to import
    const [createContract, {data, loading}] = useMutation(props.createContractMutation);
    let history = useHistory();
    const {toastManager} = props

    const nextMove = async () => {
        try {
            let resp = await createContract({variables: {...state}})
            if(resp){
                toastManager.add("Successfully created contract", {
                    appearance: 'success',
                    autoDismiss: true
                })
                // 
                history.push('/contracts')
            }
        } catch (e) {
            // toastManager.add("Something went wrong", {
            //     appearance: "error", 
            //     autoDismiss: true
            // })
        }
    }


    return (
        <div className="col-9">
            <div className="card">
                <div className="card-header border-0">
                    <div className="card-header-title mt-3">
                        <h2 className="font-weight-bold">Client Information</h2>
                        <small>Enter information about this</small>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-12 form-group">
                            <label> Client name</label>
                            <input name="client" value={state.client} onChange={updateInternalState} className="form-control"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 form-group">
                            <label> Client email</label>
                            <input name="client_email" value={state.client_email} onChange={updateInternalState} className="form-control"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 form-group">
                            <label> Client phone no</label>
                            <input name="client_phone" value={state.client_phone} onChange={updateInternalState} className="form-control"/>
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <div className="row">
                        <div className="col-auto">
                            <button className="btn btn-light">
                                <span className="fe fe-chevron-left"></span> Back 
                            </button>
                        </div>
                        <div className="col">

                        </div>
                        <div className="col-auto">
                            <button className="btn btn-primary" onClick={nextMove}>
                                Create Contract <span className="fe fe-chevron-right"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    )
}

export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(SellerInfo)))