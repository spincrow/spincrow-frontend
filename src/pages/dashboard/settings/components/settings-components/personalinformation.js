import React, { Component, useState, useEffect } from "react";
import Form from "../../../../../components/form"
import {updateUserProfile} from "../../../../../store/Actions/Auth"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom"
import { useMutation, useLazyQuery} from "@apollo/react-hooks"
import classnames from "classnames"
import Uploader from "../ImageUploader"
import { withToastManager} from "react-toast-notifications";
import {UPLOAD_PROFILE_PIC, UPDATE_PROFILE_INFO} from "../../utils/queries"
import PhoneInput from 'react-phone-input-2'

const mapStateToProps = (state) => ({newContract: state.newContract, currentUser: state.currentUser})

const mapDispatchToProps = {
    updateUserProfile
}

const PersonalInfo = (props) =>{
    const [state, updateState] = useState({
       params: {
           email: props.currentUser.email || "",
           first_name: props.currentUser.first_name || "",
           last_name: props.currentUser.last_name || "",
           phone: props.currentUser.phone || "",
       }
    })

    // constructor(props){
    //     super(props);
    //     updateParams = updateContract.bind(
    //     nextMove = nextMove.bind(
    // }

    const {toastManager, updateUserProfile} = props


    const [uploadProfileImage] = useMutation(UPLOAD_PROFILE_PIC, {onCompleted: async (result) => {
        try {
            
            // use the result to update the variable.
            await updateUserProfile({profile_image: result.uploadProfilePic.path})
            toastManager.add("Profile Image uploaded Successfully", {
                appearance: "success"
            })
        }catch(err){
            toastManager.add("Something went wrong, we are working on it", {
                appearance:"error"
            })
        }
    }})

    const [updateProfileInfo] = useMutation(UPDATE_PROFILE_INFO, {onCompleted: async (result) => {
        try {
            
            // use the result to update the variable.
            await updateUserProfile({...result.updateProfile})
            toastManager.add("Profile uploaded Successfully", {
                appearance: "success"
            })
        }catch(err){
            toastManager.add("Something went wrong, we are working on it", {
                appearance:"error"
            })
        }
    }})

    const updateImage = async (uploadedImage) => {
        
        await uploadProfileImage({
            variables: {
                file: uploadedImage
            }
        })
    }

    const updateProfile = async () => {
        await updateProfileInfo({
            variables: {
                ...state.params
            }
        })
    }

    const updateParams = (e) => {
        
        const newState = {
            ...state,
            params: {
                ...state.params,
                [e.target.name]: e.target.value
            }
        }
        updateState(newState)
    }

    const nextMove = () =>{
        props.updateContract({...state}).then(resp => {
            props.history.push('payment-structure')
        })
    }

    return (
        <div className="col-md-9 col-12">
            <div className="card">
                <div className="card-header border-0">
                    <div className="card-header-title mt-3">
                        <h2 className="font-weight-bold">Personal Information</h2>
                        <small>Edit and review your personal information</small>
                    </div>
                </div>
                <div className="card-body">
                    <Uploader currentImage={props.currentUser.profile_image} uploadHandler={updateImage}/>

                    <div className="row mt-5">
                        <div className="col-6 form-group">
                            <label>First name</label>
                            <input value={state.params.first_name} className="form-control" name="first_name" onChange={updateParams} />
                        </div>
                        <div className="col-6 form-group">
                            <label>Last name</label>
                            <input value={state.params.last_name} className="form-control" name="last_name" onChange={updateParams} />
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-12 form-group">
                            <label>Email</label>
                            <input disabled value={state.params.email} className="form-control" name="email" onChange={updateParams} />
                        </div>
                    </div>
                    <div className="row mt-4">
                        <div className="col-12 form-group">
                            <label>Phone number</label>
                            <PhoneInput
                                containerClass=""
                                inputClass="form-control"
                                inputStyle={{width: '100%'}}
                                country={'ng'}
                                value={state.params.phone}
                                onChange={phone => updateState({ 
                                    ...state,
                                    params: {
                                        ...state.params,
                                        phone: `+${phone}`
                                    }
                                })}
                            />
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <div className="row">
                        <div className="col-auto">
                            {/* <button className="btn btn-light">
                                <span className="fe fe-chevron-left"></span> Back 
                            </button> */}
                        </div>
                        <div className="col"></div>
                        <div className="col-auto">
                            <button className="btn btn-primary" onClick={updateProfile}>
                                Save <span className="fe fe-save"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    )
}

export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(PersonalInfo)))