import React, { Component, Fragment, useState, useEffect } from "react";
import Form from "../../../../../components/form"
import {Link} from "react-router-dom"
import classnames from "classnames"
import {useLazyQuery, useQuery, useMutation} from "@apollo/react-hooks"
import {withRouter} from "react-router-dom"
import {GET_CARDS, GET_BANK_ACCOUNT, DELETE_BANK_ACCOUNT, DELETE_CARD} from "../../utils/queries"
import {withToastManager} from "react-toast-notifications"
import {connect} from "react-redux"
import Empty from "../../../../../components/empty"
import BankComponent from "../../../../../components/bank-component"
import CardComponent from "../../../../../components/card-component"
import CardLoaderElement from "../../../../../components/card-loading-element";

// actions
import {getCards, deleteCard} from "../../../../../store/Actions/Card"
import {getBank, deleteAccount} from "../../../../../store/Actions/Bank"

const mapStateToProps = (state) => ({currentUser: state.currentUser, cards: state.cards, bankaccounts: state.bankaccounts})
const mapActionToProps = {
    getCards: getCards,
    getBank: getBank,
    deleteCard,
    deleteAccount
}

 const  Payments = (props) =>  {
    const [state, updateState] = useState({
        loading: true,
        cards: [],
        bankaccounts: [],
        active: true
    })

    const {toastManager, currentUser, location} = props

    const [getCardsQuery,{loading, error, data}] = useLazyQuery(GET_CARDS, {onCompleted :async (results) => {
        
        updateState({
            ...state,
            loading:false,
            cards: results.cards
        })
        await props.getCards({cards: results.cards})
    }})

    
    const [bankAccountQuery, {loading: bankLoading}] = useLazyQuery(GET_BANK_ACCOUNT, {onCompleted : async (results) => {
        
        updateState({
            ...state,
            loading:false,
            bankaccounts: results.bankaccounts
        })
        await props.getBank({bankaccounts: results.bankaccounts})
    }})

    const [deleteBankMutation, {loading: deleteBankLoading}] = useMutation(DELETE_BANK_ACCOUNT, {onCompleted: async (result) => {
        props.deleteAccount({bankaccount: result.deleteBankAccount})
        updateState(state => ({
            ...state,
            bankaccounts: state.bankaccounts.filter(item => item.id != result.deleteBankAccount.id)
        }))
        toastManager.add("Successfully deleted bank account", {
            appearance: "success",
            autoDismiss: true
        })
    }});

    const [deleteCardMutation, {loading: deleteCardLoading}] = useMutation(DELETE_CARD, {onCompleted: async (result) => {
        props.deleteCard({card:result.deleteCard})
        updateState(state => ({
            ...state,
            cards: state.cards.filter(item => item.id != result.deleteCard.id)
        }))
        toastManager.add("Successfully deleted card", {
            appearance: "success",
            autoDismiss: true
        })
    }});

    const deleteBank = async (bankId) => {
        await deleteBankMutation({variables: {id: bankId}})
    }

    const deleteCard = async (cardId) => {
        await deleteCardMutation({variables: {id: cardId}})
    }

    const updateActive = (type) =>{
        switch (type) {
            case "cards":
                updateState({
                    ...state,
                    active: true
                })
                break;
            case "banks":
                updateState({
                    ...state,
                    active: false
                })
                break;
            default:
                break;
        }
    }

    const getCalls = async () => {
        
        await getCardsQuery()
        await bankAccountQuery()
    }

    useEffect(() => {
        
        if(props.cards.length > 0 && props.bankaccounts.length > 0){
            updateState({
                ...state,
                cards: props.cards,
                bankaccounts: props.bankaccounts
            })
        }else{
            getCalls()
        }
    }, [])

    return (
        <div className="col-12 col-md-9">
            <div className="card">
                <div className="card-header border-0">
                    <div className="card-header-title mt-3">
                        <h2 className="font-weight-bold">Payments</h2>
                        <small>How do you want to pay or get paid?</small>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col">
                            <ul className="nav nav-tabs nav-tabs-sm">
                                <li className="nav-item">
                                    <a onClick={()=>updateActive("cards")} className={classnames({'nav-link pointer text-center': true, 'active': state.active})}>
                                    Cards
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={() => updateActive("banks")} className={classnames({'nav-link pointer text-center': true, 'active': !state.active})}>
                                        Bank
                                    </a>
                                </li>
                                <li className="nav-item ml-auto">
                                    {
                                        state.active ? <Link to="payments/add-card" className="nav-link text-center text-primary">
                                        + Add new card
                                        </Link>: <Link to="payments/add-bank" className="nav-link text-center text-primary">
                                            + Add new bank
                                        </Link>
                                    }
                                </li>
                            </ul>
                        </div>
                    </div>
                    {
                        state.active ? 
                        <div className="row mt-3">
                        {
                            <>
                                {
                                    !loading ? <>
                                            <div className="col-12 col-md-6">
                                                <Link to="payments/add-card">
                                                    <div className="zoom card bg-light justify-content-center text-center" style={{height: 150}}>
                                                        <p className="text-center mt-4">Add your card</p>
                                                        <button className="btn rounded-circle btn-light">+</button>
                                                    </div>
                                                </Link>
                                            </div>
                                            {
                                                props.cards.map((item, index) => {
                                                    return (
                                                        <div className="col-md-6 col-12" key={index}>
                                                            <CardComponent key={index} card={item} deleteable={true} deleteCard={deleteCard}/>
                                                        </div>
                                                    )
                                                })
                                            }
                                    </>: 

                                    <>
                                        <CardLoaderElement />
                                    </>
                                }
                            </>
                        }
                    </div> : 
                    <div className="row mt-3">
                        {
                            <>
                                {
                                    !loading ? 
                                    <>
                                        {
                                            <>
                                                 <div className="col-12 col-md-6">
                                                    <Link to="payments/add-bank">
                                                        <div className="zoom card bg-light justify-content-center text-center" style={{height: 150}}>
                                                            <p className="text-center mt-4">Add your bank</p>
                                                            <button className="btn rounded-circle btn-light">+</button>
                                                        </div>
                                                    </Link>
                                                </div>
                                                {
                                           
                                                        props.bankaccounts.map((item, index) => {
                                                            return (
                                                                <div className="col-md-6 col-12" key={index} >
                                                                    <BankComponent key={index} bankaccount={item} deleteable={true} deleteBank={deleteBank} />
                                                                </div>
                                                            )
                                                        })
                                                   
                                                }
                                            </>
                                        }
                                    </>:
                                    <>
                                        <div className="row">
                                            <div className="col-md-12 d-flex justify-content-center">
                                                <div className="spinner-border mt-5" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                            </>
                        }
                    </div> 
                    }
                </div>
            </div>
        </div>   
    )
}

export default withToastManager (withRouter( connect(mapStateToProps, mapActionToProps)(Payments)))