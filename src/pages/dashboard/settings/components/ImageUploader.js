import React, {Component} from "react"

export default class Upload extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        file: null
      }
      this.handleChange = this.handleChange.bind(this)
    }
    handleChange(event) {
      this.setState({
        file: URL.createObjectURL(event.target.files[0])
      })
    //   start the image upload call.
      if(this.props.uploadHandler) {
          this.props.uploadHandler(event.target.files[0])
      }  
    }
    render() {
      return (
        <>
            <div className="row">
                <div className="col">
                    <div className="row">
                        <div className="col-12 d-flex">
                            <div className="m-auto">
                                <div className="avatar rounded-circle avatar-xl m-auto text-center bg-light">
                                    <label for="file-input" className="avatar rounded-circle avatar-xl m-auto text-center bg-light">
                                    {
                                        this.state.file ? (
                                            <img src={this.state.file} alt="..." class="avatar-img rounded-circle" />
                                        ): (
                                            <>
                                            {
                                                this.props.currentImage ? <img src={this.props.currentImage} alt="..." class="avatar-img rounded-circle" />: <i className="fe fe-image"></i>
                                            }
                                            </>
                                        )
                                        
                                        }
                                    </label>
                                    <input id="file-input" type="file" style={{display: 'none'}} onChange={this.handleChange} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <p className="text-center">Click to upload image</p>
                </div>
            </div>
        </>
      );
    }
  }