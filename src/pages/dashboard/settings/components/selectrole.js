import React, { Component, Fragment } from "react";
import {Link} from "react-router-dom"
import {connect} from "react-redux"
import {updateContract} from "../../../../../store/Actions/Contract" 
import {withRouter} from "react-router-dom"


const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    updateContract
}

class SelectRole extends Component{

    constructor(props){
        super(props);
    }


    render() {
        return (
            <Fragment>
                <div className="col-12">
                    <div className="row mt-5">
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12 text-center">
                                    <h2 className="font-weight-bold">Please select your role</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 mt-3 col-md-8">
                    <div className="row justify-content-between">
                        <div className="col-5">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-12 d-inline-flex">
                                            <img className="mx-auto" src={require('../../../../assets/img/Buyers.svg')} />
                                        </div>
                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-12 text-center">
                                            <h3>Buyers</h3>
                                        </div>
                                        <div className="col-12 text-center mt-3">
                                            <p className="small text-secondary">Are you buying goods or services to an individual or organization? Simply follow the steps to create your contract.</p>
                                        </div>
                                        <div className="col-12 text-center">
                                            <span onClick={() => this.props.updateContract({owner_type: "buyer"}).then(() => {
                                                this.props.history.push('new/basic-information')
                                            })} className="text-primary">
                                                {/* <Link to="new/basic-information"> */}
                                                    Get started <i className="fe fe-arrow-right"></i>
                                                {/* </Link> */}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-5">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-12 d-inline-flex">
                                            <img className="mx-auto" src={require('../../../../assets/img/Sellers.svg')} />
                                        </div>
                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-12 text-center">
                                            <h3>Sellers</h3>
                                        </div>
                                        <div className="col-12 text-center mt-3">
                                            <p className="small text-secondary">Are you buying goods or services to an individual or organization? Simply follow the steps to create your contract.</p>
                                        </div>
                                        <div className="col-12 text-center">
                                            <span onClick={() => this.props.updateContract({owner_type: "seller"}).then(() => {
                                                this.props.history.push('new/basic-information')
                                            })} className="text-primary">
                                                {/* <Link to="new/basic-information"> */}
                                                    Get started <i className="fe fe-arrow-right"></i>
                                                {/* </Link> */}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SelectRole))