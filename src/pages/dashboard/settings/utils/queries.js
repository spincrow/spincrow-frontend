import gql from "graphql-tag"

export const UPLOAD_PROFILE_PIC = gql`
    mutation uploadProfilePic($file: Upload) {
        uploadProfilePic(file: $file) {
            id
            path
            filename
            mimetype
            encoding
        }
    }
`

export const UPDATE_PROFILE_INFO = gql`
    mutation updateProfile($first_name: String, $last_name: String, $phone: String) {
        updateProfile(input: {
            first_name: $first_name,
            last_name: $last_name,
            phone: $phone
        }) {
            first_name
            last_name
            email
            phone
            profile_image
            id
        }
    }
`

export const RESOLVE_ACCOUNT = gql`
    mutation resolveBankAccount($account_no:String!, $account_bank:String!){
        resolveBankAccount(input:{
            account_no: $account_no,
            account_bank:$account_bank
        }){
            account_name
            account_number
        }
    }
`;

export const UPDATE_PASSWORD = gql`
    mutation passwordUpdate($currentPassword: String!, $newPassword: String!){
        passwordUpdate(input: {
            currentPassword: $currentPassword,
            newPassword: $newPassword
        }){
            first_name
            last_name
            email
            phone
            profile_image
            id
        }
    }
`

export const ADD_CARD = gql`
     mutation addCard($txRef: String!){
        addCard(input: {
            txRef: $txRef
        }){
            id
            token
            last_number
            type
            expirymonth
            expiryyear
        }
    }
`

export const GET_CARDS = gql`
    query {
        cards {
            id
            card_name
            type
            expirymonth
            expiryyear
            last_number
        }
    }
`

export const GET_BANK_ACCOUNT = gql`
    query {
        bankaccounts {
            id
            account_number
            account_name
            bank_name
            owner {
                first_name
                last_name
            }
            bank_code
        }
    }
`

export const ADD_BANK_ACCOUNT = gql`
    mutation addBankAccount($account_number: String!, $bank_name: String!, $account_name: String!, $bank_code: String){
        addBankAccount(input: {
            account_number:$account_number,
          	bank_name:$bank_name,
            account_name: $account_name,
          	bank_code: $bank_code
        }){
            id
        		account_number
      			bank_name
        		bank_code
        		owner{
              first_name
              last_name
              id
            }
        }
    }
`;

export const DELETE_CARD = gql`
    mutation deleteCard($id:ID!){
        deleteCard(input: $id){
            id
            card_name
            token
            expiryyear
            expirymonth
            last_number
            type
        }
    }
`;

export const DELETE_BANK_ACCOUNT = gql`
    mutation deleteBankAccount($id: ID!){
        deleteBankAccount(input: $id){
            id
            account_number
            bank_name
            bank_code
        }
    }
`;

export const BANK_CODES = gql`
    query BankCodes{
        bankcodes{
            bankname
            bankcode
            internetbanking
        }
    }
`