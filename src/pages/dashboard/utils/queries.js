import gql from "graphql-tag"

export const UPDATE_HOME_TOUR = gql`
    mutation homeTourUpdate{
        homeTourUpdate {
            first_name
            last_name
            email
            phone
            profile_image
            wallet {
                amount
                id
            }
            home_tour
            contract_tour
            single_contract_tour
            settings_tour
            wallet_tour
        }
    }
`

export const UPDATE_CONTRACTS_TOUR = gql`
    mutation contractsTourUpdate{
        contractsTourUpdate {
            first_name
            last_name
            email
            phone
            profile_image
            wallet {
                amount
                id
            }
            home_tour
            contract_tour
            single_contract_tour
            settings_tour
            wallet_tour
        }
    }
`

export const UPDATE_SINGLE_CONTRACT_TOUR = gql`
    mutation singleContractTourUpdate{
        singleContractTourUpdate {
            first_name
            last_name
            email
            phone
            profile_image
            wallet {
                amount
                id
            }
            home_tour
            contract_tour
            single_contract_tour
            settings_tour
            wallet_tour
        }
    }
`

export const UPDATE_WALLET_TOUR = gql`
    mutation walletTourUpdate{
        walletTourUpdate {
            first_name
            last_name
            email
            phone
            profile_image
            wallet {
                amount
                id
            }
            home_tour
            contract_tour
            single_contract_tour
            settings_tour
            wallet_tour
        }
    }
`