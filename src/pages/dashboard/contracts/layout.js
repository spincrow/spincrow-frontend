import React, {Component, useState, useEffect} from "react"
import {useHistory, Link} from "react-router-dom"
import Header from "../../../components/header"
import {useQuery, useLazyQuery, useMutation} from "@apollo/react-hooks"
import classnames from "classnames"
import ContractView from "./components/contractsView"
import CardLoadingElement from "../../../components/card-loading-element"
import {Spring} from "react-spring/renderprops"

// Tour guide
import TourGuide from "../../../components/tour-guide"
// import {addToast} from "react-toast-notifications"
// import "./confirm.scss"
const image = require('../../../assets/img/empty.svg')


const tourConfig = [
    {
        selector: `[data-tut="no_one"]`,
        content: () => (
            <div className="text-center">
                <h1>Let's introduce you to contracts</h1>

                <p>Here you get a quick grid of your contracts based on their states, let's take you around</p>
            </div>
        ),
        style: {
            minWidth: 400
        }
    },
    {
      selector: '[data-tut="reactour_contract_1"]',
      content: () => (
            <div className="text-center">
                <h1>Create your contracts</h1>
                <p>Quickly create your contracts as a buyer or seller</p>
            </div>
        )
    },
    {
      selector: '[data-tut="reactour_contract_2"]',
      content: () => (
            <div className="text-center">
                <h1>View all your contracts</h1>
                <p>Here you can see all of your contracts</p>
            </div>
        )
    },
    {
      selector: '[data-tut="reactour_contract_20"]',
        content: () => (
            <div className="text-center">
                <h1>Your non funded contracts</h1>
                <p>Here you can view all your contracts which haven't been funded.</p>
            </div>
        )
    },
    {
      selector: '[data-tut="reactour_contract_21"]',
      content: () => (
        <div className="text-center">
            <h1>Your active contracts</h1>
            <p>Here you can see all your contracts which are currently active</p>
        </div>
      )
    },
    {
      selector: '[data-tut="reactour_contract_3"]',
      content: () => (
        <div className="text-center">
            <h1>Your completed contracts</h1>
            <p>View all your contracts that have been completed</p>
        </div>
      )
    },
    {
      selector: '[data-tut="reactour_contract_4"]',
      content: () => (
            <div className="text-center">
                <h1>Contracts grid</h1>
                <p>Here you can see a grid view of all of your contracts in this section</p>
            </div>
        )
    }
];

const Layout = (props)  => {
    
    const [state, updateState ] = useState({
        contracts: [],
        loading: false,
        viewState: "all",
        isTourOpen: false,
        isShowingMore: false,
    })

    const closeTour = async () => {
        updateState({ ...state, isTourOpen: false });
        await updateContractTourMutation()
        // send a request to update HomePage tour for user
    }

    const openTour = () => {
        
        updateState(state => ({ ...state, isTourOpen: true }));
    }

    const toggleShowMore = () => {
        updateState(state => ({
            ...state,
            isShowingMore: !state.isShowingMore
        }));
    }
    
    let history = useHistory()


    const [ fetchContracts, {loading: fetchContractLoading} ] = useLazyQuery(props.getContractQuery, { variables: {}, onCompleted: data => {
        
        updateState({
            ...state,
            contracts: data.contracts,
            loading: false
        })
        props.getContracts(data.contracts)
    }});

    const [updateContractTourMutation, {loading: updateContractTourQueryLoading}] = useMutation(props.updateContractTourQuery, {
        onCompleted: async (data) => {
            props.updateUser(data.contractsTourUpdate)
        }
    })

    const fetchState = async () => {
        await fetchContracts()
    }

    const updateTourState = () => {
        setTimeout(() => {
            openTour()
        }, 500)
    }

    useEffect(() => {
        if(props.contracts.length < 1){
            fetchState()
        }else{
            updateState({
                ...state,
                contracts: props.contracts
            })
        }

        updateTourState()
    },[props])


    const renderInactiveView = () => {
        return <>
                <div className="row" data-tut="reactour_contract_4">
                    <>
                        <div className="col-12 col-md-4">
                            <Link to="/contracts/new">
                                <div className="card zoom border bg-transparent" style={{minHeight: 198}}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col mt-4">
                                                <div style={{justifyContent: "center", display: "flex"}} className="mb-1">
                                                    <button className="btn btn-lg btn-rounded-circle btn-white">+</button>
                                                </div>
                                                <h3 className="text-center">
                                                    Create a contract
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </>
                    {
                        [...state.contracts].reverse().filter(item => item.status == "INACTIVE").map((contract, index) => {
                            return <ContractView contract={contract} contractAmount ={contract.amount} key={index}/>
                        })
                    }
                </div>
            </>
    }

    const renderAllView = () => {
        return <>
                <div className="row" data-tut="reactour_contract_4">
                    <>
                        <div className="col-12 col-md-4">
                            <Link to="/contracts/new">
                                <div className="card zoom border bg-transparent" style={{minHeight: 198}}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col mt-4">
                                                <div style={{justifyContent: "center", display: "flex"}} className="mb-1">
                                                    <button className="btn btn-lg btn-rounded-circle btn-white">+</button>
                                                </div>
                                                <h3 className="text-center">
                                                    Create a contract
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </>
                    {
                        [...state.contracts].reverse().map((contract, index) => {
                            return <ContractView contract={contract} contractAmount = {contract.amount} key={index}/>
                        })
                    }
                </div>
            </>
    }

    const renderActiveView = () => {
        return <>
                {
                    [...state.contracts].filter(item => item.status == "ACTIVE").length > 0 ?
                        <div className="row">
                            {
                                [...state.contracts].reverse().filter(item => item.status == "ACTIVE").map((contract, index) => {
                                    return <ContractView contract={contract} contractAmount={contract.amount} key={index}/>
                                })
                            } 
                        </div>: 
                        <div className="card-body">
                            <div className="row">
                                <div className="col-12 justify-content-center d-flex">
                                    <div className="d-block">
                                        <img src={image} width="140" className="d-block"/>
                                        <p className="text-center">No contracts available</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </>
    }
    
    const renderCompletedView = () => {
        return  <>
                {
                    [...state.contracts].filter(item => item.status == "COMPLETED").length > 0 ?
                        <div className="row">
                            {
                                [...state.contracts].reverse().filter(item => item.status == "COMPLETED").map((contract, index) => {
                                    return <ContractView contract={contract} contractAmount = {contract.amount} key={index}/>
                                })
                            } 
                        </div>: 
                        <div className="card-body">
                            <div className="row">
                                <div className="col-12 justify-content-center d-flex">
                                    <div className="d-block">
                                        <img src={image} width="140" className="d-block"/>
                                        <p className="text-center">No contracts available</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </>
    }

    const renderViewSet = () => {
        switch (state.viewState) {
            case "all":
                return renderAllView()
                break;
            case "inactive":
                return renderInactiveView()
                break;
            case "active":
                return renderActiveView()
                break;
            case "completed":
                return renderCompletedView()
                break;

            default:
                return null
                break;
        }
    }
    // 

    return(
        <React.Fragment>
            {
                props.user.contract_tour? null : 
                <TourGuide
                    closeTour={closeTour}
                    tourConfig={tourConfig}
                    isTourOpen={state.isTourOpen}
                />
            }
             {/* <Header /> */}
             <div className="container mt-3">
                <Spring from={{ opacity: 0, marginTop: -1000 }} to={{ opacity: 1, marginTop: 0 }}>
                    {
                        styles => (
                            <div style={styles}>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="header">
                                            <div className="header-body">
                                                <div className="row align-items-end">
                                                    <div className="col">
                                                        <h1 className="header-title">
                                                            Contracts
                                                        </h1>
                                                        
                                                    </div>
                                                    <div className="col-auto" data-tut="reactour_contract_1">
                                                        
                                                        <a href="/contracts/new" className="btn btn-primary">
                                                            Create new contract
                                                        </a>
                
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="d-md-block d-none col-12">
                                                        <ul className="nav nav-tabs header-tabs">
                                                            <li className="nav-item pointer" data-tut="reactour_contract_2">
                                                                <a onClick={() => updateState({...state, viewState: "all"})} className={classnames({'nav-link text-center': true, 'active': state.viewState == "all"})}>
                                                                    All Contracts
                                                                </a>
                                                            </li>
                                                            <li className="nav-item pointer" data-tut="reactour_contract_20">
                                                                <a onClick={() => updateState({...state, viewState: "inactive"})} className={classnames({'nav-link text-center': true, 'active': state.viewState == "inactive"})}>
                                                                    Non-Funded Contracts
                                                                </a>
                                                            </li>
                                                            <li className="nav-item pointer" data-tut="reactour_contract_21">
                                                                <a onClick={() => updateState({...state, viewState: "active"})} className={classnames({'nav-link text-center': true, 'active': state.viewState == "active"})}>
                                                                    Funded Contracts
                                                                </a>
                                                            </li>
                                                            <li className="nav-item pointer" data-tut="reactour_contract_3">
                                                                <a onClick={() => updateState({...state, viewState: "completed"})} className={classnames({'nav-link text-center': true, 'active': state.viewState == "completed"})}>
                                                                    Completed Contracts
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="d-block d-md-none  col-12">
                                                        <ul className="nav nav-tabs nav-tabs-sm header-tabs">
                                                            <li className="nav-item pointer" data-tut="reactour_contract_2">
                                                                <a onClick={() => updateState({...state, viewState: "all"})} className={classnames({'nav-link': true, 'active': state.viewState == "all"})}>
                                                                    All Contracts
                                                                </a>
                                                            </li>
                                                            <li className="nav-item pointer" data-tut="reactour_contract_20">
                                                                <a onClick={() => updateState({...state, viewState: "inactive"})} className={classnames({'nav-link': true, 'active': state.viewState == "inactive"})}>
                                                                    Non-Funded Contracts
                                                                </a>
                                                            </li>
                                                            <li className="nav-item pointer" data-tut="reactour_contract_21">
                                                                <a onClick={() => updateState({...state, viewState: "active"})} className={classnames({'nav-link': true, 'active': state.viewState == "active"})}>
                                                                    Funded Contracts
                                                                </a>
                                                            </li>
                                                            <li className="nav-item pointer" data-tut="reactour_contract_3">
                                                                <a onClick={() => updateState({...state, viewState: "completed"})} className={classnames({'nav-link': true, 'active': state.viewState == "completed"})}>
                                                                    Completed Contracts
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {
                                fetchContractLoading ? (
                                   <>
                                       <div className="row">
                                           <div className="col-md-4 col">
                                               <CardLoadingElement />
                                           </div>
                                           <div className="col-md-4 col">
                                               <CardLoadingElement />
                                           </div>
                                           <div className="col-md-4 col">
                                               <CardLoadingElement />
                                           </div>
                                           <div className="col-md-4 col">
                                               <CardLoadingElement />
                                           </div>
                                           <div className="col-md-4 col">
                                               <CardLoadingElement />
                                           </div>
                                           <div className="col-md-4 col">
                                               <CardLoadingElement />
                                           </div>
                                       </div>
                                   </>
                                ): 
                                <div>
                                    {
                                        renderViewSet()
                                    }
                                </div>
                                }
                            </div>
                        )
                    }
                </Spring>
             </div>
        </React.Fragment>
     )
}

export default Layout;