import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import {getContracts} from "../../../../store/Actions/Contract";
import gql from 'graphql-tag';
import {
    GET_SINGLE_CONTRACT, 
    FUND_CONTRACT_AS_CLIENT, 
    DISBURSE_CONTRACT_MILESTONE_BY_CLIENT,
    APPROVE_RETRACTION_AS_CLIENT,
    REQUEST_RETRACTION_AS_CLIENT,
    CANCEL_RETRACTION_AS_CLIENT
} from "../utils/queries"

const mapStateToProps = (state => {
    return {
        getContractQuery: GET_SINGLE_CONTRACT,
        fundContractAsClient: FUND_CONTRACT_AS_CLIENT,
        approveRetractionAsClientGQL: APPROVE_RETRACTION_AS_CLIENT,
        requestRetractionAsClientGQL: REQUEST_RETRACTION_AS_CLIENT,
        disburseContractMilestoneForClient: DISBURSE_CONTRACT_MILESTONE_BY_CLIENT,
        cancelRetractionAsClientGQL: CANCEL_RETRACTION_AS_CLIENT,
        contracts: state.contracts,
        user: state.currentUser
    }
});

const mapDispatchToProps = {
    getContracts: getContracts,
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))