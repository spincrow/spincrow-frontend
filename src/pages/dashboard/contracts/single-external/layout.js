import React, {Component, useState, useEffect, Fragment} from "react"
import {Link, useHistory, useParams} from "react-router-dom"
import classnames from "classnames"
import Modal from "../components/modal-external"
import ModalWholePay from "../components/modal-whole-pay"
import AccountDetailsModal  from "../components/external-modals/account-details-modal"
import WithdrawFundsModal from "../components/external-modals/withdrawal-funds-modal"
import BankComponent from "../../../../components/bank-component"
import Header from "../../../../components/external-header"
import moment from "moment"
import {useQuery, useLazyQuery, useMutation} from "@apollo/react-hooks"
import cookies from "js-cookie"
import {formatCurrency, momentInTime} from "../../../../utils/helpers"
import {BANK_LIST} from "../utils/queries"
import {generateUUID, integrityValue} from "../../../../utils/helpers"
import {withToastManager} from "react-toast-notifications"
import {sha256} from "js-sha256"
import {Spring} from "react-spring/renderprops"
import DebitSign from "../../../../assets/img/debit-sign.svg"
import CreditSign from "../../../../assets/img/credit-sign.svg"
import TourGuide from "../../../../components/tour-guide"
import ContractLoading from "../../../../components/contract-loading-page"

const image = require('../../../../assets/img/empty.svg')
// define the variables
const FL_PUB = process.env.REACT_APP_RAVE_PUBLIC_KEY
const FL_SEC = process.env.REACT_APP_RAVE_SECRET_KEY
const FL_ENC = process.env.REACT_APP_RAVE_ENCRYPT_KEY

// define the internals


// CONFIGURE THINGS
const Layout = (props)  => {
    
    /**
     * - check localhost fr the code if stored for the contract.
     * - If it exist compare and open the contract.
     * - if it request the user to enter their code and confirm they own the contract.
     * - if it's incorrect keep restricting access.
     * 
     */
    
    let { id } = useParams()
    const {toastManager} = props
    // const {user} = props
    const [state, updateState ] = useState({
        contracts: null,
        contract: {},
        currentMilestone: null,
        contractVisible: false,
        contractDetails: {},
        contractAmount: null,
        bankcodes: [],
        isTourOpen: true,
        isTourAccountOpen: false,
        isShowingMore: false,
    })
    
    let history = useHistory()
    
    const closeTour = () => {
        
        if(!state.contract.client_account_no){
            updateState({ ...state, isTourOpen: false, isTourAccountOpen: true });
        }else{
            updateState({ ...state, isTourOpen: false});
        }
        // updateHomeTour()
        // send a request to update HomePage tour for user
    }
    
    const closeTourAccount = () => {
        
        updateState({ ...state, isTourAccountOpen: false});
        // updateHomeTour()
        // send a request to update HomePage tour for user
    }
    
    const openTour = () => {
        updateState(state => ({ ...state, isTourOpen: true }));
    }

    const [requestRetractionAsClientMutation, {loading: retractionRequestLoading, data: retractionRequestData, error: retractionRequestError}] = useMutation(props.requestRetractionAsClientGQL, {onCompleted: async (response) => {
        updateState({
            ...state,
            contract: response.requestRetractionAsClient
        })
        
        toastManager.add("Retraction requested 🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
    }})
    
    const [approveRetractionAsClientMutation, {loading: retractionApproveLoading, data: retractionApproveData, error: retractionApproveError}] = useMutation(props.approveRetractionAsClientGQL, {onCompleted: async (response) => {
        updateState({
            ...state,
            contract: response.approveRetractionAsClient
        })
        // toastManager.add("Retract🎉 ", {
            //     appearance: 'success',
            //     autoDismiss: true,
            
            // })
        }})
        
        const [cancelRetractionAsClientMutation, {
            loading: cancelRetractionLoading,
            data: cancelRetractionData,
            error: cancelRetractionError
        }] = useMutation(props.cancelRetractionAsClientGQL, {
            onCompleted: async (response) => {
                updateState({
                    ...state,
                    currentContract: response.cancelRetractionAsClient
                })
                toastManager.add("Successfully cancelled the retraction 🎉 ", {
                    appearance: 'success',
                    autoDismiss: true,
                })
            }
        })
        
        /******************
         *  GraphQL based calls
         ******************/
        const [ fetchContract, {loading: gettingContractLoading} ] = useLazyQuery(props.getContractQuery, { variables: {id: id}, onCompleted: data => {
            
            // compare the user's contracts with localstorage if it exists
            let contracts_code = localStorage.getItem('contract_code')
            let contractToCode = null
            if(data.contract){
                let contractAmount = data.contract.amount
                // for (let i in  data.contract.milestones) {
                    //     contractAmount = contractAmount +  data.contract.milestones[i].target;
                    // }
                    if(contracts_code){
                        contractToCode = contracts_code.split('-')
                        if(contractToCode[0] == data.contract.id){
                            updateState({
                                ...state,
                                contract: data.contract,
                                isTourOpen: data.contract.client_account_no ? false : true,
                                contractAmount: contractAmount,
                                contractVisible: contractToCode[1].toString() == data.contract.client_code ? true : false
                    })
                }else{
                    updateState({
                        ...state,
                        contractAmount: contractAmount,
                        isTourOpen: data.contract.client_account_no ? false : true,
                        contract: data.contract
                    })
                }
            }else{
                updateState({
                    ...state,
                    contractAmount: contractAmount,
                    isTourOpen: data.contract.client_account_no ? false : true,
                    contract: data.contract
                })
            }
        }else{
            toastManager.add("This contract doesn't exist.", {
                appearance: "error",
                autoDismiss: true
            })
            setTimeout(() => {
                history.push('/login')
            }, 1000)
        }
    }});
    
    const {data:bankcodes, loading: bankcodesLoading } = useQuery(BANK_LIST, {
        onCompleted: (data) => {
            // use the result
            updateState({
                ...state,
                bankcodes: data.bankcodes
            })
        }
    })
    
    // updating the contract's status
    const [ updateContractStatusMutation] = useMutation(props.fundContractAsClient, {
        onCompleted: (result) => {
            
            updateState({
                ...state,
                contract: result.fundContractAsClient,
                contractAmount: result.fundContractAsClient.amount
            })
        }
    })
    
    const [disburseContractMilestoneForClient] = useMutation(props.disburseContractMilestoneForClient, {
        onCompleted: (result) => {
            updateState({
                ...state,
                contract: result.disburseContractMilestoneForClient
            })
        }
    })
    
    /******************
     *  END OF GraphQL based calls
     ******************/
    const updateDetails = (e) => {
        // contract details
        updateState({
            ...state,
            contractDetails: {
                ...state.contractDetails,
                [e.target.name]: e.target.value
            }
        })
    }
    
    const confirmContractDetails = () => {
        if(state.contractDetails.email == state.contract.client_email && state.contractDetails.code == state.contract.client_code){
            updateState({
                ...state,
                contractVisible: true
            })
            // store the code
            let code = `${id}-${state.contractDetails.code}`
            localStorage.setItem('contract_code', code);
        }else{
            toastManager.add("Something went wrong", {
                appearance: 'warning',
                autoDismiss: true
            })
        }
    }
    
    
    const fetchState = async () => {
        await fetchContract()
    }
    
    useEffect(() => {
        fetchState()
    },[])
    
    const disburseFundsForMilestone = async (item) => {
        try {
            await disburseContractMilestoneForClient({
                variables: {
                    contract_id: id,
                    milestone_id: item.id,
                    disburseStatus: "CONFIRMED",
                    status: "SUCCESSFUL"
                }
            })
            toastManager.add("Successfully disbursed funds", {
                appearance: 'success',
                autoDismiss: true
            })
        } catch (e) {
            toastManager.add("Something went wrong", {
                appearance: 'warning',
                autoDismiss: true
            })
        }
    }
    
    const fundAccount = async () => {
        // add all Milestones.
        // let contractAmount = 0;
        // for(let i in state.contract.milestones){
            //     contractAmount = contractAmount + state.contract.milestones[i].target
            // }
            // var PBFKey = "FLWPUBK-aa82cac8ee08f5bb206f937db274081a-X";
            let transactionCode = generateUUID();
            const integrity_hash = integrityValue({
                PBFPubKey: FL_PUB,
                customer_email: state.contract.client_email,
                customer_firstname: state.contract.client,
                customer_lastname: "",
                custom_description: "Contract Payment",
                custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
                custom_title: "Spincrow Contract funding",
                amount: state.contract.amount,
                customer_phone: state.contract.client_phone,
                country: "NG",
                currency: "NGN",
            txref: transactionCode,
        });
        // 
        window.getpaidSetup({
            PBFPubKey: FL_PUB,
            customer_email: state.contract.client_email,
            customer_firstname: state.contract.client,
            customer_lastname: "",
            custom_description: "Contract Payment",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Spincrow Contract funding",
            amount: state.contract.amount,
            customer_phone: state.contract.client_phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
            integrity_hash: integrity_hash,
            onclose: function() {},
            callback: async function(response) {
                var flw_ref = response.tx.flwRef; // collect flwRef returned and pass to a 					server page to complete status check.
                
                if (
                    response.tx.chargeResponseCode == "00" ||
                    response.tx.chargeResponseCode == "0"
                    ) {
                        // redirect to a success page, payment was successfully done, initiate call to update the current contract base and move value to 
                        
                        try{
                            await updateContractStatusMutation({
                                variables: {
                                    contract_id: id,
                                    contract_state: "ACTIVE",
                                    amount: state.contract.amount,
                                    transactionCode: transactionCode
                                }
                            })
                            toastManager.add("Successfully activated the contract 🎉 ", {
                                appearance: 'success',
                                autoDismiss: true,
                                
                        })
                    }catch(err){
                        toastManager.add("Something went wrong 🎉 ", {
                            appearance: 'error',
                            autoDismiss: true,
                            
                        })
                    }
                } else {
                    // redirect to a failure page.
                }
            }
        });
    }
    
    const updateMilestone = (milestone) => {
        updateState({
            ...state,
            currentMilestone: milestone
        })
    }
    
    const updateContract = (updatedContract) => {
        updateState({
            ...state,
            contract: updatedContract
        })
    }
    
    const milestoneCalls = (milestones) => {
        // get number that are completed
        let realscore = milestones.filter((item) => item.disburseStatus == "CONFIRMED");
        let score = `${realscore.length}/${milestones.length}`
        return score;
    }
    
    const percentageOfSuccess = (milestones) => {
        return (milestones.filter((item) => item.disburseStatus == "CONFIRMED").length / milestones.length) * 100
    }
    
    // Dont tempt yourself
    // const buyerOrSellerView = (owner_type) => {
        //     switch (owner_type) {
            //         case "buyer":
            //             return (<></>);
            //         case "seller":
            //             return (<></>);
            //         default:
            //             return null
            //     }
            // }
            
            const reviewUIForBuyer = (retraction_status, defaultUI) => {
                switch (retraction_status) {
                    case "REQUESTED":
                        return <span> Retraction requested</span>
                        break;
                        case "REJECTED":
                            return <span className="text-danger"> Retraction rejected</span>
                break;
                case "PENDING":
                    return <span className="text-warning"> Retraction approved, payment pending</span>
                    case "SUCCESSFUL":
                        return <span className="text-success"> Retraction successful</span>
                        break;
                        case "INACTIVE":
                            return defaultUI
                            break;
                            default:
                                return defaultUI
                                break;
                            }
                            // if(state.currentContract.owner_type == "buyer"){
                                // }else{
                                    //     return defaultUI
                                    // }
                                }
                                
                                const approveRetraction = async (milestone, approveState = true) => {
                                    try{
                                        if(milestone){
                                            await approveRetractionAsClientMutation({
                                                variables: {
                                                    contract_id: id,
                                                    milestone_id: milestone,
                        approve_status: approveState ? "APPROVED" : "REJECTED"
                    }
                })
            }else{
                await approveRetractionAsClientMutation({
                    variables: {
                        contract_id: id,
                        approve_status: approveState ? "APPROVED" : "REJECTED"
                    }
                })
            }
        }catch(err) {
            // toastManager.add(err.message, {
                //     appearance: "error",
                //     autoDismiss: true
                // })
            }
        }
        
        const cancelRetraction = async (milestone) => {
            try{
                if(milestone){
                    await cancelRetractionAsClientMutation({
                        variables: {
                            contract_id: id,
                            milestone_id: milestone
                        }
                    })
                }else{
                    await cancelRetractionAsClientMutation({
                        variables: {
                            contract_id: id
                        }
                    })
                }
            }catch(err) {
                // toastManager.add(err.message, {
                    //     appearance: "error",
                    //     autoDismiss: true
            // })
        }
    }
    
    const requestRetraction = async (milestone) => {
        try{
            if(milestone){
                await requestRetractionAsClientMutation({
                    variables: {
                        contract_id: id,
                        milestone_id: milestone
                    }
                })
            }else{
                await requestRetractionAsClientMutation({
                    variables: {
                        contract_id: id
                    }
                })
            }
        }catch(err) {
            // toastManager.add(err.message, {
                //     appearance: "error",
                //     autoDismiss: true
                // })
            }
            
        }
        
        const retractionOptionsDropdownBuyer = (milestone, retraction_status) => {
            if(state.contract.status == "COMPLETED"){
                return <>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
        }
        switch (retraction_status) {
            case "INACTIVE":
                if(state.contract.owner_type == "buyer"){
                    return <>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.contract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => requestRetraction(milestone || null)}>
                        <i className="fe fe-credit-card"></i>  Request a retraction 
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
                break;
                case "REQUESTED":
                    if(state.contract.owner_type == "buyer"){
                        return <>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, true)}>
                        <i className="fe fe-check-circle"></i> Approve retraction 
                        </div>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, false)}>
                        <i className="fe fe-x-circle"></i> Reject retraction 
                        </div>
                    </>
                }else if(state.contract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => cancelRetraction(milestone || null)}>
                            <i className="fe fe-trash"></i> Cancel retraction
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
                break;
            case "PENDING":
                case "SUCCESSFUL":
                return <div className="dropdown-item pointer">
                <i className="fe fe-check-circle"></i> Send complaint.
                </div>
            case "REJECTED":
                if(state.contract.owner_type == "buyer"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, true)}>
                        <i className="fe fe-check-circle"></i> Approve retraction 
                        </div>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, false)}>
                        <i className="fe fe-x-circle"></i> Reject retraction 
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.contract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => cancelRetraction(milestone || null)}>
                        <i className="fe fe-trash"></i> Cancel retraction
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
                default:
                    if(state.contract.owner_type == "buyer"){
                        return <>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.contract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => requestRetraction(milestone || null)}>
                        <i className="fe fe-credit-card"></i>  Request a retraction 
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
                break;
            }
        }
        
        
        const determineVisibility = () => {
            if( !state.contractVisible && gettingContractLoading)
            return <ContractLoading />;
            if( !state.contractVisible && !gettingContractLoading)
            return  <>
                    <div className="container mt-3">
                        <div className="row justify-content-center">
                            <div className="col-md-5 col-12 mt-5">
                                <div className="card">
                                    <div className="card-header">
                                        <div className="card-header-body">
                                            <div className="row align-items-end">
                                                <div className="col">
                                                    <small className="header-pretitle">CONTRACT NAME</small>
                                                    <h1 className="header-title">
                                                        {state.contract.name}
                                                    </h1>
                                                <p>
                                                    please enter the following details to access your contract
                                                </p>
                                                </div>
                                            </div>
                                            <div className="row">
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group">
                                                    <label>Enter your Email address</label>
                                                    <input type="email" className="form-control" name="email" id="exampleInputEmail1" placeholder="Enter email" onChange={updateDetails} />
                                                </div>
                                                <div className="form-group">
                                                    <label>Enter the contract code</label>
                                                    <input type="text" name="code" className="form-control" id="exampleInputPassword1" placeholder="Password" onChange={updateDetails} />
                                                </div>
                                                <button type="submit" className="btn btn-primary btn-block" onClick={confirmContractDetails}>Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12"></div>
                        </div>
                    </div>
                </>;
        if(state.contractVisible)
        return  <>
                {
                    state.contract ? 
                    <>
                        <AccountDetailsModal 
                                contract_id={state.contract.id}
                                contract={state.contract}
                                bankcodes={state.bankcodes}
                                updateContract={updateContract}
                                />
                        {
                            state.contract.owner_type == "buyer"?
                            <>
                                <TourGuide
                                    closeTour={closeTour}
                                    tourConfig={tourConfig}
                                    isTourOpen={state.isTourOpen} 
                                    />
                                <TourGuide
                                    closeTour={closeTourAccount}
                                    tourConfig={tourConfigAddAccount}
                                    isTourOpen={state.isTourAccountOpen} 
                                    />
                                {/* {
                                    state.contract.client_account_no? (() => {
                                        return null
                                    })(): 
                                    (() => {
                                        return  <TourGuide
                                        closeTour={closeTour}
                                        tourConfig={tourConfig}
                                        isTourOpen={state.isTourOpen}
                                        />
                                    })()
                                } */}
                                <WithdrawFundsModal 
                                    contract_id={state.contract.id}
                                    contract={state.contract}
                                    updateContract={updateContract}
                                    milestone={state.currentMilestone}
                                    />
                            </> 
                            :
                            <>
                                <TourGuide
                                closeTour={closeTour}
                                tourConfig={tourConfig}
                                isTourOpen={state.isTourOpen}
                                />
                                <TourGuide
                                    closeTour={closeTourAccount}
                                    tourConfig={tourConfigAddAccount}
                                    isTourOpen={state.isTourAccountOpen} 
                                    />
                                {/* {
                                    state.contract.client_account_no? (() => {
                                        return null
                                    })(): 
                                    (() => {
                                        return  
                                    })()
                                } */}
                                {
                                    state.contract.payment_type == "whole" ? 
                                    <ModalWholePay 
                                    contract_id={state.contract.id}
                                    contract={state.contract}
                                    updateContract={updateContract}
                                    />:
                                    <Modal 
                                    contract_id={state.contract.id} 
                                    milestone={state.currentMilestone} 
                                    disburseFundsForMilestone={disburseFundsForMilestone}
                                    requestRetraction={disburseFundsForMilestone}
                                    />
                                }
                            </>
                        }
                    </>: null
                }
                <div className="container mt-3">
                    <div className="row">
                        <div className="col-12">
                            <div className="header border-0">
                                <div className="header-body">
                                    <div className="row align-items-end">
                                        <div className="col">
                                            <h1 className="header-title">
                                                {state.contract.name} <span className={classnames('badge', state.contract.owner_type == "buyer" ? "badge-soft-warning" : "badge-soft-success", "ml-1 mt-n1")}>{state.contract.owner_type == "buyer"? "Seller": "Buyer"}</span>
                                            </h1>
                                            <p>
                                                {state.contract.description || null}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col">
                                                    <h2 className="card-header-title">{formatCurrency(state.contract.amount)}</h2>
                                                </div>
                                                <div className="col-auto">
                                                    <h4>{ state.contract.payment_type == "whole" ? null: milestoneCalls(state.contract.milestones)}</h4>
                                                </div>
                                            </div>
                                            {
                                                state.contract.payment_type == "whole" ? 
                                                null
                                                :
                                                <div className="row">
                                                    <div className="col">
                                                        <div className="progress progress-sm">
                                                            <div className="progress-bar bg-success" role="progressbar" style={{
                                                                width: `${percentageOfSuccess(state.contract.milestones)}%`
                                                            }} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                            <hr/>
                                            {
                                                state.contract.payment_type == "whole" ? 
                                                <div className="row my-2">
                                                    <div className="col">
                                                        <span className="small text-secondary">Whole payment</span>
                                                        <h4>{formatCurrency(state.contract.amount)}</h4>
                                                    </div>
                                                    <div className="col-auto pt-2">
                                                        {
                                                            reviewUIForBuyer(state.contract.retraction_status, (
                                                                <>
                                                                            {
                                                                                state.contract.owner_type == "buyer"?
                                                                                <>
                                                                                        {
                                                                                            state.contract.disburseStatus == "CONFIRMED" ?
                                                                                            <>
                                                                                                <button className="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#withdraw-funds" disabled={state.contract.paid == true}>withdraw funds<i className="fe fe-check-circle text-success pointer"></i></button>
                                                                                            </>: 
                                                                                            <>
                                                                                                <button disabled className="btn btn-outline-light btn-sm">awaiting disbursement<i className="fe fe-check-circle text-success pointer"></i></button>
                                                                                            </>
                                                                                        }
                                                                                    </>
                                                                                :
                                                                                <>
                                                                                    {
                                                                                        state.contract.disburseStatus == "CONFIRMED" ? 
                                                                                        <>
                                                                                            <i className="fe fe-check-circle text-success pointer"></i>
                                                                                        </>
                                                                                        :
                                                                                        <>
                                                                                            <button className="btn btn-sm btn-outline-primary" data-tut="disburse-view" data-toggle="modal" data-target="#update-contract">Disburse funds <i className="fe fe-check-circle pointer"></i></button> 
                                                                                        </>
                                                                                    }
                                                                                </>
                                                                            }
                                                                        </>
                                                                    ))
                                                                }
                                                        
                                                    </div>
                                                    <>
                                                        <div className="col-auto">
                                                            {
                                                                state.contract.status != "INACTIVE" ?
                                                                <>
                                                                    <div className="text-secondary mt-2" data-tut="retractions" data-toggle="dropdown" id="contractdropdown"><i className="fe fe-menu" ></i></div>
                                                                    <div className="dropdown-menu" aria-labelledby="contractdropdown">
                                                                        {retractionOptionsDropdownBuyer(null, state.contract.retraction_status)}
                                                                    </div>
                                                                </>: null
                                                            }
                                                        </div>
                                                    </>
                                                </div>
                                                : 
                                                <>
                                                    {
                                                        state.contract.milestones.map((item, index) => {
                                                            return <div className="row my-2" key={index}>
                                                                        <div className="col">
                                                                            <span className="small text-secondary">Milestone {index + 1}</span>
                                                                            <h4>{formatCurrency(item.target)}</h4>
                                                                        </div>
                                                                        <div className="col-auto pt-2">
                                                                            {reviewUIForBuyer(item.retraction_status, <>
                                                                                {
                                                                                    state.contract.owner_type == "buyer" ?
                                                                                    <>
                                                                                        {
                                                                                            item.disburseStatus == "CONFIRMED" ? 
                                                                                            <>
                                                                                                <button className="btn btn-outline-primary btn-sm" onClick={()=>updateMilestone(item)} data-toggle="modal" data-target="#withdraw-funds" disabled={item.paid == true}>withdraw funds<i className="fe fe-check-circle text-success pointer"></i></button>
                                                                                            </>: 
                                                                                            <>
                                                                                                <button disabled className="btn btn-outline-light btn-sm">awaiting disbursement<i className="fe fe-check-circle text-success pointer"></i></button>
                                                                                            </>
                                                                                        }
                                                                                    </>
                                                                                :
                                                                                
                                                                                <>
                                                                                    {
                                                                                        item.disburseStatus == "CONFIRMED" ? 
                                                                                        <>
                                                                                            <i className="fe fe-check-circle text-success pointer"></i>
                                                                                        </>: 
                                                                                        <>
                                                                                            {
                                                                                                item.state == "COMPLETE" ? 
                                                                                                <i className="fe fe-check-circle pointer" data-tut="disburse-view" data-toggle="modal" data-target="#update-milestone" onClick={()=>updateMilestone(item)}></i> : 
                                                                                                <i className="fe fe-circle pointer" data-tut="disburse-view" data-toggle="modal" data-target="#update-milestone" onClick={()=>updateMilestone(item)}></i>
                                                                                            }
                                                                                        </>
                                                                                    }
                                                                                </>
                
                                                                                }
                                                                            </>)}
                                                                            
                                                                        </div>
                                                                        <>
                                                                            <div className="col-auto">
                                                                                {
                                                                                    state.contract.status != "INACTIVE" ?
                                                                                    <>
                                                                                        <div className="text-secondary mt-2" data-tut="retractions" data-toggle="dropdown" id={`contractdropdown-${item.id}`}><i className="fe fe-menu" ></i></div>
                                                                                        <div className="dropdown-menu" aria-labelledby={`contractdropdown-${item.id}`}>
                                                                                            {retractionOptionsDropdownBuyer(item.id, item.retraction_status)}
                                                                                        </div>
                                                                                    </>: null
                                                                                }
                                                                            </div>
                                                                        </>
                                                                    </div>
                                                        })
                                                    }
                                                </>
                                            }
                                            <hr/>
                                            <div className="row">
                                                    <div className="col-12">
                                                        <span className="header-pretitle small">Funding status</span>
                                                        <h2 data-tut="funding-contract-state" className={classnames({
                                                            'text-danger': state.contract.status == "INACTIVE",
                                                            'text-warning': state.contract.status == "ACTIVE",
                                                            'text-success': state.contract.status == "COMPLETED",
                                                        })}>{state.contract.status} <span>{ state.contract.status == "COMPLETED"? '🎉': null }</span> </h2>
                                                    </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-md-12">
                                                    {
                                                        state.contract.owner_type == "buyer" ?
                                                        null
                                                        :
                                                        <button onClick={fundAccount} data-tut="funding-contract" disabled={state.contract.status != "INACTIVE"} className="btn btn-primary btn-block btn-sm mx-2">
                                                            Fund contract
                                                        </button>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 mt-3">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-header-title">
                                            Contract details
                                            </h4>
                                        </div>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-6">
                                                    <span className="small text-secondary">Buyer name</span>
                                                    <h4>{state.contract.client}</h4>
                                                </div>
                                                <div className="col-6">
                                                    <span className="small text-secondary">Buyer email</span>
                                                    <h4>{state.contract.client_email}</h4>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-6">
                                                    <span className="small text-secondary">Escrow start</span>
                                                    <h4>{momentInTime(state.contract.duration_start)}</h4>
                                                </div>
                                                <div className="col-6">
                                                    <span className="small text-secondary">Escrow end</span>
                                                    <h4>{momentInTime(state.contract.duration_end)}</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-header">
                                            <h4 className="card-header-title">
                                                Account details
                                            </h4>
                                        </div>
                                        <div className="card-body">
                                            {
                                                state.contract.client_account_no ?
                                                <>
                                                    <div className="row">
                                                        <div className="col-6">
                                                            <span className="small text-secondary">Account No</span>
                                                            <h4>{state.contract.client_account_no}</h4>
                                                        </div>
                                                        <div className="col-6">
                                                            <span className="small text-secondary">Account Name</span>
                                                            <h4>{state.contract.client_account_name}</h4>
                                                        </div>
                                                    </div>
                                                    {
                                                        bankcodesLoading ? null : 
                                                        <div className="row">
                                                            <div className="col-6">
                                                                <span className="small text-secondary">Bank</span>
                                                                {/* {console.log(state.bankcodes, state.contract)}
                                                                {console.log(
                                                                    state.bankcodes.filter(bank => bank.bankcode === state.contract.client_bank_code)[0]
                                                                )} */}
                                                                <h4>{ state.bankcodes.length > 0 ?  state.bankcodes.filter(bank => bank.bankcode === state.contract.client_bank_code)[0].bankname : null}</h4>
                                                            </div>
                                                        </div>
                                                    }
                                                </>
                                                : 
                                                <div className="row">
                                                    <div className="col">
                                                        <button className="btn btn-outline-primary btn-block" data-target="#add-contract-account" data-toggle="modal" data-tut="reactour__account_no">
                                                            Add your account details <i className="fe fe-circle"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 mt-3">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-header-title">
                                            Contract account details
                                            </h4>
                                        </div>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-12" data-tut="contract-account">
                                                    <BankComponent bankaccount={{
                                                        bank_name: state.contract.va_bank_name,
                                                        account_number: state.contract.va_account_number,
                                                        account_name: state.contract.va_account_name,
                                                        expiry: state.contract.va_expirydate
                                                    }}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="card overflow-hidden">
                                <div className="card-header">
                                    <h3 className="card-header-title">All Transactions</h3>
                                </div>
                                <div className="overflow-scroll w-100">
                                    {
                                        state.contract.transactions.length  < 1 ? 
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-12 justify-content-center d-flex">
                                                    <div className="d-block">
                                                        <img src={image} width="140" className="d-block"/>
                                                        <p>No transactions available</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>:
                                        <table className="table table-sm table-nowrap">
                                            <thead>
                                                <tr>
                                                <th scope="col">Type</th>
                                                <th scope="col">Date</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {state.contract.transactions.map((item, index) => {
                                                let sign = null
                                                switch (item.flow) {
                                                    case "CREDIT":
                                                        sign = CreditSign
                                                        break;
                                                        case "DEBIT":
                                                            sign = DebitSign
                                                            break;
                                                            default:
                                                                break;
                                                            }
                                                            return <tr key={index}>
                                                    <td><img src={sign} width={20}/></td>
                                                    <td>{momentInTime(item.created_at)}</td>
                                                    <td>{item.details}</td>
                                                    <td>{formatCurrency(item.amount)}</td>
                                                </tr>
                                            })}
                                            </tbody>
                                        </table>
                                    }
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </>
    }
    
    const tourConfig = [
        {
            selector: `[data-tut="no_one"]`,
            content: () => (
                <div className="text-center">
                    <h3>Hey {state.contract? state.contract.client: 'there'}</h3>
                    {
                        state.contract ? 
                        <>
                            <h1>Welcome to your contract ({state.contract.name})</h1>
                            <p>
                                {state.contract.owner.first_name} created it for you, please follow the tour to understand a little more about how it works.
                            </p>
                        </>
                        : null
                    }
                </div>
            ),
            style: {
                minWidth: 400
            }
        },
        ...[Object.assign({}, 
            
        (state.contract.owner_type == "seller" && state.contract.status == "INACTIVE") && {
            selector: '[data-tut="funding_contract"]',
            content: () => (
                  <div className="text-center">
                      <h1>Fund your contract.</h1>:
                      <p>As the buyer, you can fund your contract, order to activate it, when your contract is active, then your deal is on</p>:
                  </div>
              ),
          },
        (state.contract.owner_type == "seller" && state.contract.status == "ACTIVE") && {
            selector: '[data-tut="disburse-view"]',
            content: () => (
                  <div className="text-center">
                      <h1>You've already funded the contract, weldone.</h1>:
                      <p>Once your client has completed milestones, you can disburse funds by clicking this button</p>:
                  </div>
              ),
          },
        (state.contract.owner_type == "seller" && state.contract.status == "COMPLETED") && {
            selector: '[data-tut="funding-contract-state"]',
            content: () => (
                  <div className="text-center">
                      <h1>Your contract has been successfully completed, welldone</h1>:
                      <p>Your contract has been completed, weldone you are a real MVP</p>:
                  </div>
              ),
          },
          (state.contract.owner_type == "buyer" && state.contract.status == "INACTIVE")  && {
            selector: '[data-tut="funding-contract-state"]',
            content: () => (
                  <div className="text-center">
                      <h1>Your contract is awaiting funding.</h1>
                      <p>As the seller, once you created your contract, your client would proceed to fund it, when funded, the status of the project displays here.</p>
                  </div>
              ),
          },
          (state.contract.owner_type == "buyer" && state.contract.status == "ACTIVE")  && {
            selector: '[data-tut="funding-contract-state"]',
            content: () => (
                  <div className="text-center">
                      <h1>Your contract has been funded</h1>
                      <p>Your contract has been funded and it's active, when you've completed your service, your client would disburse your funds.</p>
                  </div>
              ),
          },
          (state.contract.owner_type == "buyer" && state.contract.status == "COMPLETED")  && {
            selector: '[data-tut="funding-contract-state"]',
            content: () => (
                  <div className="text-center">
                      <h1>Your contract has been successfully completed, welldone</h1>
                      <p>Your contract has been completed, weldone you are a real MVP.</p>
                  </div>
              ),
          }
        )],
        ...((state.contract.owner_type == "seller" && state.contract.status == "INACTIVE") ? [{
            selector: '[data-tut="contract-account"]',
            content: () => (
                  <div className="text-center">
                      <h1>Fund with bank transfer.</h1>:
                      <p>You can also fund your contract with a bank transfer to the account details here</p>:
                  </div>
              ),
          }]: []),
        ...[Object.assign({},
            ( state.contract.status == "ACTIVE")  && {
            selector: '[data-tut="retractions"]',
            content: () => (
                  <div className="text-center">
                      <h1>Processing retractions.</h1>
                      <p>To request retractions or manage retractions use the options provided by this menu to make retraction requests or complaints requests</p>
                      <p>Let's get rocking</p>
                  </div>
              ),
          },
            ( state.contract.status !== "ACTIVE")  && {
            selector: '[data-tut="retractions"]',
            content: () => (
                  <div className="text-center">
                      <h1>Let's get rocking</h1>
                      <p>To review any steps, simply reload this page to view any of the afore mentioned tutorials</p>
                  </div>
              ),
          },
        )],
      ];
    
      console.log(tourConfig)
      const tourConfigAddAccount = [
          {
              selector: '[data-tut="reactour__account_no"]',
              content: () => (
                <div className="text-center">
                    <h1>Add your account number</h1>
                    <p>
                        Add your account number to be able to recieve withdrawals or retractions
                    </p>
                </div>
            ),
          }
      ]
    
    return(
        <>
            <Header />
            <Spring from={{ opacity: 0, marginTop: -1000 }} to={{ opacity: 1, marginTop: 0 }}>
                {
                    styles => (
                        <div style={styles}>
                            {
                                determineVisibility()
                            }
                        </div>
                    )
                }
            </Spring>
        </>
     )
    }
    
    export default  withToastManager(Layout);