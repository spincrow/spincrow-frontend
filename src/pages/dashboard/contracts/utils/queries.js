import gql from 'graphql-tag'

export const CREATE_CONTRACT = gql`
    mutation createContract(
            $name: String!, 
            $amount: Float!, 
            $client: String!, 
            $client_email: String!, 
            $client_phone: String!, 
            $description: String, 
            $duration_start: String!, 
            $duration_end: String!, 
            $owner_type: String!, 
            $payment_type: String!, 
            $spread_type: String!, 
            $periodic_amount: Float!, 
            $whole_amount: Float!, 
            $periodic_type: String!, 
            $milestones: [MileStoneInput!]
    ) {
        createContract (input: {
            name: $name,
            amount: $amount,
            client: $client,
            client_email: $client_email,
            client_phone: $client_phone
            duration_start: $duration_start,
            description: $description,
            duration_end: $duration_end,
            owner_type: $owner_type,
            payment_type: $payment_type,
            spread_type: $spread_type,
            periodic_amount: $periodic_amount,
            whole_amount: $whole_amount,
            periodic_type: $periodic_type,
            milestones: $milestones
        }){
            id
            name
            owner_type
            amount
            created_at
        }
    }
`;

export const GET_CONTRACTS = gql`
    query {
        contracts{
            id
            name
            amount
            created_at
            description
            paid
            client
            client_code
            payment_type
            owner_type
            client_phone
            disburseStatus
            status
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            client_account_name
            withdrawalStatus
            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note

            milestones {
                id
                target
                status
                disburseStatus
                progress
                status
                state
                withdrawalStatus
            }
            transactions {
                id
                details
                flow
                status
                amount
                created_at
            }
        }
    }
`;

export const GET_SINGLE_CONTRACT = gql`
    query getContract( $id: ID!, $first: Int, $last: Int, $skip: Int, $orderBy: ContractTransactionOrderByInput, $before: String, $after: String ){
        contract (id: $id ){
            id
            name
            amount
            created_at
            description
            paid
            client
            client_code
            payment_type
            owner_type
            client_phone
            disburseStatus
            status
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            client_account_name
            duration_end
            duration_start
            owner {
                first_name
                last_name
                email
                phone
            }
            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note
            retraction_status
            withdrawalStatus
            milestones {
                id
                target
                status
                disburseStatus
                progress
                status
                state
                retraction_status
                withdrawalStatus
            }
            transactions (first: $first, last: $last, skip: $skip, orderBy: $orderBy, before: $before, after: $after) {
                id
                details
                flow
                status
                amount
                created_at
            }
        }
    }
`;

export const UPDATE_CONTRACT_MILESTONE = gql`
    mutation updateContractMilestone(
        $contract_id: ID!,
        $milestone_id: ID!,
        $progress: Float!,
        $state: MilestoneStateEnum,
    ){
        updateContractMilestone( input: {
            contract_id: $contract_id,
            milestone_id: $milestone_id,
            progress: $progress,
            state: $state
        }){
            target
            progress
            status
            state
            id
            withdrawalStatus
            disburseStatus
        }
    }
`;

export const REQUEST_RETRACTION_AS_USER = gql`
mutation requestRetractionAsUser($contract_id: ID!, $milestone_id: ID){
    requestRetractionAsUser(input:{
      contract_id:$contract_id,
      milestone_id:$milestone_id
    }){
      id
      name
      amount
      description
      client
      client_email
      client_phone
      client_bank_code
      client_account_no
      spread_type
      payment_type
      paid
      status
      withdrawalStatus
      disburseStatus
      duration_end
      duration_start
      owner_type
      whole_amount
      periodic_type
      created_at
      va_flw_ref
      va_order_ref
      va_account_number
      va_account_name
      va_bank_name
      va_created_at
      va_note
      va_bank_name
      va_frequency
      va_expirydate
      client_account_name
      retraction_status
      transactions{
        id
        amount
        flow
        details
        created_at
        status
      }
      milestones{
        id
        paid
        retraction_status
        state
        status
        target
        disburseStatus
        progress
        withdrawalStatus
      }
      owner{
        id
      }
    }
  }
`;

export const REQUEST_RETRACTION_AS_CLIENT = gql`
    mutation requestRetractionAsClient(
        $contract_id: ID!, 
        $milestone_id: ID
    ){
        requestRetractionAsClient(
                input:{
                    contract_id:$contract_id,   
                    milestone_id:$milestone_id
                }
        ){
            id
            name
            amount
            created_at
            description
            paid
            client
            client_code
            payment_type
            owner_type
            client_phone
            disburseStatus
            status
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            client_account_name
            withdrawalStatus
            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note
            retraction_status

            milestones {
                id
                target
                status
                disburseStatus
                progress
                status
                state
                withdrawalStatus
                retraction_status
            }
            transactions {
                id
                details
                flow
                status
                amount
                created_at
            }
        }
    }
`;

export const APPROVE_RETRACTION_AS_CLIENT = gql`
mutation approveRetractionAsClient($contract_id: ID!, $milestone_id: ID, $approve_status: RetractionApprovedEnum!){
    approveRetractionAsClient(input:{
      contract_id:$contract_id,
      milestone_id:$milestone_id,
      approve_status: $approve_status
    }){
      id
      name
      amount
      description
      client
      client_email
      client_phone
      client_bank_code
      client_account_no
      withdrawalStatus
      spread_type
      payment_type
      paid
      status
      disburseStatus
      duration_end
      duration_start
      owner_type
      whole_amount
      periodic_type
      created_at
      va_flw_ref
      va_order_ref
      va_account_number
      va_account_name
      va_bank_name
      va_created_at
      va_note
      va_bank_name
      va_frequency
      va_expirydate
      client_account_name
      retraction_status
      transactions{
        id
        amount
        flow
        details
        created_at
        status
      }
      milestones{
        id
        paid
        retraction_status
        state
        target
        withdrawalStatus
        status
        disburseStatus
        progress
      }
      owner{
        id
      }
    }
  }
`;

export const APPROVE_RETRACTION_AS_USER = gql`
mutation approveRetractionAsUser($contract_id: ID!, $milestone_id: ID, $approve_status: RetractionApprovedEnum!){
    approveRetractionAsUser(input:{
      contract_id:$contract_id,
      milestone_id:$milestone_id,
      approve_status: $approve_status
    }){
      id
      name
      amount
      description
      client
      client_email
      client_phone
      withdrawalStatus
      client_bank_code
      client_account_no
      spread_type
      payment_type
      paid
      status
      disburseStatus
      duration_end
      duration_start
      owner_type
      whole_amount
      periodic_type
      created_at
      va_flw_ref
      va_order_ref
      va_account_number
      va_account_name
      va_bank_name
      va_created_at
      va_note
      va_bank_name
      va_frequency
      va_expirydate
      client_account_name
      retraction_status
      transactions{
        id
        amount
        flow
        details
        created_at
        status
      }
      milestones{
        id
        paid
        retraction_status
        withdrawalStatus
        state
        target
        status
        disburseStatus
        progress
      }
      owner{
        id
      }
    }
  }
`;

export const CANCEL_RETRACTION_AS_CLIENT = gql`
mutation cancelRetractionAsClient($contract_id: ID!, $milestone_id: ID){
  cancelRetractionAsClient(input:{
    contract_id:$contract_id,
    milestone_id:$milestone_id
  }){
    id
    name
    amount
    description
    client
    client_email
    client_phone
    client_bank_code
    client_account_no
    spread_type
    payment_type
    paid
    status
    disburseStatus
    duration_end
    duration_start
    owner_type
    whole_amount
    periodic_type
    withdrawalStatus
    created_at
    va_flw_ref
    va_order_ref
    va_account_number
    va_account_name
    va_bank_name
    va_created_at
    va_note
    va_bank_name
    va_frequency
    va_expirydate
    client_account_name
    retraction_status
    transactions{
      id
      amount
      flow
      details
      created_at
      status
    }
    milestones{
      id
      paid
      retraction_status
      state
      status
      disburseStatus
      progress
      withdrawalStatus
    }
    owner{
      id
    }
  }
}`;

export const CANCEL_RETRACTION_AS_USER = gql`
    mutation cancelRetractionAsUser($contract_id: ID!, $milestone_id: ID){
        cancelRetractionAsUser(input:{
            contract_id:$contract_id,
            milestone_id:$milestone_id
        }){
            id
            name
            amount
            description
            client
            client_email
            client_phone
            client_bank_code
            client_account_no
            spread_type
            payment_type
            paid
            status
            disburseStatus
            duration_end
            duration_start
            owner_type
            whole_amount
            periodic_type
            created_at
            va_flw_ref
            va_order_ref
            withdrawalStatus
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note
            va_bank_name
            va_frequency
            va_expirydate
            client_account_name
            retraction_status
            transactions{
            id
            amount
            flow
            details
            created_at
            status
            }
            milestones{
            id
            paid
            retraction_status
            withdrawalStatus
            state
            status
            disburseStatus
            progress
            }
            owner{
            id
            }
        }
    }
`;

export const DISBURSE_WHOLE_CONTRACT = gql`
    mutation disburseContractWholePayment(
        $contract_id: ID!
    ){
        disburseContractWholePayment( input: {
            contract_id: $contract_id
        }){
            amount
            owner{
                id
                first_name
                last_name
            }
            client_code
            owner_type
            status
            disburseStatus
            payment_type
            spread_type
            periodic_amount
            va_flw_ref
            withdrawalStatus
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note
            milestones {
                id
                target
                status
                progress
                disburseStatus
                withdrawalStatus
                state
                status
            }
            transactions {
                id
                status
                amount
                details
                flow
                created_at
            }
            id
        }
    }
`;

export const FUND_CONTRACT_AS_CLIENT = gql`
    mutation fundContractAsClient(
        $contract_id: ID!,
        $contract_state: ContractStateEnum!,
        $amount: Float,
        $transactionCode: String
    ){
        fundContractAsClient( input: {
            contract_id: $contract_id,
            contract_state: $contract_state,
            amount: $amount
            transactionCode: $transactionCode
        }){
            id
            name
            amount
            created_at
            payment_type
            client
            owner_type
            client_phone
            client_email
            status

            va_flw_ref
            va_order_ref
            va_expirydate
            withdrawalStatus
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note

            milestones {
                id
                target
                status
                progress
                withdrawalStatus
                disburseStatus
                state
                status
            }
            transactions {
                id
                status
                amount
                details
                flow
                created_at
            }
        }
    }
`;

export const FUND_CONTRACT_AS_USER = gql`
    mutation fundContractAsUser(
        $card_id: ID,
        $contract_id: ID!,
        $funding_type: FundingTypeEnum!,
        $amount: Float!
    ){
        fundContractAsUser(
            input: {
                contract_id: $contract_id,
                amount: $amount,
                funding_type: $funding_type,
                card_id: $card_id
            }
        ) {
            id
            name
            amount
            created_at
            payment_type
            client
            client_phone
            owner_type
            withdrawalStatus
            client_email
            status

            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note

            milestones {
                id
                target
                status
                progress
                withdrawalStatus
                disburseStatus
                state
                status
            }
            transactions {
                id
                details
                flow
                status
                amount
                created_at
            }
        }
    }
`;

export const FUND_WITH_CARD = gql`
    mutation fundWithCard(
        $tx_ref: String,
        $contract_id: ID!,
        $amount: Float!
    ){
        fundWithCard(
            input: {
                contract_id: $contract_id,
                amount: $amount,
                tx_ref: $tx_ref
            }
        ) {
            id
            name
            amount
            created_at
            payment_type
            client
            client_phone
            owner_type
            client_email
            status

            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            withdrawalStatus
            va_note

            milestones {
                id
                target
                status
                progress
                withdrawalStatus
                disburseStatus
                state
                status
            }
            transactions {
                id
                details
                flow
                status
                amount
                created_at
            }
        }
    }
`;
 
export const DISBURSE_CONTRACT_MILESTONE_BY_CLIENT = gql`
    mutation disburseContractMilestoneForClient(
        $contract_id: ID!,
        $milestone_id: ID!,
        $disburseStatus: DisburseStatusEnum,
        $status: TransactionStateEnum
    ){
        disburseContractMilestoneForClient( input: {
            contract_id: $contract_id,
            milestone_id: $milestone_id,
            disburseStatus: $disburseStatus,
            status: $status
        }){
            id
            name
            amount
            created_at
            client
            client_phone
            client_email
            status

            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note
            withdrawalStatus
            milestones {
                id
                target
                status
                progress
                disburseStatus
                state
                withdrawalStatus
                status
            }
            transactions {
                id
                status
                amount
                details
                flow
                created_at
            }
        }
    }
`;

export const WITHDRAWAL_FROM_CONTRACT_TO_ACCOUNT = gql`
    mutation withdrawFromContractToAccount(
        $contract_id: ID!,
        $milestone_id: ID,
        $client_code: String,
        $client_email: String,
        $amount: Float
    ){
        withdrawFromContractToAccount (
            input: {
                milestone_id: $milestone_id,
                contract_id: $contract_id,
                client_code: $client_code,
                client_email: $client_email,
                amount: $amount
            }
        ){
            id
            name
            amount
            created_at
            owner_type
            payment_type
            client
            client_phone
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            disburseStatus
            paid
            client_account_name
            status

            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note
            withdrawalStatus

            milestones {
                id
                target
                status
                progress
                withdrawalStatus
                disburseStatus
                state
                status
            }
            transactions {
                id
                status
                amount
                details
                flow
                created_at
            }
        }
    }
`;

export const WITHDRAWAL_FROM_MILESTONE_TO_ACCOUNT = gql`
    mutation withdrawFromMilestoneToAccount(
        $contract_id: ID!,
        $milestone_id: ID,
        $client_code: String,
        $client_email: String,
        $amount: Float
    ){
        withdrawFromMilestoneToAccount (
            input: {
                milestone_id: $milestone_id,
                contract_id: $contract_id,
                client_code: $client_code,
                client_email: $client_email,
                amount: $amount
            }
        ){
            id
            name
            amount
            created_at
            owner_type
            payment_type
            client
            client_phone
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            client_account_name
            status
            withdrawalStatus

            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note

            milestones {
                id
                target
                status
                progress
                paid
                disburseStatus
                state
                withdrawalStatus
                status
            }
            transactions {
                id
                status
                amount
                details
                flow
                created_at
            }
        }
    }
`;

export const ADD_CONTRACT_CLIENT_ACCOUNT = gql`
    mutation addContractClientAccount(
        $account_no: String,
        $bank_code: String,
        $contract_id: ID!,
        $bank_name: String,
        $account_name: String
    ){
        addContractClientAccount(
            input: {
                account_no: $account_no,
                bank_code: $bank_code,
                contract_id: $contract_id,
                bank_name: $bank_name,
                account_name: $account_name
            }
        ){
            id
            name
            amount
            created_at
            owner_type
            payment_type
            client
            client_phone
            client_email
            automaticWithdrawal
            client_account_no
            client_bank_code
            client_account_name
            status
            withdrawalStatus
            va_flw_ref
            va_order_ref
            va_expirydate
            va_frequency
            va_account_number
            va_account_name
            va_bank_name
            va_created_at
            va_note

            milestones {
                id
                target
                status
                progress
                disburseStatus
                withdrawalStatus
                state
                status
            }
            transactions {
                id
                status
                amount
                details
                flow
                created_at
            }
        }
    }
`;

export const BANK_LIST = gql`
    query bankcodes{
        bankcodes{
            bankname
            bankcode
            internetbanking
        }
    }
`;

export const CARD_LIST = gql`
    query cards{
        cards{
        id
        last_number
        type
        token
    }
}`;

export const GET_WALLET = gql`
    query {
        wallet {
            amount
            transactions {
                flow
                created_at
                details
                amount
            }
        }
    }
`
export const RESOLVE_ACCOUNT = gql`
    mutation resolveBankAccount($account_no:String!, $account_bank:String!){
        resolveBankAccount(input:{
            account_no: $account_no,
            account_bank:$account_bank
        }){
            account_name
            account_number
        }
    }
`;
