import React, {useState, useEffect, useRef}from "react"
import classnames from "classnames"
import {useParams, useHistory, withRouter} from "react-router-dom"
import {useMutation, useQuery, useLazyQuery} from "@apollo/react-hooks"
import {connect} from "react-redux"
import {CARD_LIST, FUND_CONTRACT_AS_USER, GET_SINGLE_CONTRACT,GET_CONTRACTS, GET_WALLET, FUND_WITH_CARD} from "../utils/queries"
import {formatCurrency, generateUUID, integrityValue} from "../../../../utils/helpers"
import { useToasts } from "react-toast-notifications"
import CardLoading from "../../../../components/card-loading"
import Form from "../../../../components/form"
import  BankComponent from "../../../../components/bank-component"
import {getContracts} from "../../../../store/Actions/Contract"
import {walletUpdate} from "../../../../store/Actions/Settings"

import analytics from "../../../../modules/analytics"
const mapStateToProps = (state) => ({currentUser: state.currentUser})


const FL_PUB = process.env.REACT_APP_RAVE_PUBLIC_KEY
const FL_SEC = process.env.REACT_APP_RAVE_SECRET_KEY
const FL_ENC = process.env.REACT_APP_RAVE_ENCRYPT_KEY

const mapDispatchToProps = {
    getContracts,
    walletUpdate
}

const  FundContract = props => {

    let mountedRef = useRef(null)

    const {id} = useParams()
    const history = useHistory()
    const {addToast} = useToasts()
    const [state, updateState]= useState({
        view: "wallet",
        cards: [],
        contract: null,
        card_id: null
    })

    const {currentUser} = props

    const updateWallet = async (data) => {
        await props.walletUpdate(data)
    }

    const {loading: walletLoading, data: walletData} = useQuery(GET_WALLET, {
        onCompleted: data => {
            updateWallet(data.wallet)
        }
    })
    const [getSingleContract, {loading, data}] = useLazyQuery(GET_SINGLE_CONTRACT, { variables: {id: id}, onCompleted: data => {
        updateStateCall({
            contract: data.contract
        })
    }, fetchPolicy: "no-cache"});

    const {data:cards, loading: cardLoading } = useQuery(CARD_LIST, {
        onCompleted: (data) => {
            // use the result
            updateStateCall({cards: data.cards})
        }
    })

    const [loadCards] = useLazyQuery(CARD_LIST, {
        onCompleted: (data) => {
            updateStateCall({cards: data.cards})
        }
    })

    const [ fetchContracts , {loading: fetchContractLoad}] = useLazyQuery(GET_CONTRACTS, { variables: {}, onCompleted: async data => {

        await props.getContracts(data.contracts)
        goToContract()
    }});

    const [fundContractAsUser, {data: fundingResult, loading: fundingLoading}] = useMutation(FUND_CONTRACT_AS_USER, { onCompleted: async (data) => {
        analytics.track("Funded contract - by wallet", {
            name: state.contract.name,
            description: state.contract.description,
            milestones: state.contract.milestones,
            whole_amount: state.contract.whole_amount,
        })
        updateStateCall({contract: data.fundContractAsUser})
        addToast("Successfully funded the contract🎉 ", {
            appearance: 'success',
            autoDismiss: true,
        })
        fetchContractsCall()
    }})

    const [addCard, {loading: addCardloading}] = useMutation(FUND_WITH_CARD, {onCompleted: async (resp) => {
        analytics.track("Funded contract - by adding card", {
            name: state.contract.name,
            description: state.contract.description,
            milestones: state.contract.milestones,
            whole_amount: state.contract.whole_amount,
        })
        updateStateCall({contract: data.fundContractAsUser})
        addToast("Successfully funded your contract and added your card 🎉 ", {
            appearance: 'success',
            autoDismiss: true,
        })
        await loadCards();
        fetchContractsCall();
    }})

    const fundContractWithCard = async () => {
        let transactionCode = generateUUID();
        const integrity_hash = integrityValue({
            PBFPubKey: FL_PUB,
            customer_email: currentUser.email,
            customer_firstname: currentUser.first_name,
            customer_lastname: currentUser.last_name,
            custom_description: "Adding your card",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Spincrow Contract card",
            amount: state.contract.amount,
            customer_phone: currentUser.phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
        });
        window.getpaidSetup({
            PBFPubKey: FL_PUB,
            customer_email: currentUser.email,
            customer_firstname: currentUser.first_name,
            customer_lastname: currentUser.last_name,
            custom_description: "Adding your card",
            custom_logo: "https://res.cloudinary.com/mozartted/image/upload/v1592398390/spincrow/Group.png",
            custom_title: "Spincrow Contract card",
            amount: state.contract.amount,
            customer_phone: currentUser.phone,
            country: "NG",
            currency: "NGN",
            txref: transactionCode,
            integrity_hash: integrity_hash,
            onclose: function() {},
            callback: async function(response) {
                var flw_ref = response.tx.flwRef; // collect flwRef returned and pass to a 					server page to complete status check.
                if (
                response.tx.chargeResponseCode == "00" ||
                response.tx.chargeResponseCode == "0"
                ) {
                // redirect to a success page, payment was successfully done, initiate call to update the current contract base and move value to 
                    try{
                        await addCard({
                            variables: {
                                contract_id: id,
                                amount: state.contract.amount,
                                tx_ref: transactionCode,
                            }
                        })
                    }catch(err){
                        addToast("Something went wrong 🎉 ", {
                            appearance: 'error',
                            autoDismiss: true
                        })
                    }
                } else {
                // redirect to a failure page.
                }
            }
        });
    }

    const fetchContractsCall = async () => {
        await fetchContracts()
    }

    const updateStateCall = (updates) => {
        if(mountedRef.current){
            updateState({
                ...state,
                ...updates
            })
        }
    }

    const goToContract = () => {
        history.push(`/contracts/${id}`)
    }

    const fundContractAsUserCall = async () => {
        let funding_type = null
        switch (state.view) {
            case "wallet":
                funding_type = "WALLET"
                break;
            case "card":
                funding_type = "CARD"
                break;
            default:
                break;
        }
        try {
            await fundContractAsUser({
                variables: {
                    contract_id: id,
                    amount: state.contract.amount,
                    funding_type: funding_type,
                    card_id: state.card_id
                }
            })
        }catch(err){
            analytics.track("Funding contract failed - by user", {
                name: state.contract.name,
                description: state.contract.description,
                milestones: state.contract.milestones,
                whole_amount: state.contract.whole_amount,
            })
            // addToast(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true,
                
            // })
        }
    }

    const contractStatus = (contract) => {
        switch (contract.status) {
            case "INACTIVE":
                return <span className="h3 badge badge-warning"> Not funded</span>
                break;
            case "ACTIVE":
                return <span className="h3 badge badge-success"> Funded</span>
                break;
            case "COMPLETED":
                return <span className="h3 badge badge-success"> Funded</span>
                break;
            default:
                break;
        }
    }

    const renderWalletView = () => {
        return (
            <>
                {
                    state.contract ? <> 
                        <div className="row">
                            <div className="col-6">
                                <h4>Contract name</h4>
                                <h4 className="font-weight-bolder mt-2">{state.contract.name}</h4>
                            </div>
                            <div className="col-6 text-right">
                                <h4>Amount</h4>
                                <h3 className="font-weight-bolder mt-2">{formatCurrency(state.contract.amount)}</h3>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-6">
                                <h4>Contract status</h4>
                                <h2 className="font-weight-bolder mt-2">{contractStatus(state.contract)}</h2>
                            </div>
                            {
                                walletLoading ? null : 
                                <div className="col-6 text-right">
                                    <h4>Wallet Balance</h4>
                                    <h4 className="font-weight-bolder mt-2">{formatCurrency(walletData.wallet.amount)}</h4>
                                </div>
                            }
                        </div>
                        
                    </> : null
                }
            </>
        )
    }
    const renderCardView = () => {
        return (
            <>
                {
                    state.contract ? <> 
                        <div className="row">
                            <div className="col-6">
                                <h4>Contract name</h4>
                                <h4 className="font-weight-bolder mt-2">{state.contract.name}</h4>
                            </div>
                            <div className="col-6 text-right">
                                <h4>Amount</h4>
                                <h3 className="font-weight-bolder mt-2">{formatCurrency(state.contract.amount)}</h3>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-auto">
                                <h4>Contract status</h4>
                                <h2 className="font-weight-bolder mt-2">{contractStatus(state.contract)}</h2>
                            </div>
                        </div>
                        <div className="row my-3">
                            {
                                cardLoading ? 
                                    <div className="col-md-12 mt-3 d-flex justify-content-center">
                                        <div className="spinner-border mt-5" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                :
                                <div className="col-md-12 mt-3">
                                    {
                                        cards.cards.length > 0 ? 
                                        <>
                                            <label>
                                                Select your card
                                            </label>
                                            <select id="form_select" className="form-control" name="card_id" onChange={e => updateStateCall({
                                            card_id: e.target.value
                                            })}>
                                                <option> --SELECT YOUR CARD -- </option>
                                                {
                                                    cards.cards.map((card, index) => <option value={card.id} key={index}>Card {index+1} (**** **** **** {card.last_number})</option>)
                                                }
                                            </select>
                                        </>:
                                        <>
                                            <div className="col form-group">
                                                <h2 className="font-weight-bolder text-center"> Your have no cards saved with us, Pay with a debit card</h2>
                                                <h3 className="text-center">Click the pay with card button below to pay the amount {formatCurrency(state.contract.amount)} using your card. <br/> We'll also save the your card for future transactions. </h3>
                                            </div>
                                        </>
                                    }
                                </div>
                            }
                        </div>
                    </> : null
                }
            </>
        )
    }

    const renderAccountView = () => {
        return (
            <>
                {
                    state.contract ? <> 
                        <div className="row">
                            <div className="col-6">
                                <h4>Contract name</h4>
                                <h4 className="font-weight-bolder mt-2">{state.contract.name}</h4>
                            </div>
                            <div className="col-6 text-right">
                                <h4>Amount</h4>
                                <h3 className="font-weight-bolder mt-2">{formatCurrency(state.contract.amount)}</h3>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-auto">
                                <h4>Contract status</h4>
                                <h2 className="font-weight-bolder mt-2">{contractStatus(state.contract)}</h2>
                            </div>
                        </div>
                        <div className="row my-3">
                            <div className="col-12 col-md-6">
                                <BankComponent bankaccount={{
                                    bank_name: state.contract.va_bank_name,
                                    account_number: state.contract.va_account_number,
                                    expiry: state.contract.va_expirydate,
                                    account_name: state.contract.va_account_name
                                }}/>
                            </div>
                            <div className="col-12 col-md-6">
                                <small className="d-md-block d-none mt-3 font-weight-bolder">
                                    👈 pay the amount {formatCurrency(state.contract.amount)} into the bank account <br/>
                                    that's the account number for your contract
                                </small>
                                <small className="d-block d-md-none mt-3 font-weight-bolder">
                                    ☝️ pay the amount {formatCurrency(state.contract.amount)} into the bank account <br/>
                                    that's the account number for your contract
                                </small>
                            </div>
                        </div>
                    </> : null
                }
            </>
        )
    }

    const renderFundingView = () => {
        switch (state.view) {
            case "wallet":
                return renderWalletView()
                break;
            case "card":
                return renderCardView()
                break;
            case "account":
                return renderAccountView()
                break;
            default:
                return null
                break;
        }
    }

    const asyncGetSingleContract = async () => {
        await getSingleContract();
    }

    const asyncLoadCards = async () => {
        await loadCards();
    }

    useEffect(() => {
        mountedRef.current = true
        asyncGetSingleContract()
        asyncLoadCards()
        return () => {
            mountedRef.current = false
        }
    }, [id])

    const completeButton = () => {
        switch (state.view) {
            case "wallet":
                return state.contract ? <Form.LightButton onClick={() => fundContractAsUserCall()} className="btn-block">Fund the contract</Form.LightButton> : null
                break;
            case "card":
                return state.contract? <> 
                    {
                        cardLoading ? <Form.LightButton onClick={() => {}} className="btn-block">Fund with Card</Form.LightButton>: <>
                        <Form.LightButton onClick={()=> {
                                cards.cards.length > 0? fundContractAsUserCall() : fundContractWithCard()
                            }} className="btn-block">Proceed with card payment</Form.LightButton>
                         </>
                    }
                </> : null
                break;
            case "account":
                return state.contract? <Form.LightButton onClick={() => fetchContractsCall()} className="btn-block">I've paid, let's got to contract</Form.LightButton> : null
                break;
            default:
                break;
        }
    }

    const skip = () => {
        fetchContractsCall()
    }

    return (
        <>
            <div className="container mt-3">
                <div className="row">
                    <div className="col-12 d-block">
                        <div className="header ">
                            <div className="header-body">
                                <div className="row align-items-end">
                                    <div className="col">
                                        <h6 className="header-pretitle">
                                        Fund your contract
                                        </h6>

                                        <h1 className="header-title">
                                        Fund contract
                                        </h1>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-9 col-12">
                        <div className="card">
                            <div className="card-header border-0">
                                <div className="card-header-title mt-3">
                                    <h2 className="font-weight-bold">Fund your contract</h2>
                                </div>
                            </div>
                            <div className="card-header">
                                <h3 className="card-header-title mr-auto">Pick a method</h3>
                                <ul className="nav nav-tabs nav-tabs-sm card-header-tabs">
                                    <li className="nav-item" data-tut="reactour__wallet_3">
                                        <a href="#!" onClick={()=> updateStateCall({view: "wallet"})}  className={classnames({'nav-link': true, 'active': state.view == "wallet"})}>Wallet</a>
                                    </li>
                                    <li className="nav-item" data-tut="reactour__wallet_4">
                                        <a href="#!" onClick={()=> updateStateCall({view: "card"})}  className={classnames({'nav-link': true, 'active': state.view == "card"})}>Card</a>
                                    </li>
                                    <li className="nav-item" data-tut="reactour__wallet_4">
                                        <a href="#!" onClick={()=> updateStateCall({view: "account"})}  className={classnames({'nav-link': true, 'active': state.view == "account"})}>Contract Account</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="card-body">
                                {
                                    loading ? <CardLoading /> :  renderFundingView()
                                }
                            </div>
                            <div className="card-footer">
                                <div className="row">
                                    <div className="col-auto">
                                        <button className="btn btn-light btn-block" onClick={skip}>
                                            {
                                                fetchContractLoad ? 
                                                <div className="spinner-border" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                                : <>
                                                    <span className="fe fe-chevron-left"></span> Skip
                                                </>
                                            }
                                            
                                        </button>
                                    </div>
                                    <div className="col">

                                    </div>
                                    <div className="col-auto">
                                        {
                                            completeButton()
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FundContract))