import React, { Component, Fragment } from "react";
import Form from "../../../../../../components/form"
import classnames from "classnames"
import {updateContract} from "../../../../../../store/Actions/Contract"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom"
import NumberFormat from "react-number-format"
import analytics from "../../../../../../modules/analytics"

const mapStateToProps = (state) => ({newContract: state.newContract})

const mapDispatchToProps = {
    updateContract
}

class PaymentStructure extends Component{

    constructor(props){
        super(props);

        this.state = {
            milestones: [
                {
                    target: 0.00,
                    status: "PENDING"
                }
            ],
            payment_type: "whole",
            amount: parseFloat("0"),
            spread_type: "milestones",
            periodic_amount: parseFloat("0.00"),
            whole_amount: parseFloat("0.00"),
            periodic_type: "daily"
        }

        // Updating the next move of the sequence.
        this.updateMileStone = this.updateMileStone.bind(this)
        this.nextMove = this.nextMove.bind(this)
        this.removeMilestone = this.removeMilestone.bind(this)
        this.back = this.back.bind(this)
    }

    async componentDidMount(){
        this.setState({
            ...this.state,
            ...this.props.newContract
        })
        analytics.track("Create contract - payment structure", {
            name: this.props.newContract.name,
            description: this.props.newContract.description
        })
    }

    
    updateMileStone(index, value ){
        let milestones = Object.assign([], this.state.milestones)
        
        milestones[index].target = parseFloat(value)
        this.setState({
            ...this.state,
            milestones: milestones
        })
    }

    back(){
        this.props.history.goBack()
    }

    removeMilestone(index){
        let milestones = [...this.state.milestones]
        // remove the milestone
        let updatedMilestones = milestones.filter((item, newIndex) => index !== newIndex)
        this.setState({
            ...this.state,
            milestones: updatedMilestones
        })
    }

    nextMove(){
        analytics.track("Create contract - payment structure completed", {
            name: this.props.newContract.name,
            description: this.props.newContract.description,
            milestones: this.state.milestones,
            whole_amount: this.state.whole_amount
        })
        this.props.updateContract({...this.state}).then(resp => {
            this.props.history.push('seller-information')
        })
    }

    render() {
        
        return (
            <div className="col-md-9 col-12">
                <div className="card">
                    <div className="card-header border-0">
                        <div className="card-header-title mt-3">
                            <h2 className="font-weight-bold">Payment Structure</h2>
                            <small>How would you like to structure the payment?</small>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-12">
                                <h4>How are you paying?</h4>
                            </div>
                            <div className="col-12">
                                <div className="row">
                                <div className="col-6">
                                    <button onClick={()=>{this.setState({
                                        ...this.state,
                                        payment_type: "whole"
                                    })}} className={classnames({
                                        'btn btn-outline-primary btn-block': true, 
                                        'active': this.state.payment_type == "whole"
                                    })}>
                                        Whole Payment
                                    </button>
                                </div>
                                    <div className="col-6">
                                        <button onClick={()=>{this.setState({
                                        ...this.state,
                                        payment_type: "spread"
                                    })}} className={classnames({
                                        'btn btn-outline-primary btn-block': true, 
                                        'active': this.state.payment_type == "spread"
                                    })}>
                                            Spread Payment
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            this.state.payment_type == 'whole'? <div className="row mt-5">
                                <div className="col-12 form-group">
                                    <label>How much are you paying in total</label>
                                    <NumberFormat thousandSeparator={true} value={this.state.whole_amount} prefix="NGN " onValueChange={(values) => {
                                        const {formattedValue, value} = values
                                        this.setState({
                                            ...this.state,
                                            whole_amount: parseFloat(value)
                                        })
                                    }} className="form-control"/>
                                </div>
                            </div>: null
                        }
                        {
                            this.state.payment_type == 'spread'? 
                            <Fragment>
                                <div className="row mt-5">
                                    <div className="col-12">
                                        <h4>How would you like to split payment</h4>
                                    </div>
                                    {/* <div className="col-12">
                                        <div className="row">
                                            <div className="col-6">
                                                <button onClick={()=>{this.setState({
                                                    ...this.state,
                                                    spread_type: "milestones"
                                                })}} className={classnames({
                                                    'btn btn-outline-primary btn-block': true, 
                                                    'active': this.state.spread_type == "milestones"
                                                })}>
                                                    Milestones
                                                </button>
                                            </div>
                                            <div className="col-6">
                                                <button  onClick={()=>{this.setState({
                                                    ...this.state,
                                                    spread_type: "periodic"
                                                })}} className={classnames({
                                                    'btn btn-outline-primary btn-block': true, 
                                                    'active': this.state.spread_type == "periodic"
                                                })}>
                                                    Periodic Payment
                                                </button>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                                <div className="row mt-4">
                                    <div className="col-12">
                                        {
                                            this.state.milestones.map((item, index) => {
                                                return <div className="row" key={index}>
                                                            <div className="col-12 form-group">
                                                                <label>Milestone {index +1} amount</label>
                                                                <div className="row">
                                                                    <div className="col">
                                                                        <NumberFormat value={item.target} prefix={"NGN "} thousandSeparator={true} onValueChange={values => {
                                                                            const {formattedValue, value} = values
                                                                            this.updateMileStone(index, value)
                                                                        }} onKeyPress={(event) => {
                                                                            if(event.charCode == 13){
                                                                                this.setState({
                                                                                    milestones: [...this.state.milestones, ...[{target: parseFloat("0"), status: "PENDING"}]]
                                                                                })
                                                                            }
                                                                        }} className="form-control"/>
                                                                    </div>
                                                                    <div className="col-auto">
                                                                        <button className="btn btn-light rounded-circle" onClick={(e) => this.removeMilestone(index)}><i className="fe fe-x" /></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 d-inline-flex">
                                        <button onClick={()=>{this.setState({
                                            milestones: [...this.state.milestones, ...[{target: parseFloat("0"), status: "PENDING"}]]
                                        })}} className="text-center mx-auto btn btn-sm btn-light">
                                            <i className="fe fe-plus"></i> Add milestone
                                        </button>
                                    </div>
                                </div>
                                {/* {
                                   this.state.spread_type == "milestones" ?  <Fragment>

                                    </Fragment>: null
                                }
                                {
                                    // the periodic payment
                                    this.state.spread_type == "periodic" ?  
                                        <Fragment>
                                        <div className="row mt-4">
                                            <div className="col">
                                                <label>What Frequency would like to pay?</label>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-4">
                                                <button onClick={()=>this.setState({periodic_type : "daily"})} className={classnames({
                                                    'btn btn-outline-primary btn-block': true, 
                                                    'active': this.state.periodic_type == "daily"
                                                })}>Daily</button>
                                            </div>
                                            <div className="col-4">
                                                <button onClick={()=>this.setState({periodic_type : "weekly"})} className={classnames({
                                                    'btn btn-outline-primary btn-block': true, 
                                                    'active': this.state.periodic_type == "weekly"
                                                })}>Weekly</button>
                                            </div>
                                            <div className="col-4">
                                                <button onClick={()=>this.setState({periodic_type : "monthly"})} className={classnames({
                                                    'btn btn-outline-primary btn-block': true, 
                                                    'active': this.state.periodic_type == "monthly"
                                                })}>Monthly</button>
                                            </div>
                                        </div>
                                        <div className="row mt-4">
                                             <div className="col-12 form-group">
                                                <label>{this.state.periodic_type} amount</label>
                                                <input value={this.state.periodic_amount} onChange={(e) => this.setState({
                                                    ...this.state,
                                                    periodic_amount: e.target.value
                                                })} className="form-control"/>
                                            </div>
                                        </div>
                                    </Fragment>: null
                                } */}
                            </Fragment>
                            : null
                        }
                    </div>
                    <div className="card-footer">
                        <div className="row">
                            <div className="col-auto">
                                <button className="btn btn-light" onClick={this.back}>
                                    <span className="fe fe-chevron-left"></span> Back 
                                </button>
                            </div>
                            <div className="col">

                            </div>
                            <div className="col-auto">
                                <button className="btn btn-primary" onClick={this.nextMove}>
                                    Next <span className="fe fe-chevron-right"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        )
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PaymentStructure))