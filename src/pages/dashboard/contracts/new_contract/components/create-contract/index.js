import React, { Component } from "react";
import {Switch, Route, NavLink, withRouter, useParams} from "react-router-dom"
import BasicInformation from "./basicinformation"
import PaymentStructure from "./paymentstructure"
import PaymentInfo from "./paymentinformation"
import SellerInformation from "./sellerinformation"
import {Spring} from "react-spring/renderprops"

const SelectRole = props =>{

    let {id} = useParams()
    return (
        <Spring from={{ opacity: 0, marginTop: -1000 }} to={{ opacity: 1, marginTop: 0 }}>
                {
                    styles => (
                        <div className="row justify-content-center" style={styles}>
                            <div className="col-12 mt-3 col-md-9">
                                <div className="row">
                                    <div className="col-3 d-none d-md-block">
                                        <div className="col-12 mt-5">
                                            <ul className="list-group list-group-flush mt-4">
                                                <li className="list-group-item mt-3 border-0">
                                                    <NavLink exact={true} activeClassName="text-primary" className="font-weight-bold" to="/contracts/new/basic-information"><span className="fe fe-circle"></span>  Basic information </NavLink>
                                                </li>
                                                <li className="list-group-item mt-3 border-0">
                                                    <NavLink exact={true} activeClassName="text-primary" className="font-weight-bold" to="/contracts/new/payment-structure"><span className="fe fe-circle"></span>  Payment structure </NavLink>
                                                </li>
                                                <li className="list-group-item mt-3 border-0">
                                                    <NavLink exact={true} activeClassName="text-primary" className="font-weight-bold" to="/contracts/new/seller-information"><span className="fe fe-circle"></span>  Seller information </NavLink>
                                                </li>
                                                {
                                                    props.history.location.pathname == '/contracts/new/fund-contract' ?
                                                    <li className="list-group-item mt-3 border-0">
                                                        <NavLink activeClassName="text-primary" className="font-weight-bold" to={`/contracts/new/fund-contract/${id}`}><span className="fe fe-circle"></span>  Fund Contract </NavLink>
                                                    </li>: null
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-12 d-block d-md-none">
                                        <div className="header ">
                                            <div className="header-body">
                                                <div className="row align-items-end">
                                                    <div className="col">
                                                        <h6 className="header-pretitle">
                                                        Create your contract
                                                        </h6>

                                                        <h1 className="header-title">
                                                        Create Contract
                                                        </h1>
                                                        
                                                    </div>
                                                    <div className="col-auto">
                                                            <ul className="nav nav-tabs header-tabs  nav-tabs-sm">
                                                                <li className="nav-itm">
                                                                    <NavLink exact={true} activeClassName="active" className="nav-link" to="/contracts/new/basic-information">Basic information </NavLink>
                                                                </li>
                                                                <li className="nav-item">
                                                                    <NavLink exact={true} activeClassName="active" className="nav-link" to="/contracts/new/payment-structure">Payment structure </NavLink>
                                                                </li>
                                                                <li className="nav-item">
                                                                    <NavLink exact={true} activeClassName="active" className="nav-link" to="/contracts/new/seller-information">Seller information </NavLink>
                                                                </li>
                                                                {
                                                                    props.history.location.pathname == '/contracts/new/fund-contract' ?
                                                                    <li className="nav-item">
                                                                        <NavLink  activeClassName="active" className="nav-link" to={`/contracts/new/fund-contract/${id}`}>Fund contract</NavLink>
                                                                    </li>: null
                                                                }
                                                            </ul>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <Switch>
                                        <Route exact path="/contracts/new/basic-information">
                                            <BasicInformation/>
                                        </Route>
                                        <Route exact path="/contracts/new/payment-structure">
                                            <PaymentStructure/>
                                        </Route>
                                        <Route exact path="/contracts/new/payment-information">
                                            <PaymentInfo/>
                                        </Route>
                                        <Route exact path="/contracts/new/seller-information">
                                            <SellerInformation/>
                                        </Route>
                                    </Switch>         
                                </div>
                            </div>
                        </div>
                    )
                }
        </Spring>
    )
}

export default withRouter(SelectRole)