import React, { Component, Fragment } from "react";
import Form from "../../../../../../components/form"
import classnames from "classnames"

const credit_card_active = require('../../../../../../assets/img/credit-card/credit-card-active.svg')
const credit_card_inactive = require('../../../../../../assets/img/credit-card/credit-card-inactive.svg')

const building_active = require('../../../../../../assets/img/building/building-active.svg')
const building_inactive = require('../../../../../../assets/img/building/building-inactive.svg')

export default class PaymentInformation extends Component{

    constructor(props){
        super(props);

        this.state = {
            milestones: 1,
            type: "debit",
            spread_type: "milestones",
            periodic_type: "daily"
        }
    }


    render() {
        return (
            <div className="col-md-9 col-12">
                <div className="card">
                    <div className="card-header border-0">
                        <div className="card-header-title mt-3">
                            <h2 className="font-weight-bold">Payment Information</h2>
                            <small>How would you like to structure the payment?</small>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-12">
                                <div className="row">
                                <div className="col-6">
                                    <div onClick={()=>{this.setState({
                                        ...this.state,
                                        type: "debit"
                                    })}} className={classnames({
                                        'col-12 border d-inline-flex flex-column text-center p-4 rounded active': true, 
                                        'border-primary': this.state.type == "debit"
                                    })}>
                                        <img src={this.state.type == "debit" ? credit_card_active: credit_card_inactive}/>
                                        <small className="mt-3">Debit Card</small>
                                    </div>
                                </div>
                                    <div className="col-6">
                                        <div onClick={()=>{this.setState({
                                            ...this.state,
                                            type: "transfer"
                                        })}} className={classnames({
                                            'col-12 border d-inline-flex flex-column text-center p-4 rounded active': true, 
                                            'border-primary': this.state.type == "transfer"
                                        })}>
                                            <img src={this.state.type == 'transfer' ? building_active: building_inactive}/>
                                            <small className="mt-3">Bank Transfer</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            this.state.type == "debit" ? 
                            <Fragment>
                                <div className="row mt-4">
                                    <div className="col-12 form-group">
                                        <label>Card number</label>
                                        <input className="form-control" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 form-group">
                                        <div className="row">
                                            <div className="col-6">
                                                <label>Expiry date</label>
                                                <input className="form-control" />
                                            </div>
                                            <div className="col-6">
                                                <label>CVV</label>
                                                <input className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Fragment>
                            : null
                        }
                        {
                            this.state.type == "transfer" ? 
                            <Fragment>
                                <div className="row mt-5">
                                    <div className="col-12">
                                        <p>Simply transfer ₦230,000 to the following account number to add money to your wallet </p>
                                    </div>
                                    <div className="col-12">
                                        <div className="row mt-3">
                                            <div className="col-auto">
                                                <h4 className="text-secondary">Bank</h4>
                                            </div>
                                            <div className="col text-right">
                                                <h4 className="text-secondary">Highstreet MFB</h4>
                                            </div>
                                        </div>
                                        <div className="row mt-3">
                                            <div className="col-auto">
                                                <h4 className="text-secondary">Account Number</h4>
                                            </div>
                                            <div className="col text-right">
                                                <h4 className="text-secondary">001209348348</h4>
                                            </div>
                                        </div>
                                        <div className="row mt-3">
                                            <div className="col-auto">
                                                <h4 className="text-secondary">Name</h4>
                                            </div>
                                            <div className="col text-right">
                                                <h4 className="text-secondary">Adegoke Damola</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 mt-5">
                                        <p className="text-primary">Funds transferred into this account will be used in sent to your wallet and automatically used to fund this escrow </p>
                                    </div>
                                </div>
                            </Fragment>
                            : null
                        }
                    </div>
                    <div className="card-footer">
                        <div className="row">
                            <div className="col-auto">
                                <button className="btn btn-light">
                                    <span className="fe fe-chevron-left"></span> Back 
                                </button>
                            </div>
                            <div className="col">

                            </div>
                            <div className="col-auto">
                                <button className="btn btn-primary">
                                    Confirm
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        )
    }
}