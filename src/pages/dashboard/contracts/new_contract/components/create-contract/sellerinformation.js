import React, { Component, useState, useEffect} from "react";
import Form from "../../../../../../components/form"
import classnames from "classnames"
import {updateContract, getContracts} from "../../../../../../store/Actions/Contract"
import {connect} from "react-redux"
import {withRouter, useHistory} from "react-router-dom"
import PhoneInput from 'react-phone-input-2'
import {withToastManager} from "react-toast-notifications"
import {useMutation, useLazyQuery} from "@apollo/react-hooks"
import gql from "graphql-tag";
import {CREATE_CONTRACT, GET_CONTRACTS} from "../../../utils/queries"
// import '../../../../../../assets/styles/phone-input.css'
import analytics from "../../../../../../modules/analytics"
import swal from "sweetalert"

const mapStateToProps = (state) => ({
    newContract: state.newContract, 
    createContractMutation: CREATE_CONTRACT, 
    getContractsQuery: GET_CONTRACTS
})

const mapDispatchToProps = {
    updateContract,
    getContracts
}

const SellerInfo  = (props) => {

    const [state, updateState] = useState({
        client: "",
        client_email:"",
        client_phone: ""
    })

    useEffect(() => {
        updateState({
            ...state,
            ...props.newContract
        })
    }, []);

    const updateInternalState = (e) => {
        updateState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    const back = () => {
        history.goBack()
    }

    analytics.track("Create contract - client information", {
        name: props.newContract.name,
        description: props.newContract.description,
        milestones: props.newContract.milestones,
        whole_amount: props.newContract.whole_amount
    })

    // defining all props and axillary methods to import
    const [createContract, {data, loading: creatingContractLoading}] = useMutation(props.createContractMutation, {
        onCompleted: async response => {
            // move to the funding page.
            analytics.track("Create contract - create contract completed", {
                name: props.newContract.name,
                description: props.newContract.description,
                milestones: props.newContract.milestones,
                whole_amount: props.newContract.whole_amount,
                client_name: state.client,
                client_phone: state.client_phone,
                client_email: state.client_email
            })
            props.updateContract({})
            swal("Well done!", "You've successfully created your contract.", "success").then(value => {
                if(response.createContract.owner_type == "seller"){
                    history.push(`/contracts/${response.createContract.id}`)
                    toastManager.add("Successfully created contract", {
                        appearance: 'success',
                        autoDismiss: true
                    })
                }else{
                    history.push(`/contracts/fund-contract/${response.createContract.id}`)
                    toastManager.add("Successfully created contract", {
                        appearance: 'success',
                        autoDismiss: true
                    })
                }
            })
        }
    });

    // re loading all contracts
    let history = useHistory();
    const {toastManager} = props

    const nextMove = async () => {
        try {
            analytics.track("Create contract - create contract initiated", {
                name: props.newContract.name,
                description: props.newContract.description,
                milestones: props.newContract.milestones,
                whole_amount: props.newContract.whole_amount,
                client_name: state.client,
                client_phone: state.client_phone,
                client_email: state.client_email
            })
            props.updateContract({...state})
            // console.log(state)
            let resp = await createContract({variables: {...state}})
        } catch (e) {
            
        }
    }


    return (
        <div className="col-md-9 col-12">
            <div className="card">
                <div className="card-header border-0">
                    <div className="card-header-title mt-3">
                        <h2 className="font-weight-bold">Client Information</h2>
                        <small>Enter information about this</small>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-12 form-group">
                            <label> Client name</label>
                            <input name="client" value={state.client} placeholder="What's your client's name?" onChange={updateInternalState} className="form-control"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 form-group">
                            <label> Client email</label>
                            <input name="client_email" value={state.client_email} placeholder="enter your client's email" onChange={updateInternalState} className="form-control"/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 form-group">
                            <label> Client phone no</label>
                            <PhoneInput
                                containerClass=""
                                inputStyle={{width: '100%'}}
                                inputClass="form-control"
                                country={'ng'}
                                value={state.client_phone}
                                onChange={phone => updateState({ 
                                    ...state,
                                    client_phone: `+${phone}`
                                 })}
                            />
                            {/* <input name="client_phone" value={state.client_phone} placeholder="(1232) "onChange={updateInternalState} className="form-control"/> */}
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <div className="row">
                        <div className="col-auto">
                            <button className="btn btn-light btn-block" onClick={back}>
                                <span className="fe fe-chevron-left"></span> Back 
                            </button>
                        </div>
                        <div className="col">

                        </div>
                        <div className="col-auto">
                            <button className="btn btn-primary btn-block" onClick={nextMove}>
                                {
                                    creatingContractLoading ? 
                                    <div className="spinner-border" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>
                                    : <>
                                        Create Contract <span className="fe fe-chevron-right"></span>
                                    </>
                                }
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    )
}

export default withToastManager(withRouter(connect(mapStateToProps, mapDispatchToProps)(SellerInfo)))