import React, { Component } from "react";
import Form from "../../../../../../components/form"
import {updateContract} from "../../../../../../store/Actions/Contract"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom"
import classnames from "classnames"
import analytics from "../../../../../../modules/analytics"

const mapStateToProps = (state) => ({newContract: state.newContract})

const mapDispatchToProps = {
    updateContract
}

class BasicInfo extends Component{
    constructor(props){
        super(props);
        this.state = {
            owner_type: "",
            name: "",
            description: "",
            duration_start: "",
            duration_end: ""
        }
        this.updateContract = this.updateContract.bind(this)
        this.nextMove = this.nextMove.bind(this)
    }

    async componentDidMount(){
        analytics.track("Create contract initiated")
        this.setState({
            ...this.state,
            ...this.props.newContract
        })
    }

    updateContract(e){
        this.setState({
            ...this.state,
            [e.target.name]: e.target.value
        })
    }

    nextMove(){
        analytics.track("Create contract step 1", {
           name: this.state.name,
           description: this.state.description
        })
        this.props.updateContract({...this.state}).then(resp => {
            this.props.history.push('payment-structure')
        })
    }

    render() {
        return (
            <div className="col-md-9 col-12">
                <div className="card">
                    <div className="card-header border-0">
                        <div className="card-header-title mt-3">
                            <h2 className="font-weight-bold">Basic Information</h2>
                            <small>Enter information about this</small>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-12">
                                <h4>Role</h4>
                            </div>
                            <div className="col-12">
                                <div className="row">
                                <div className="col-6">
                                    <button className={classnames({
                                        'btn btn-outline-primary btn-block': true,
                                        'active': this.state.owner_type == "buyer"
                                    })} value="buyer" name="owner_type" onClick={this.updateContract}>
                                        Buyer
                                    </button>
                                </div>
                                    <div className="col-6">
                                        <button className={classnames({
                                        'btn btn-outline-primary btn-block': true,
                                        'active': this.state.owner_type == "seller"
                                    })} value="seller" name="owner_type" onClick={this.updateContract}>
                                            Seller
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-12 form-group">
                                <label> What are you paying for</label>
                                <input className="form-control" name="name" placeholder="a car, a house, a course program, a contract, anything" onChange={this.updateContract} value={this.state.name}/>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-12 form-group">
                                <label>Description</label>
                                <textarea rows="6" className="form-control" placeholder="tell us more about what you paying for (for your own good)" name="description" onChange={this.updateContract} value={this.state.description}></textarea>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-12">
                                <label>Escrow duration</label>
                            </div>
                            <div className="col-6 form-group">
                                <Form.DateInput name="duration_start" onChange={this.updateContract} value={this.state.duration_start}/>
                            </div>
                            <div className="col-6 form-group">
                                <Form.DateInput name="duration_end" onChange={this.updateContract} value={this.state.duration_end}/>
                            </div>
                        </div>
                    </div>
                    <div className="card-footer">
                        <div className="row">
                            <div className="col-auto">
                                {/* <button className="btn btn-light">
                                    <span className="fe fe-chevron-left"></span> Back 
                                </button> */}
                            </div>
                            <div className="col">

                            </div>
                            <div className="col-auto">
                                <button className="btn btn-primary" onClick={this.nextMove}>
                                    Next <span className="fe fe-chevron-right"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BasicInfo))