import React, {Component} from "react"
import {Link, Switch, Route} from "react-router-dom"
import Header from "../../../../components/header"
import SelectScreen from "./components/selectrole"
import CreateContract from "./components/create-contract"
// import {addToast} from "react-toast-notifications"
// import "./confirm.scss"

export default class Layout extends Component {
    
    state = {
        agencies: [],
        passwordVisible: false,
        params: {},
        loading: false
    }

    constructor(props){
        super(props);

        this.onChange = this.onChange.bind(this)
        this.signupUser = this.signupUser.bind(this)
    }


    async componentDidMount(){
        try {
            let response = await this.props.loadAgencies();
            this.setState({
                agencies: response
            })
            
        }catch(err){
            
        }
    }

    signupUser(){
        
        this.setState({loading: true});
        const {signup, setRedirectUrl, autoLogin, toastManager} = this.props
        signup(this.state.params).then(resp => {
            
            if(resp.status == 'success'){
                // the ideas behind  the greatest of us all
                toastManager.add(resp.message, {
                    appearance: 'success',
                    autoDismiss: true,
                    
                })
                this.setState({loading: false})
                setTimeout(() => {
                    setRedirectUrl('/onboarding')
                    autoLogin()
                }, 3000)
            }
        }).catch(err => {
            
        })
    }

    onChange(e){
        
        
        let target = e.target
        this.setState({
            ...this.state,
            params: {
                ...this.state.params,
                [target.name] : target.value
            }
        })
    }

    render(){
        return(
           <React.Fragment>
                {/* <Header/> */}
                <div className="container mt-3">
                        <Switch>
                            <Route exact path="/contracts/new">
                                <SelectScreen/>
                            </Route>
                            <Route path="/contracts/new">
                                <CreateContract/>
                            </Route>
                        </Switch>
                </div>
           </React.Fragment>
        )
    }
}