import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag'
import {getContracts} from "../../../store/Actions/Contract"
import {updateUser} from "../../../store/Actions/Auth"
import {GET_CONTRACTS} from "./utils/queries"
import {UPDATE_CONTRACTS_TOUR} from "../utils/queries"

const mapStateToProps = (state => {
    return {
        getContractQuery: GET_CONTRACTS,
        updateContractTourQuery: UPDATE_CONTRACTS_TOUR,
        contracts: state.contracts,
        user: state.currentUser
    }
});
const mapDispatchToProps = {
    getContracts,
    updateUser
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))