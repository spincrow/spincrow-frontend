import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import {getContracts} from "../../../../store/Actions/Contract";
import {updateUser} from "../../../../store/Actions/Auth"
import gql from 'graphql-tag';
import {GET_SINGLE_CONTRACT, REQUEST_RETRACTION_AS_USER, APPROVE_RETRACTION_AS_USER, CANCEL_RETRACTION_AS_USER} from "../utils/queries"


const mapStateToProps = (state => {
    return {
        getContractQuery: GET_SINGLE_CONTRACT,
        requestRetractionAsUserGQL: REQUEST_RETRACTION_AS_USER,
        approveRetractionAsUserGQL: APPROVE_RETRACTION_AS_USER,
        cancelRetractionAsUserGQL: CANCEL_RETRACTION_AS_USER,
        contracts: state.contracts,
        user: state.currentUser
    }
});

const mapDispatchToProps = {
    getContracts: getContracts,
    updateUser
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout))
)