import classnames from "classnames"
import Header from "../../../../components/header"
import {useQuery, useLazyQuery, useMutation} from "@apollo/react-hooks"
import React, {Component, useState, useEffect} from "react"
import { withToastManager } from "react-toast-notifications"
import DebitSign from "../../../../assets/img/debit-sign.svg"
import CreditSign from "../../../../assets/img/credit-sign.svg"
import BankComponent from "../../../../components/bank-component"
import {generateUUID, integrityValue} from "../../../../utils/helpers"
import {Link, useHistory, useParams, withRouter} from "react-router-dom"
import FundingModal from "../components/internal-modals/fund-contract-modal"
import DisburseWholeModal from "../components/internal-modals/disburse-whole-modal"
import DisburseMilestoneModal from "../components/internal-modals/disburse-milestone-modal"
import Skeleton from "react-loading-skeleton"
import {Spring} from "react-spring/renderprops"
import CardLoading from "../../../../components/card-loading"
import ContractLoading from "../../../../components/contract-loading-page"
import {formatCurrency, momentInTime, milestoneCalls, percentageOfSuccess} from "../../../../utils/helpers"
import TourGuide from "../../../../components/tour-guide"
import {UPDATE_SINGLE_CONTRACT_TOUR} from "../../utils/queries"

const image = require('../../../../assets/img/empty.svg')
// define the variables
const FL_PUB = process.env.REACT_APP_RAVE_PUBLIC_KEY
const FL_SEC = process.env.REACT_APP_RAVE_SECRET_KEY
const FL_ENC = process.env.REACT_APP_RAVE_ENCRYPT_KEY

// define the internals
const Layout = (props)  => {
    
    let { id } = useParams()
    const {user, toastManager} = props
    const [state, updateState ] = useState({
        currentMilestone: null,
        currentContract: null,
        loading: false,
        isTourOpen: false,
        isShowingMore: false
    })
     
    let history = useHistory()

    const [ fetchContracts, {loading: contractLoading, error: contractLoadingError} ] = useLazyQuery(props.getContractQuery, { onCompleted: data => {
        
        let contract = data.contract
        updateState({
            ...state,
            currentContract: contract,
            loading: true
        })
        // props.getContracts(data.contracts)
    }});
// 
    const [requestRetractionAsUserMutation, {loading: retractionRequestLoading, data: retractionRequestData, error: retractionRequestError}] = useMutation(props.requestRetractionAsUserGQL, {onCompleted: async (response) => {
        updateState({
            ...state,
            currentContract: response.requestRetractionAsUser
        })
        toastManager.add("Requested retraction successfully", {
            appearance: "success",
            autoDismiss: true
        })
    }})

    const [approveRetractionAsUserMutation, {loading: retractionApproveLoading, data: retractionApproveData, error: retractionApproveError}] = useMutation(props.approveRetractionAsUserGQL, {onCompleted: async (response) => {
        updateState({
            ...state,
            currentContract: response.approveRetractionAsUser
        })
        // toastManager.add("Retract🎉 ", {
        //     appearance: 'success',
        //     autoDismiss: true,
        // })
    }})

    const [cancelRetractionAsUserMutation, {
        loading: cancelRetractionLoading,
        data: cancelRetractionData,
        error: cancelRetractionError
    }] = useMutation(props.cancelRetractionAsUserGQL, {
        onCompleted: async (response) => {
            updateState({
                ...state,
                currentContract: response.cancelRetractionAsUser
            })
            toastManager.add("Successfully cancelled the retraction🎉 ", {
                appearance: 'success',
                autoDismiss: true,
            })
        }
    })

    /**
     * Async wrapper to fetch contract
     */
    const fetchState = async () => {
        updateState({
            ...state,
            loading: true
        })
        await fetchContracts({
            variables: {
                id: id,
                first: 10,
                orderBy: "created_at_DESC"
            }
        })
    }

    const paginationPrevCalls = async () => {
        // we are limited to 10 transactions per call.
        await fetchContracts({
            variables: {
                id: id,
                last: 10,
                before: state.currentContract.transactions[0].id,
                orderBy: "created_at_DESC"
            }
        })
    }

    const paginationNextCalls = async () => {
        // we are limited to 10 transactions per call.
        await fetchContracts({
            variables: {
                id: id,
                first: 10,
                after: state.currentContract.transactions[9].id,
                orderBy: "created_at_DESC"
            }
        })
    }

    /**
     * Lifecycle hook
     */
    useEffect(() => {
        fetchState()
        openTour()
    },[history.location.pathname == `contracts/${id}`])

    /**
     * Update the modal with the current milestone
     * @param {*} milestone 
     */
    const updateMilestone = (milestone) => {
        updateState({
            ...state,
            currentMilestone: milestone
        })
    }

    const closeTour = () => {
        updateState({ ...state, isTourOpen: false });
        updateSingleContractTour()
        // send a request to update HomePage tour for user
    }

    const [updateSingleContractTour, {loading: updateContractTourLoading}] = useMutation(UPDATE_SINGLE_CONTRACT_TOUR, {
        onCompleted: async (data) => {
            props.updateUser(data.singleContractTourUpdate)
        }
    })

    const openTour = () => {
        updateState(state => ({ ...state, isTourOpen: true }));
    }

    const toggleShowMore = () => {
        updateState(state => ({
            ...state,
            isShowingMore: !state.isShowingMore
        }));
    }

    const updateMilestoneData = (stateUpdates) => {
        // the new milestone update
        let milestones = state.currentContract.milestones
        
        // get the milestones to update
        milestones.forEach((milestone, index) => {
            if(milestone.id === stateUpdates.id ){
                milestones[index] = stateUpdates
            }
        })
        updateState({
            ...state,
            currentContract: {
                ...state.currentContract,
                milestones: milestones
            }
        })
        toastManager.add("Succcessfully updated the milestone 🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
    }

    const updateContractData = (stateUpdates) => {
        // the new milestone update
        updateState({
            ...state,
            currentContract: {
                ...state.currentContract,
                ...stateUpdates
            }
        })
    }

    const requestRetraction = async (milestone) => {
        try{
            if(milestone){
                await requestRetractionAsUserMutation({
                    variables: {
                        contract_id: id,
                        milestone_id: milestone
                    }
                })
            }else{
                await requestRetractionAsUserMutation({
                    variables: {
                        contract_id: id
                    }
                })
            }
        }catch(err) {
            // toastManager.add(err.message, {
            //     appearance: "error",
            //     autoDismiss: true
            // })
        }
    }

    const reviewUIForBuyer = (retraction_status, defaultUI) => {
        switch (retraction_status) {
            case "REQUESTED":
                return <span> Retraction requested</span>
                break;
            case "REJECTED":
                return <span className="text-danger"> Retraction rejected</span>
                break;
            case "PENDING":
                return <span className="text-warning"> Retraction approved, payment pending</span>
            case "SUCCESSFUL":
                return <span className="text-success"> Retraction successful</span>
                break;
            case "INACTIVE":
                return defaultUI
                break;
            default:
                return defaultUI
                break;
        }
        // if(state.currentContract.owner_type == "buyer"){
        // }else{
        //     return defaultUI
        // }
    }

    const approveRetraction = async (milestone, approveState = true) => {
        try{
            if(milestone){
                await approveRetractionAsUserMutation({
                    variables: {
                        contract_id: id,
                        milestone_id: milestone,
                        approve_status: approveState ? "APPROVED" : "REJECTED"
                    }
                })
            }else{
                await approveRetractionAsUserMutation({
                    variables: {
                        contract_id: id,
                        approve_status: approveState ? "APPROVED" : "REJECTED"
                    }
                })
            }
        }catch(err) {
            // toastManager.add(err.message, {
            //     appearance: "error",
            //     autoDismiss: true
            // })
        }
    }

    const cancelRetraction = async (milestone) => {
        try{
            if(milestone){
                await cancelRetractionAsUserMutation({
                    variables: {
                        contract_id: id,
                        milestone_id: milestone
                    }
                })
            }else{
                await cancelRetractionAsUserMutation({
                    variables: {
                        contract_id: id
                    }
                })
            }
        }catch(err) {
            // toastManager.add(err.message, {
            //     appearance: "error",
            //     autoDismiss: true
            // })
        }
    }

    const retractionOptionsDropdown = (milestone, retraction_status) => {
        if(state.currentContract.status == "COMPLETED"){
            return <>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
        }
        switch (retraction_status) {
            case "INACTIVE":
                if(state.currentContract.owner_type == "buyer"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => requestRetraction(milestone || null)}>
                        <i className="fe fe-credit-card"></i>  Request a retraction 
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.currentContract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
                break;
            case "REQUESTED":
                if(state.currentContract.owner_type == "buyer"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => cancelRetraction(milestone || null)}>
                        <i className="fe fe-trash"></i> Cancel retraction
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.currentContract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, true)}>
                        <i className="fe fe-check-circle"></i> Approve retraction 
                        </div>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, false)}>
                        <i className="fe fe-x-circle"></i> Reject retraction 
                        </div>
                    </>
                }
                break;
            case "PENDING":
            case "SUCCESSFUL":
                return <div className="dropdown-item pointer">
                <i className="fe fe-check-circle"></i> Send complaint.
                </div>
            case "REJECTED":
                if(state.currentContract.owner_type == "buyer"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => cancelRetraction(milestone || null)}>
                        <i className="fe fe-trash"></i> Cancel retraction
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.currentContract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, true)}>
                        <i className="fe fe-check-circle"></i> Approve retraction 
                        </div>
                        <div className="dropdown-item pointer" onClick={() => approveRetraction(milestone || null, false)}>
                        <i className="fe fe-x-circle"></i> Reject retraction 
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
            default:
                if(state.currentContract.owner_type == "buyer"){
                    return <>
                        <div className="dropdown-item pointer" onClick={() => requestRetraction(milestone || null)}>
                        <i className="fe fe-credit-card"></i>  Request a retraction 
                        </div>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }else if(state.currentContract.owner_type == "seller"){
                    return <>
                        <div className="dropdown-item pointer">
                        <i className="fe fe-check-circle"></i> Send complaint.
                        </div>
                    </>
                }
                break;
        }
    }

    const tourConfig = [
        {
            selector: `[data-tut="no_one"]`,
            content: () => (
                <div className="text-center">
                    <h3>Hey {user.first_name}</h3>
                    <h1>Welcome to your first contract</h1>

                    <p>So you've created your first contract, let's work you through how it works</p>
                </div>
            ),
            style: {
                minWidth: 400
            }
        },
        {
          selector: '[data-tut="funding_contract"]',
          content: () => (
                <div className="text-center">
                    {
                        state.currentContract.owner_type == "buyer"?
                        <h1>Fund your contract.</h1>:
                        <h1>Your contract is awaiting funding.</h1>
                    }
                    {
                        state.currentContract.owner_type == "buyer"?
                        <p>As the buyer, you can fund your contract, order to activate it, when your contract is active, then your deal is on</p>:
                        <p>As the seller, once you created your contract, your client recieved a notification to fund the contract, you can also reach out to them, to ensure the contract is funded</p>
                    }
                </div>
            ),
        },
        {
          selector: '[data-tut="bank_account"]',
          content: () => (
                <div className="text-center">
                    <h1>Funding by bank account</h1>
                    {
                        state.currentContract.owner_type == "buyer"?
                        <p>You can fund your contract by using the bank account details here, each contract created on the platform has an individual bank account for funding it.</p>:
                        <p>You can have your other party fund their bank account using the account details here, They should have access to it on their end.</p>
                    }
                </div>
            ),
        },
        {
          selector: '[data-tut="retractions"]',
          content: () => (
                <div className="text-center">
                    <h1>Retractions and chargebacks</h1>
                    <p>On clicking this menu bar, you can use the options in the dropdown to request, fulfil or cancel retractions, in the event of one or when you need to</p>
                </div>
            ),
        }
      ];
    

    return(
        <React.Fragment>
                {
                    props.user.single_contract_tour == true? null : 
                    <TourGuide
                        closeTour={closeTour}
                        tourConfig={tourConfig}
                        isTourOpen={state.isTourOpen}
                    />
                }
            <Spring from={{ opacity: 0, marginTop: -1000 }} to={{ opacity: 1, marginTop: 0 }}>
                {
                    styles => (
                        <div style={styles}>
                            {/* Modals here */}
                            {
                            state.currentContract ? 
                            <> 
                                {
                                    state.currentContract.payment_type == "whole" ?
                                        <DisburseWholeModal 
                                            contract={state.currentContract}
                                            contract_id={state.currentContract.id} 
                                            updateContract={updateContractData}
                                        />
                                    : 
                                        <DisburseMilestoneModal 
                                            contract={state.currentContract}
                                            contract_id={state.currentContract.id} 
                                            milestone={state.currentMilestone} 
                                            updateMilestone={updateMilestoneData}
                                            updateContract={updateContractData}
                                        /> 
                                }
                                <FundingModal 
                                    contract={state.currentContract}
                                    contract_id={state.currentContract.id} 
                                    milestone={state.currentMilestone} 
                                    updateContract={updateContractData}
                                    updateMilestone={updateMilestoneData}
                                />
                            </>
                            : null}
                             {/* <Header /> */}
                             { state.currentContract ? 
                                <div className="container mt-3">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="header border-0">
                                                <div className="header-body">
                                                    <div className="row align-items-end">
                                                        <div className="col">
                                                            <h1 className="header-title">
                                                                {state.currentContract.name} <span className={classnames('badge', state.currentContract.owner_type == "buyer" ? "badge-soft-success" : "badge-soft-warning", "ml-1 mt-n1")}>{state.currentContract.owner_type}</span>
                                                            </h1>
                                                            <p>
                                                                {state.currentContract.description || null}
                                                            </p>
                                                        </div>
                                                        <div className="col-auto">
                                                            {/* <a href="/contracts/new" className="btn btn-primary">
                                                                Update Contract
                                                            </a> */}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="row">
                                                <div className="col-md-12">
                                                    <div className="card">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col">
                                                                    <h2 className="card-header-title">{formatCurrency(state.currentContract.amount)}</h2>
                                                                </div>
                                                                <div className="col-auto">
                                                                <h4>{ state.currentContract.payment_type == "whole" ? null: milestoneCalls(state.currentContract.milestones)}</h4>
                                                                </div>
                                                            </div>
                                                            {
                                                                state.currentContract.payment_type == "whole" ? null : 
                                                                <div className="row">
                                                                    <div className="col">
                                                                        <div className="progress progress-sm">
                                                                            <div className="progress-bar bg-success" role="progressbar" style={{
                                                                                width: `${percentageOfSuccess(state.currentContract.milestones)}%`
                                                                                }} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            }
                                                            <hr/>
                                                            {
                                                                state.currentContract.payment_type == "whole" ? 
                                                                <div className="row my-2">
                                                                    <div className="col">
                                                                        <span className="small text-secondary">Whole payment</span>
                                                                        <h4>{formatCurrency(state.currentContract.amount)}</h4>
                                                                    </div>
                                                                    <>
                                                                        {
                                                                            state.currentContract.status != "INACTIVE" ? <>
                                                                            {
                                                                                state.currentContract.owner_type == "buyer"?
                                                                                <div className="col-auto pt-2">
                                                                                
                                                                                    {
                                                                                        reviewUIForBuyer(state.currentContract.retraction_status, (
                                                                                            <>
                                                                                                {
                                                                                                    state.currentContract.disburseStatus == "CONFIRMED" ? 
                                                                                                    <>
                                                                                                        <i className="fe fe-check-circle text-success pointer"></i>
                                                                                                    </>
                                                                                                    :
                                                                                                    <>
                                                                                                        <button className="btn btn-outline-primary btn-small btn-sm" data-toggle="modal" data-target="#disburse-contract">Disburse funds <i className="fe fe-check-circle pointer mt-1"></i></button> 
                                                                                                    </>
                                                                                                }
                                                                                            </>
                                                                                        ))
                                                                                    }
                
                                                                                </div>: 
                                                                                <div className="col-auto pt-2">
                                                                                    {
                                                                                        reviewUIForBuyer(state.currentContract.retraction_status, null)
                                                                                    }
                                                                                </div>
                                                                            }
                                                                            </>
                                                                            : null
                                                                        }
                                                                    </>
                                                                    <>
                                                                        <div className="col-auto">
                                                                            <div className="text-secondary mt-2" data-toggle="dropdown" data-tut="retractions" id="contractdropdown"><i className="fe fe-menu" ></i></div>
                                                                            <div className="dropdown-menu" aria-labelledby="contractdropdown">
                                                                                {retractionOptionsDropdown(null, state.currentContract.retraction_status)}
                                                                            </div>
                                                                        </div>
                                                                    </>
                                                                </div>
                                                                : 
                                                                <>
                                                                    {state.currentContract.milestones.map((item, index) => {
                                                                        return <div className="row my-2" key={index}>
                                                                                    <div className="col">
                                                                                        <span className="small text-secondary">Milestone {index + 1}</span>
                                                                                        <h4>{formatCurrency(item.target)}</h4>
                                                                                    </div>
                                                                                    <div className="col-auto pt-2">
                                                                                                {reviewUIForBuyer(item.retraction_status, <>
                                                                                                    {
                                                                                                        state.currentContract.status != "INACTIVE" ? <> 
                                                                                                            {
                                                                                                                item.disburseStatus == "CONFIRMED" ? 
                                                                                                                <>
                                                                                                                    <i className="fe fe-check-circle text-success pointer"></i>
                                                                                                                </>: 
                                                                                                                <>
                                                                                                                    {
                                                                                                                        item.state == "COMPLETE" ? 
                                                                                                                        <i className="fe fe-check-circle pointer" data-toggle="modal" data-target="#update-milestone" onClick={()=>updateMilestone(item)}></i> : 
                                                                                                                        <i className="fe fe-circle pointer" data-toggle="modal" data-target="#update-milestone" onClick={()=>updateMilestone(item)}></i>
                                                                                                                    }
                                                                                                                </>
                                                
                                                                                                            }
                                                                                                        </>: null
                                                                                                    }    
                                                                                                </>)}
                                                                                    </div>
                                                                                    <>
                                                                                        <div className="col-auto">
                                                                                            <div className="text-secondary mt-2" data-toggle="dropdown" data-tut="retractions" id={`contractdropdown-${item.id}`}><i className="fe fe-menu" ></i></div>
                                                                                            <div className="dropdown-menu" aria-labelledby={`contractdropdown-${item.id}`}>
                                                                                                {retractionOptionsDropdown(item.id, item.retraction_status)}
                                                                                            </div>
                                                                                        </div>
                                                                                    </>
                                                                                </div>
                                                                    })}
                                                                </>
                                                            }
                                                            <hr/>
                                                                <div className="row">
                                                                        <div className="col-12">
                                                                            {
                                                                                state.currentContract.status == "INACTIVE" ?
                                                                                <div className="alert alert-warning show fade">
                                                                                    {
                                                                                        state.currentContract.owner_type == "buyer" ?  
                                                                                            <strong>Hey, you've not funded your contract yet, Fund the contract</strong>
                                                                                        :
                                                                                            <strong>Hey, this contract hasn't been funded, contact the buyer to fund the contract</strong>
                                                                                    }
                                                                                
                                                                                </div>:
                                                                                <>
                                                                                        <span className="header-pretitle small">Funding status</span>
                                                                                        <h2 className={classnames({
                                                                                            'text-danger': state.currentContract.status == "INACTIVE",
                                                                                            'text-warning': state.currentContract.status == "ACTIVE",
                                                                                            'text-success': state.currentContract.status == "COMPLETED",
                                                                                        })}>{state.currentContract.status} <span>{ state.currentContract.status == "COMPLETED"? '🎉': null }</span></h2>
                                                                                </>
                                                                            
                                                                            }
                                                                        </div>
                                                                </div>
                                                            <hr/>
                                                            <div className="row">
                                                                <div className="col-md-12">
                                                                    {
                                                                        state.currentContract.owner_type == "buyer" ? 
                                                                        <>
                                                                            {
                                                                                state.currentContract.status !== "INACTIVE" ? <button className="btn btn-light btn-block" disabled={true} data-tut="funding_contract" >
                                                                                    <span className="text-primary"><i className="fe fe-circle"></i> Fund Contract</span>
                                                                                </button> : 
                                                                                <Link to={`/contracts/fund-contract/${state.currentContract.id}`} className="btn btn-light btn-block" data-tut="funding_contract">
                                                                                    <span className="text-primary"><i className="fe fe-circle"></i> Fund Contract</span>
                                                                                </Link>
                                                                            }
                                                                        </>
                                                                        : <button disabled className="btn btn-outline-primary disabled btn-block" data-tut="funding_contract">
                                                                            Awaiting funding from your client
                                                                        </button>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mt-3">
                                                    <div className="card">
                                                        <div className="card-header">
                                                            <h4 className="card-header-title">
                                                            Contract details
                                                            </h4>
                                                        </div>
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <span className="small text-secondary">Buyer name</span>
                                                                    <h4>{state.currentContract.client}</h4>
                                                                </div>
                                                                <div className="col-6">
                                                                    <span className="small text-secondary">Buyer email</span>
                                                                    <h4>{state.currentContract.client_email}</h4>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-6">
                                                                    <span className="small text-secondary">Escrow start</span>
                                                                    <h4>{momentInTime(state.currentContract.duration_start)}</h4>
                                                                </div>
                                                                <div className="col-6">
                                                                    <span className="small text-secondary">Escrow end</span>
                                                                    <h4>{momentInTime(state.currentContract.duration_end)}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 mt-3">
                                                    <div className="card">
                                                        <div className="card-header">
                                                            <h4 className="card-header-title">
                                                            Contract account details
                                                            </h4>
                                                        </div>
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-12" data-tut="bank_account">
                                                                    <BankComponent bankaccount={{
                                                                        account_name: state.currentContract.va_account_name,
                                                                        bank_name: state.currentContract.va_bank_name,
                                                                        account_number: state.currentContract.va_account_number,
                                                                        expiry: state.currentContract.va_expirydate
                                                                    }}/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-8">
                                            <div className="card overflow-hidden">
                                                <div className="card-header">
                                                    <h3 className="card-header-title">All Transactions</h3>
                                                </div>
                                                {
                                                    state.currentContract.transactions.length < 1 ? 
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-12 justify-content-center d-flex">
                                                                <div className="d-block">
                                                                    <img src={image} width="140" className="d-block"/>
                                                                    <p>No transactions available</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>:
                                                    <table className="table table-sm table-nowrap">
                                                        <thead>
                                                            <tr>
                                                            <th scope="col">Type</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {state.currentContract.transactions.map((item, index) => {
                                                            // let classNameObject = {
                                                            //     'fe': true,
                                                            //     'fe-arrow-right-circle text-success': item.flow == "CREDIT",
                                                            //     'fe-arrow-left-circle text-danger': item.flow == "DEBIT"
                                                            // }
                                                            let sign = null
                                                            switch (item.flow) {
                                                                case "CREDIT":
                                                                    sign = CreditSign
                                                                    break;
                                                                case "DEBIT":
                                                                    sign = DebitSign
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                            return <tr key={index}>
                                                                <td><img src={sign} width={20}/></td>
                                                                <td>{momentInTime(item.created_at)}</td>
                                                                <td>{item.details}</td>
                                                                <td>{formatCurrency(item.amount)}</td>
                                                            </tr>
                                                        })}
                                                        </tbody>
                                                    </table>
                                                }
                                                <div className="card-footer">
                                                    <ul className="pagination pagination-sm">
                                                        <li className="page-item"><a className="page-link" onClick={paginationPrevCalls}>Previous</a></li>
                                                        <li className="page-item"><a className="page-link" onClick={paginationNextCalls}>Next</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                             : 
                            <ContractLoading />}
                        </div>
                    )
                }
            </Spring>
        </React.Fragment>
     )
}

export default withRouter(withToastManager(Layout))