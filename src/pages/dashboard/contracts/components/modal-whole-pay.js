import React, {useState, useEffect} from "react"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import PropTypes from "prop-types"
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import {useMutation} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import {DISBURSE_WHOLE_CONTRACT} from "../utils/queries"
import {formatCurrency} from "../../../../utils/helpers"
import {withToastManager} from "react-toast-notifications"

const Modal = (props) => {

    const {toastManager} = props
    const [state, updateState] = useState({
        contract: null
    })

    const [disburseContract, {data}] = useMutation(DISBURSE_WHOLE_CONTRACT, { onCompleted: (data) => {
        updateState({
            ...state,
        })
        props.updateContract(data.updateContractWholePayment)
        toastManager.add("Successfully disbursed funds🎉 ", {
            appearance: 'success',
            autoDismiss: true,
        })
    }})

    useEffect(() => {
        updateState({
            ...state,
            contract: props.contract
        })
    }, [props.milestone])

    const disburseContractFunds = async () => {
        try {
            await disburseContract({
                variables: {
                    contract_id: props.contract_id
                }
            })
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true,
                
            // })
        }
    }

    return (
        <div className="modal fade show" id="update-contract" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        {
                            props.contract ? 
                            <React.Fragment>
                                <div className="card-header">
                                    <div className="row">
                                        <div className="col">
                                            <h4 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                                Update Contract
                                            </h4>
                                        </div>
                                        <div className="col-auto">
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                                Contract Whole Amount
                                            </small>
                                            <h2>
                                                {formatCurrency(props.contract.amount)}
                                            </h2>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <button className="btn mx-2 btn-sm btn-success btn-block" data-dismiss="modal" aria-label="Close" onClick={() => disburseContractFunds()}> Disburse funds</button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                            : null
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    contract_id: PropTypes.string,
    contract: PropTypes.object.isRequired,
    updateContract: PropTypes.func.isRequired
}


export default withToastManager(Modal)