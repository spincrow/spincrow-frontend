import React, {useState, useEffect} from "react"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import {useMutation} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import {UPDATE_CONTRACT_MILESTONE} from "../utils/queries"
import {formatCurrency} from "../../../../utils/helpers"

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

const handle = (props) => {
    const { value, dragging, index, ...restProps } = props;
    // 
    return (
      <Tooltip
        prefixCls="rc-slider-tooltip"
        overlay={value}
        visible={dragging}
        placement="top"
        key={index}
      >
        <Handle value={value} {...restProps} />
      </Tooltip>
    );
  };

const Modal = (props) => {

    const [state, updateState] = useState({
        progress: 0
    })

    const [updateMilestoneMutation, {data}] = useMutation(UPDATE_CONTRACT_MILESTONE, { onCompleted: (data) => {
        updateState({
            ...state,
            update: data.updateContractMilestone
        })
        props.updateMilestone(data.updateContractMilestone)
    }})

    useEffect(() => {
        updateState({
            ...state,
            progress: props.milestone ? props.milestone.progress : 0,
            state: props.milestone ? props.milestone.state : "INCOMPLETE"
        })
    }, [props.milestone])

    const onChange = (e) => {
        updateState({
            ...state,
            progress: e
        })
    }

    const onProjectDone = (e) => {
        if(e.target.checked){
            updateState({
                ...state,
                state: "COMPLETE"
            })
        }else{
            updateState({
                ...state,
                state: "INCOMPLETE"
            })
        }
    }

    const updateMilestone = async () => {
        // props.updateMilestone(state)
        
        await updateMilestoneMutation({variables: {
            contract_id: props.contract_id,
            milestone_id: props.milestone.id,
            progress: state.progress,
            state: state.state
        }})
    }

    return (
        <div className="modal fade show" id="update-milestone" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        {
                            props.milestone ? 
                            <React.Fragment>
                                <div className="card-header">
                                    <div className="row">
                                        <div className="col">
                                            <h4 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                                Update Milestone
                                            </h4>
                                        </div>
                                        <div className="col-auto">
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                                Target
                                            </small>
                                            <h2>
                                                {formatCurrency(props.milestone.target)}
                                            </h2>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                                Milestone status
                                            </small>
                                            <Slider  min={0} max={100} defaultValue={props.milestone.progress} handle={handle} onChange={onChange}/>
                                        </div>
                                    </div>
                                    <div className="row mt-4">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                               MIlestone State
                                            </small>
                                            <div className="custom-control custom-switch">
                                                <input checked={state.state === "COMPLETE"}  type="checkbox" className="custom-control-input" id="customSwitch1" onChange={onProjectDone}  />
                                                <label className="custom-control-label" for="customSwitch1">This project is done?</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <div className="row">
                                        <div className="col">
                                            <button className="btn btn-primary" data-dismiss="modal" aria-label="Close" onClick={updateMilestone}>Update Milestone</button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                            : null
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal