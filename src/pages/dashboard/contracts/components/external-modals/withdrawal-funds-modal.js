import React, {useState, useEffect} from "react"
import classnames from "classnames"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import PropTypes from "prop-types"
import {useMutation} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import {WITHDRAWAL_FROM_CONTRACT_TO_ACCOUNT, WITHDRAWAL_FROM_MILESTONE_TO_ACCOUNT} from "../../utils/queries"
import {formatCurrency} from "../../../../../utils/helpers"
import {withToastManager} from "react-toast-notifications"

const Modal = (props) => {

    const {toastManager} = props
    const [state, updateState] = useState({
        contract: props.contract,
        contract_id: props.contract.id,
        bankcodes: props.bankcodes,
        account_name: ""
    })

    const [withdrawFromContractToAccount, { loading: withdrawContractLoading}] = useMutation(WITHDRAWAL_FROM_CONTRACT_TO_ACCOUNT, { onCompleted: (data) => {
        updateState({
            ...state,
            contract: data.withdrawFromContractToAccount
        })
        window.$("#withdraw-funds").modal("hide")
        toastManager.add("Successfully added account no🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
        props.updateContract(data.withdrawFromContractToAccount)
    }})

    const [withdrawFromMilestoneToAccount, { loading: withdrawMilestoneLoading}] = useMutation(WITHDRAWAL_FROM_MILESTONE_TO_ACCOUNT, { onCompleted: (data) => {
        updateState({
            ...state,
            contract: data.withdrawFromMilestoneToAccount
        })
        window.$("#withdraw-funds").modal("hide")
        toastManager.add("Processing withdrawal🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
        props.updateContract(data.withdrawFromMilestoneToAccount)
    }})

    const withdrawFunds = async () => {
        try {
            switch (props.contract.payment_type) {
                case "whole":
                    await withdrawFromContractToAccount({
                        variables: {
                            contract_id: props.contract.id,
                            client_code: localStorage.getItem('contract_code').split('-')[1],
                            client_email: props.contract.client_email,
                            amount: props.contract.amount
                        }
                    })
                    break;
                case "spread":
                        await withdrawFromMilestoneToAccount({
                            variables: {
                                contract_id: props.contract.id,
                                milestone_id: props.milestone.id,
                                client_code: localStorage.getItem('contract_code').split('-')[1],
                                client_email: props.contract.client_email,
                                amount: props.milestone.target
                            }
                        })
                    break;
                default:
                    break;
            }
        }catch(err){
            window.$("#withdraw-funds").modal("hide")
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true,
            // })
        }
    }

    const withdrawalViewSelector = (type) => {
        switch (type) {
            case "whole":
                return (<>
                    <React.Fragment>
                        <div className="card-header">
                            <div className="row">
                                <div className="col">
                                    <h3 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                        Are you sure you wanna withdraw your contract funds?
                                    </h3>
                                </div>
                                <div className="col-auto">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <small className="header-pretitle">
                                        Contract Amount
                                    </small>
                                    <h2>
                                        {formatCurrency(props.contract.amount)}
                                    </h2>
                                </div>
                            </div>
                            <div className="row">
                                        <div className="col-md-12 mt-3">
                                            <button 
                                                className="btn btn-lg btn-outline-primary btn-block" 
                                                disabled={withdrawContractLoading} 
                                                onClick={() => withdrawFunds()}
                                            > 
                                            {
                                                withdrawContractLoading ? 
                                                <div className="spinner-border" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>: 
                                                <span>Yes, Withdraws funds 🚀</span>
                                            }
                                            </button>
                                        </div>
                                    </div>
                        </div>
                    </React.Fragment>
                </>);
            case "spread":
                return (<>
                    { 
                        props.milestone ? 
                        <React.Fragment>
                        <div className="card-header">
                            <div className="row">
                                <div className="col">
                                    <h3 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                        Are you sure you wanna withdraw your milestone funds?
                                    </h3>
                                </div>
                                <div className="col-auto">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <small className="header-pretitle">
                                        Milestone Target Amount
                                    </small>
                                    <h2>
                                        {formatCurrency(props.milestone.target)}
                                    </h2>
                                </div>
                            </div>
                            <div className="row">
                                        <div className="col-md-12 mt-3">
                                            <button 
                                                className="btn btn-lg btn-outline-primary btn-block" 
                                                disabled={withdrawMilestoneLoading} 
                                                onClick={() => withdrawFunds()}
                                            > 
                                            {
                                                withdrawMilestoneLoading ? 
                                                <div className="spinner-border" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>: 
                                                <span>Yes, Withdraws funds 🚀</span>
                                            }
                                            </button>
                                        </div>
                                    </div>
                        </div>
                    </React.Fragment>:null
                    }
                </>);
            default:
                return null;
        }
    }

    return (
        <div className="modal fade show" id="withdraw-funds" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        {withdrawalViewSelector(props.contract.payment_type)}
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    contract_id: PropTypes.string,
    contract: PropTypes.object.isRequired,
    updateContract: PropTypes.func.isRequired
}


export default withToastManager(Modal)