import React, {useState, useEffect} from "react"
import classnames from "classnames"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import PropTypes from "prop-types"
import {useMutation} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import {ADD_CONTRACT_CLIENT_ACCOUNT, RESOLVE_ACCOUNT} from "../../utils/queries"
import {formatCurrency} from "../../../../../utils/helpers"
import {withToastManager} from "react-toast-notifications"

const Modal = (props) => {

    const {toastManager} = props
    const [state, updateState] = useState({
        // contract: props.contract,
        // contract_id: props.contract.id,
        account_no: null,
        bank_code: null,
        bank_name: null,
        // bankcodes: props.bankcodes,
        account_name: ""
    })

    const [addContractClientAccount, {data: contractData, loading: addAccountLoading}] = useMutation(ADD_CONTRACT_CLIENT_ACCOUNT, { onCompleted: (data) => {
        // updateState({
        //     ...state,
        //     contract: data.addContractClientAccount
        // })
        window.$("#add-contract-account").modal("hide")
        toastManager.add("Successfully added account no🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
        props.updateContract(data.addContractClientAccount)
    }})

    const [resolveAccount, {loading: loadingResolveAccount}] = useMutation(RESOLVE_ACCOUNT)

    const queryToGetAccount = async () => {
        if(state.account_no && state.bank_code&& state.account_name == ""){
            try {
                let response = await resolveAccount({
                    variables:{
                        account_no: state.account_no,
                        account_bank: state.bank_code
                    }
                })
                if(response.data){
                    updateState({
                        ...state,
                        account_name: response.data.resolveBankAccount.account_name
                    })
                }
            } catch (e) {
                toastManager.add("Your bank account doesn't exist, check your entry",{
                    appearance: 'error',
                    autoDismiss: true
                } )
            }
        }
    }

    useEffect(() => {
        queryToGetAccount()
    }, [state])

    const addContractClientAccountCall = async () => {
        try {
            await addContractClientAccount({
                variables: {
                    contract_id: props.contract.id,
                    bank_code: state.bank_code,
                    account_no: state.account_no,
                    account_name: state.account_name,
                    bank_name: state.bank_name
                }
            })
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true,
            // })
        }
    }

    const updateDetails = (e) => {
        updateState({
            ...state,
            [e.target.name]: e.target.value
        })
    }

    return (
        <div className="modal fade show" id="add-contract-account" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        {
                            props.contract? 
                            <React.Fragment>
                                <div className="card-header">
                                    <div className="row">
                                        <div className="col">
                                            <h3 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                                Add your bank account
                                            </h3>
                                        </div>
                                        <div className="col-auto">
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    {
                                        props.bankcodes.length < 1 ? 
                                            <div className="col-md-12 mt-3 d-flex justify-content-center">
                                                <div className="spinner-border mt-5" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        :
                                        <>
                                            <div className="row">
                                                <div className="col-md-12 mt-3">
                                                    <label>
                                                        Select your bank
                                                    </label>
                                                    <select id="form_select" className="form-control" name="card_id" onChange={e => updateState({
                                                        ...state,
                                                        bank_code: e.target.value,
                                                        bank_name: e.target.dataset.bankName
                                                    })}>
                                                        <option> --SELECT YOUR BANK -- </option>
                                                        {   
                                                            props.bankcodes.map((bank, index) => {
                                                                return <option value={bank.bankcode} key={index} data-bank-name={bank.bankname}>{bank.bankname}</option>
                                                            })
                                                        }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-12 mt-3">
                                                    <label>
                                                        What's your account no
                                                    </label>
                                                    <input className="form-control" placeholder="Enter your account no" name="account_no" onChange={updateDetails}/>
                                                </div>
                                            </div>
                                            {
                                                (loadingResolveAccount) ?
                                                    <div className="row">
                                                       <div className="col-md-12 mt-3 d-flex justify-content-center">
                                                            <div className="spinner-border" role="status">
                                                                <span className="sr-only">Loading...</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                : null
                                            }
                                            <div className="row">
                                                <div className="col-md-12 mt-3">
                                                    <label>
                                                        What's your account name
                                                    </label>
                                                    <input disabled className="form-control" placeholder="Enter your account name" name="account_name" value={state.account_name}/>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-md-12 mt-3">
                                                    <button 
                                                        className="btn btn-lg btn-outline-primary btn-block" 
                                                        disabled={addAccountLoading || !(state.account_name && state.account_no && state.bank_code)} 
                                                        onClick={() => addContractClientAccountCall()}
                                                    > 
                                                    {
                                                        addAccountLoading ? 
                                                        <span>Processing...</span>: 
                                                        <span>Add your account no 🚀</span>
                                                    }
                                                    </button>
                                                </div>
                                            </div>
                                        </>
                                    }
                                </div>
                            </React.Fragment>
                            : null
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    contract_id: PropTypes.string,
    contract: PropTypes.object.isRequired,
    updateContract: PropTypes.func.isRequired
}


export default withToastManager(Modal)