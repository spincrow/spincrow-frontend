import React, {useState, useEffect} from "react"
import classnames from "classnames"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import PropTypes from "prop-types"
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import {useMutation, useQuery} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import {CARD_LIST, FUND_CONTRACT_AS_USER} from "../../utils/queries"
import {formatCurrency} from "../../../../../utils/helpers"
import {withToastManager} from "react-toast-notifications"

const Modal = (props) => {

    const {toastManager} = props
    const [state, updateState] = useState({
        contract: null,
        cards: [],
        card_id: null,
        amount: props.contract.amount,
        funding_type: "WALLET"
    })

    const [fundContractAsUser, {data: fundingResult, loading: fundingLoading}] = useMutation(FUND_CONTRACT_AS_USER, { onCompleted: (data) => {
        updateState({
            ...state,
            contract: data.fundContractAsUser
        })
        window.$("#funding-contract").modal("hide")
        toastManager.add("Successfully funded the contract🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
        props.updateContract(data.fundContractAsUser)
    }})

    const {data:cards, loading: cardLoading } = useQuery(CARD_LIST, {
        onCompleted: (data) => {
            // use the result
            updateState({
                ...state,
                cards: data.cards
            })
        }
    })

    useEffect(() => {
        updateState({
            ...state,
            contract: props.contract
        })
    }, [props.milestone])

    const fundContractAsUserCall = async () => {
        try {
            await fundContractAsUser({
                variables: {
                    contract_id: props.contract_id,
                    amount: state.amount,
                    funding_type: state.funding_type,
                    card_id: state.card_id
                }
            })
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true,
                
            // })
        }
    }

    return (
        <div className="modal fade show" id="funding-contract" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        {
                            props.contract? 
                            <React.Fragment>
                                <div className="card-header">
                                    <div className="row">
                                        <div className="col">
                                            <h3 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                                Fund Your Contract
                                            </h3>
                                        </div>
                                        <div className="col-auto">
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                                Contract Amount
                                            </small>
                                            <h2>
                                                {formatCurrency(props.contract.amount)}
                                            </h2>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>
                                                How do you wanna fund your contract?
                                            </label>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <button className={
                                                        classnames("btn btn-block", (state.funding_type == "WALLET" ? "btn-primary": "btn-light"))
                                                        }
                                                        onClick={() => {
                                                            updateState({
                                                                ...state,
                                                                funding_type: "WALLET"
                                                            })
                                                        }}
                                                    >WALLET</button>
                                                </div>
                                                <div className="col-md-6">
                                                    <button className={
                                                        classnames("btn btn-block", (state.funding_type == "CARD" ? "btn-primary": "btn-light"))
                                                        }
                                                        onClick={() => {
                                                            updateState({
                                                                ...state,
                                                                funding_type: "CARD"
                                                            })
                                                        }}
                                                    >CARD</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        {
                                            state.funding_type == "WALLET" ?
                                            null:
                                            <>
                                                {
                                                    cardLoading ? 
                                                        <div className="col-md-12 mt-3 d-flex justify-content-center">
                                                            <div className="spinner-border mt-5" role="status">
                                                                <span className="sr-only">Loading...</span>
                                                            </div>
                                                        </div>
                                                    :
                                                    <div className="col-md-12 mt-3">
                                                        <label>
                                                            Select your card
                                                        </label>
                                                        <select id="form_select" className="form-control" name="card_id" onChange={e => updateState({
                                                            ...state,
                                                            card_id: e.target.value
                                                        })}>
                                                            <option> --SELECT YOUR CARD -- </option>
                                                            {
                                                                state.cards.map((card, index) => <option value={card.id} key={index}>Card {index+1} (**** **** **** {card.last_number})</option>)
                                                            }
                                                        </select>
                                                    </div>
                                                }
                                            </>
                                        }
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12 mt-3">
                                            <button 
                                                className="btn btn-lg btn-outline-primary btn-block" 
                                                disabled={!((state.funding_type == "WALLET" && !fundingLoading) || (state.funding_type == "CARD" && state.card_id !== null && !fundingLoading))} 
                                                onClick={() => fundContractAsUserCall()}
                                            > 
                                            {
                                                fundingLoading ? 
                                                <div className="spinner-border" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>: 
                                                <span>Fund Contract 🚀</span>
                                            }
                                            
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                            : null
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    contract_id: PropTypes.string,
    contract: PropTypes.object.isRequired,
    updateContract: PropTypes.func.isRequired
}


export default withToastManager(Modal)