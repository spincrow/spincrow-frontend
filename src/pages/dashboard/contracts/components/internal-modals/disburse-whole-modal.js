import React, {useState, useEffect} from "react"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import {useMutation} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import {UPDATE_CONTRACT_MILESTONE, DISBURSE_WHOLE_CONTRACT} from "../../utils/queries"
import {formatCurrency} from "../../../../../utils/helpers"
import { withToastManager } from "react-toast-notifications"

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

const Modal = (props) => {

    const {user, toastManager} = props
    const [state, updateState] = useState({
        progress: 0,
        state: null,
        update: null
    })

    const [disburseWholeContract, {loading: disburseLoading}] = useMutation(DISBURSE_WHOLE_CONTRACT, { onCompleted: (data) => {
        updateState({
            ...state,
            update: data.disburseContractWholePayment
        })
        toastManager.add("Funds disbursed 🎉 ", {
            appearance: 'success',
            autoDismiss: true,
            
        })
        window.$("#disburse-contract").modal("hide")
        props.updateContract(data.disburseContractWholePayment)
    }})

    useEffect(() => {
        updateState({
            ...state,
            progress: props.milestone ? props.milestone.progress : 0,
            state: props.milestone ? props.milestone.state : "INCOMPLETE"
        })
    }, [props.milestone])

    const disburseWholeContractCall = async () => {
        // props.disburseWholeContractCall(state)
        
        try {
            await disburseWholeContract({variables: {
                contract_id: props.contract_id
            }})
        } catch (e) {
            // toastManager.add(e.message, {
            //     appearance: 'error',
            //     autoDismiss: true,
                
            // })
        }
    }

    return (
        <div className="modal fade show" id="disburse-contract" role="dialog" aria-modal="true">
            {
                props.contract ? 
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-card card" data-toggle="lists" >
                            <React.Fragment>
                                <div className="card-header">
                                    <div className="row">
                                        <div className="col">
                                            <h3 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                                Disburse Contract's Funds
                                            </h3>
                                        </div>
                                        <div className="col-auto">
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                                Amount
                                            </small>
                                            <h2>
                                                {formatCurrency(props.contract.amount)}
                                            </h2>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <div className="row">
                                        <div className="col">
                                            <button 
                                                className="btn btn-outline-primary btn-block" 
                                                disabled={disburseLoading} 
                                                onClick={disburseWholeContractCall}
                                            >
                                                Disburse Contract Funds 🚀
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                        </div>
                    </div>
                </div>: null
            }
        </div>
    )
}

export default withToastManager(Modal)