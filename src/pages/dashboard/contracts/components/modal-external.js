import React, {useState, useEffect} from "react"
import 'rc-slider/assets/index.css';
import 'rc-tooltip/assets/bootstrap.css';
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import {useMutation} from "@apollo/react-hooks"
import 'rc-slider/assets/index.css';
import PropTypes from 'prop-types'
import {UPDATE_CONTRACT_MILESTONE} from "../utils/queries"
import {formatCurrency} from "../../../../utils/helpers"

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

const handle = (props) => {
    const { value, dragging, index, ...restProps } = props;
    // 
    return (
      <Tooltip
        prefixCls="rc-slider-tooltip"
        overlay={value}
        visible={dragging}
        placement="top"
        key={index}
      >
        <Handle value={value} {...restProps} />
      </Tooltip>
    );
  };

const Modal = (props) => {

    const [state, updateState] = useState({
        progress: 0
    })

    const [updateMilestoneMutation, {data}] = useMutation(UPDATE_CONTRACT_MILESTONE, { onCompleted: (data) => {
        updateState({
            ...state,
            update: data.updateContractMilestone
        })
        props.updateMilestone(data.updateContractMilestone)
    }})

    useEffect(() => {
        updateState({
            ...state,
            progress: props.milestone ? props.milestone.progress : 0,
            state: props.milestone ? props.milestone.state : "INCOMPLETE"
        })
    }, [props.milestone])

    const updateMilestone = async () => {
        // props.updateMilestone(state)
        
        await updateMilestoneMutation({variables: {
            contract_id: props.contract_id,
            milestone_id: props.milestone.id,
            progress: state.progress,
            state: state.state
        }})
    }

    return (
        <div className="modal fade show" id="update-milestone" role="dialog" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-card card" data-toggle="lists" >
                        {
                            props.milestone ? 
                            <React.Fragment>
                                <div className="card-header">
                                    <div className="row">
                                        <div className="col">
                                            <h4 className="card-header-title mr-auto" id="exampleModalCenterTitle">
                                                Update Milestone
                                            </h4>
                                        </div>
                                        <div className="col-auto">
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <small className="header-pretitle">
                                                Target
                                            </small>
                                            <h2>
                                                {formatCurrency(props.milestone.target)}
                                            </h2>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <button className="btn mx-2 btn-sm btn-success btn-block" data-dismiss="modal" aria-label="Close" onClick={() => props.disburseFundsForMilestone(props.milestone)}> Disburse funds</button>
                                        </div>
                                        <div className="col-md-6">
                                            <button className="btn mx-2 btn-sm btn-danger btn-block">Request retraction</button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                            : null
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    contract_id: PropTypes.string,
    milestones: PropTypes.object.isRequired,
    disburseFundsForMilestone: PropTypes.func.isRequired
}

export default Modal