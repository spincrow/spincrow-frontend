import React from "react"
import {Link} from "react-router-dom"
import {formatCurrency, percentageOfSuccess} from "../../../../utils/helpers"
import classnames from "classnames"
import moment from "moment"

const ContractView = ({contract, contractAmount}) => {
    return <div className="col-12 col-md-4">
            <Link to={`/contracts/${contract.id}`} >
                <div className="card zoom">
                    <div className="card-header border-0">
                        <div className="row">
                            <div className="col">
                                <div className="card-header-title">
                                    <h3>{contract.name}</h3>
                                 </div>
                            </div>
                            <div className="col-auto">
                                <span className="badge badge-soft-warning">{contract.owner_type}</span>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col">
                                <div className="row">
                                    <div className="col d-inline-flex">
                                            <h1 className="font-weight-bolder">{formatCurrency( contractAmount)}
                                            </h1>
                                            {/* <span className="">.00</span> */}
                                    </div>
                                    {
                                        contract.payment_type == "whole" ? null :<div className="col-auto">
                                            <small className="text-secondary">{contract.milestones.length} milestones</small>
                                        </div>
                                    }
                                </div>
                                {
                                    contract.payment_type == "whole" ? null : 
                                        <div className="progress progress-sm">
                                            <div className="progress-bar bg-success" role="progressbar" style={{width: `${percentageOfSuccess(contract.milestones)}%`}} aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="card-footer">
                        <div className="row">
                            <div className="col">
                                <span className="h4">{contract.client}</span>
                            </div>
                            <div className="col-auto">
                                <span className={classnames("badge", contract.status == "INACTIVE" ? "badge-soft-warning": "badge-soft-success")}>{contract.status}</span>
                                {/* <span className="text-center">{moment(contract.created_at).format("DD MMM, YYYY")}</span> */}
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
</div>
}

export default ContractView