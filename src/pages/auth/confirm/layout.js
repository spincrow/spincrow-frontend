import React, {Component, useState} from "react"
import {Link, useHistory} from "react-router-dom"
import Sidebar from "../../../components/sidebar"
import {useMutation} from "@apollo/react-hooks"
import {withToastManager} from "react-toast-notifications"
import "./confirm.scss"
import CodeInput from "../components/code-input"

const Layout = (props) =>{
    
    const {toastManager, setRedirectUrl, login, autoLogin, RESEND_CODE} = props
    setRedirectUrl('emailconfirmation')
    let history = useHistory()
    const [confirmEmailCode, {loading: emailLoading}] = useMutation(props.CONFIRM_CODE, {onCompleted: async (data) => {
        // if(data.confirmEmailCode.state == true){
        // }else{
        //     toastManager.add("Code is incorrect", {
        //         appearance: "error"
        //     })
        // }
        await login({user: data.confirmEmailCode})
        toastManager.add("Code matches successfully", {
            appearance: "success"
        })
        
        history.push("/addphone")
    }})

    const [resendCodeMutation, {loading: resendLoading}] = useMutation(RESEND_CODE, {onCompleted: async (data) => {
        if(data.resendEmailVerificationCode.state){
            toastManager.add("Code successfully resent", {
                appearance: "success"
            })
        }
    }})




    const [state, updateState] = useState({
        agencies: [],
        passwordVisible: false,
        params: {},
        loading: false,
        links: [
            {
                anchor: "/signup",
                name: "Enter personal information"
            },
            {
                anchor: "/emailconfirmation",
                active: true,
                name: "Verify email"
            },
            {
                anchor: "/addphone",
                name: "Add phone number"
            },
            {
                anchor: "/phoneconfirmation",
                name: "Verify phone number"
            },
        ]
    })

    const updateConfirmCode = (e) => {
        updateState({
            ...state,
            params: {
                code: e
            }
        })
    }

    const confirmCode = async () => {
        await confirmEmailCode({
            variables: {
                code: state.params.code
            }
        })
    }

    const skip =() =>{
        history.push("/addphone")
    }


    return (
        <div className="container-fluid">
            <div className="row full-height bg-body-alt">
                <Sidebar image={require('../../../assets/img/header-email.svg')} links={ state.links} />
                <div className="col-md-8 col-12 h-100">
                    <div className="container-fluid pt-5 ">
                        <div className="row mt-5 justify-content-center">
                            <div className="px-md-auto col-md-7 col-12 mt-5">
                                    <div className="row mt-5">
                                        <div className="col-12">
                                            <h1 className="display-4">Verify email</h1>
                                            <p className="text-secondary">Kindly enter the code sent to <span className="text-primary"> { props.currentUser.email} </span> to verify your account</p>
                                            <p className="text-secondary"><a className="text-primary text-underline pointer" onClick={() => resendCodeMutation()}> Click here to resend the code</a></p>
                                        </div>
                                    </div>
                            </div>
                            <div className="px-md-auto col-md-7 col-12">
                                    <div className="row mt-4 mb-4">
                                            <div className="col-12">
                                                <CodeInput onChange={ updateConfirmCode} type="number" fields={6} className="row justify-content-center" inputClassName="confirm-input form-control"/>
                                            </div>
                                    </div>
                                    <div className="row mt-4 mb-4">
                                            <div className="col-auto">
                                                <button className="btn btn-light" onClick={skip}>Skip</button>
                                            </div>
                                            <div className="col">
                                                
                                            </div>
                                            <div className="col-auto">
                                                <button className="btn btn-primary btn-block" disabled={emailLoading} onClick={confirmCode}>
                                                    <span>Verify</span>
                                                    {
                                                        emailLoading ? <div className="spinner-grow" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>: null
                                                    }
                                                </button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withToastManager(Layout)