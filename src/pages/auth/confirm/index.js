import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import gql from 'graphql-tag'
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import {setRedirectUrl, autoLogin, login} from "../../../store/Actions/Auth"

const CONFIRM_CODE = gql`
    mutation confirmCode($code:String!){
        confirmEmailCode(input:{
            code:$code
        }){
            first_name
            last_name
            profile_image
            phone
            email
            wallet {
                id
                amount
            }
            home_tour
            email_verified
            contract_tour
            single_contract_tour
            settings_tour
            wallet_tour
        }
    }
`;

const RESEND_CODE = gql`
mutation resendEmailVerificationCode{
    resendEmailVerificationCode{
        state
    }
}
`;

const mapStateToProps = (state => {
    return {
        currentUser: state.currentUser,
        CONFIRM_CODE,
        RESEND_CODE
    }
});
const mapDispatchToProps = {setRedirectUrl, login, autoLogin}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))