import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag'
import {autoLogin, login} from "../../../store/Actions/Auth"


const FORGOT_PASSWORD_ENTER_MAIL = gql`
    mutation forgotPasswordEnterEmail($email:String) {
        forgotPasswordEnterEmail (input: {email: $email})
    }
`;


const mapStateToProps = (state) => ({
    forgotPasswordEnterEmailMutation: FORGOT_PASSWORD_ENTER_MAIL,
    redirectUrl: state.redirectUrl
})

const mapDispatchToProps = {
    autoLogin: autoLogin,
    login: login
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))