import React, {Component, useState} from "react"
import {useHistory, Link} from "react-router-dom"
import {useMutation} from "@apollo/react-hooks"
import cookies from "js-cookie"


const Layout = (props) => {

    const [state, updateState ] = useState({
        agencies: [],
        passwordVisible: false,
        params: {},
        loading: false
    })

    const [forgotPasswordEnterEmail, { data, loading }] = useMutation(props.forgotPasswordEnterEmailMutation);
    let history = useHistory()

    const sendEmailConfirmation = async () => {
        const { toastManager, login} = props
        try {
            let resp = await forgotPasswordEnterEmail({ variables: { ...state.params } });
            if(resp){
                if(resp.data.forgotPasswordEnterEmail){
                    toastManager.add("Password reset email has been sent, Check your email to confirm", {
                        appearance: 'success',
                        autoDismiss: false
                    })
                }else{
                    toastManager.add("The user you entered doesn't exist in our platform", {
                        appearance: 'error',
                        autoDismiss: true
                    })
                }
            }
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true
            // })
            
        }
    }

    const onChange = (e) => {
        let target = e.target
        updateState({
            ...state,
            params: {
                ...state.params,
                [target.name] : target.value
            }
        })
    }
    
    return(
        <div className="container-fluid">
            <div className="row full-height">
                <div className="col-4 d-md-block d-none  bg-light">
                    <div className="container">
                        <div className="row mt-4">
                            <div className="col-12 mb-auto d-flex">
                                <img className="mx-auto" src={require('../../../assets/img/logo.svg')}/>
                            </div>
                        </div>
                        <div className="mt-3 row"></div>
                        <div className="row mb-5" style={{marginTop: 150}}>
                            <div className="col-12 mb-auto d-flex justify-content-center">
                                <img src={require('../../../assets/img/header-profile.svg')} className="d-block"/>
                        </div>
                            <div className="col-12 mb-auto mt-4 text-center">
                                <span className="font-weight-bolder">Reset your password</span>
                        </div>
                        </div>
                        <div className="mt-3 row"></div>
                    </div>
                </div>
                <div className="col-md-8 col-12 h-100">
                    <div className="container-fluid">
                        <div className="row  justify-content-center" style={{marginTop: 100}}>
                            <div className="px-md-auto col-md-6 col-12">
                                    <div className="row mt-4 d-block d-md-none">
                                        <div className="col-12 mb-auto d-flex">
                                            <img className="" height={50} src={require('../../../assets/img/logo.svg')}/>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-12">
                                            <h1 className="display-4">Forgot your password </h1>
                                            <p>Let's help you reset your password</p>
                                        </div>
                                    </div>
                            </div> 
                        </div>
                        <div className="row justify-content-center">
                            <div className="px-md-auto col-md-6 col-12">
                                    <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label>Email</label>
                                                    <input onChange={onChange} name="email" className="form-control"/>
                                                </div>
                                            </div>
                                    </div>
                                    {/* <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <div className="input-group input-group-merge mb-3">
                                                        <input onChange={onChange} name="password" type={state.passwordVisible? "text": "password"} className="form-control form-control-appended" placeholder="Input group appended"/>
                                                        <div  className="input-group-append">
                                                            <div className="input-group-text">
                                                                <span onClick={() => updateState({...state, passwordVisible: ! state.passwordVisible})}  className="fe fe-lock"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div className="row">
                                            <div className="col-12 text-right">
                                                <Link to="sendEmailConfirmation" className="text-underline">Forgot Password?</Link>
                                            </div>
                                    </div> */}
                                    <div className="row">
                                            <div className="col-12 text-right">
                                                <Link to="/login" className="text-underline">Back to Login ?</Link>
                                            </div>
                                    </div>
                                    <div className="row mt-4">
                                    <div className="col-12">
                                            <button disabled={loading} className="btn btn-primary btn-block" onClick={sendEmailConfirmation}>
                                                {
                                                    loading ? <div className="spinner-grow" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>: 
                                                    'Request Email Confirmation'
                                                }
                                                </button>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Layout;