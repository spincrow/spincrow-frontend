import React, {Component, useState} from "react"
import {Link, useHistory, useParams, useLocation} from "react-router-dom"
import {useMutation} from "@apollo/react-hooks"
import cookies from "js-cookie"

function useQuery() {
    return new URLSearchParams(useLocation().search);
}
  

const Layout = (props) => {

    let query = useQuery()
    const [state, updateState ] = useState({
        params: {
            newpassword: null,
            newpassword_confirm: null
        }
    })

    const [resetPassword, { data, loading }] = useMutation(props.resetPasswordMutation);
    let history = useHistory()

    const resetPasswordCall = async () => {
        const { toastManager, login} = props
        try {
            if(state.params.newpassword !== state.params.newpassword_confirm){
                throw new Error("Your password and confirmation doesn't match")
            }
            let resp = await resetPassword({ variables: { 
                newpassword: state.params.newpassword, 
                verification_code: query.get("resetcode")
            }});
            if(resp.data.updateForgottenPassword){
                toastManager.add("Your password has been reset successfully", {
                    appearance: 'success',
                    autoDismiss: true
                })
                history.push("login")
            }else{
                toastManager.add("Something went wrong", {
                    appearance: 'error',
                    autoDismiss: true
                })
            }
        }catch(err){
            // toastManager.add(err.message, {
            //     appearance: 'error',
            //     autoDismiss: true
            // })
        }
    }

    const onChange = (e) => {
        let target = e.target
        updateState({
            ...state,
            params: {
                ...state.params,
                [target.name] : target.value
            }
        })
    }
    
    return(
        <div className="container-fluid">
            <div className="row full-height">
                <div className="col-4 d-md-block d-none  bg-light">
                    <div className="container">
                        <div className="row mt-4">
                            <div className="col-12 mb-auto d-flex">
                                <img className="mx-auto" src={require('../../../assets/img/logo.svg')}/>
                            </div>
                        </div>
                        <div className="mt-3 row"></div>
                        <div className="row mb-5" style={{marginTop: 150}}>
                            <div className="col-12 mb-auto d-flex justify-content-center">
                                <img src={require('../../../assets/img/header-profile.svg')} className="d-block"/>
                        </div>
                            <div className="col-12 mb-auto mt-4 text-center">
                                <span className="font-weight-bolder">Reset your password</span>
                        </div>
                        </div>
                        <div className="mt-3 row"></div>
                    </div>
                </div>
                <div className="col-md-8 col-12 h-100">
                    <div className="container-fluid">
                        <div className="row  justify-content-center" style={{marginTop: 100}}>
                            <div className="px-md-auto col-md-6 col-12">
                                    <div className="row mt-5">
                                        <div className="col-12">
                                            <h1 className="display-4">Reset password</h1>
                                            <p>let's reset your password</p>
                                        </div>
                                    </div>
                            </div> 
                        </div>
                        <div className="row justify-content-center">
                            <div className="px-md-auto col-md-6 col-12">
                                    <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label>New Password</label>
                                                    <input onChange={onChange} type="password" name="newpassword" className="form-control"/>
                                                </div>
                                            </div>
                                    </div>
                                    <div className="row">
                                            <div className="col-12">
                                                <div className="form-group">
                                                    <label>Confirm password</label>
                                                    <input onChange={onChange} type="password" name="newpassword_confirm" className="form-control"/>
                                                </div>
                                            </div>
                                    </div>
                                    <div className="row mt-4">
                                        <div className="col-12">
                                                <button disabled={(state.params.newpassword !== state.params.newpassword_confirm)|| loading} className="btn btn-primary btn-block" onClick={resetPasswordCall}>
                                                    {
                                                        loading ? <div className="spinner-grow" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>: 
                                                        'Reset your password'
                                                    }
                                                    </button>
                                                </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Layout;