import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag'
import {autoLogin, login} from "../../../store/Actions/Auth"


const RESET_PASSWORD = gql`
    mutation updateForgottenPassword($newpassword:String, $verification_code: String) {
        updateForgottenPassword (input: {newpassword: $newpassword, verification_code: $verification_code}) 
    }
`;


const FORGOT_PASSWORD_ENTER_MAIL = gql`
    mutation forgotPasswordEnterEmail($email:String) {
        forgotPasswordEnterEmail (input: {email: $email})
    }
`;


const mapStateToProps = (state) => ({
    resetPasswordMutation: RESET_PASSWORD,
    forgotPasswordEnterEmailMutation: FORGOT_PASSWORD_ENTER_MAIL,
    redirectUrl: state.redirectUrl
})

const mapDispatchToProps = {
    autoLogin: autoLogin,
    login: login
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))