import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag';
import {autoLogin, login, setRedirectUrl} from "../../../store/Actions/Auth"


const SIGN_UP = gql`
    mutation signupUser($first_name: String!, $last_name: String!, $email: String!, $password: String!, $phone: String) {
        signup(input: {first_name: $first_name, last_name: $last_name, email: $email, password: $password, phone: $phone}){
            token
            user {
                first_name
                last_name
                email
                phone
                profile_image
                wallet {
                    amount
                    id
                }
                home_tour
                contract_tour
                email_verified
                single_contract_tour
                settings_tour
                wallet_tour
            }
        }
    }
`;

const mapStateToProps = (state => {
    return {
        signupMutations: SIGN_UP
    }
});
const mapDispatchToProps = {
    login,
    setRedirectUrl
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))