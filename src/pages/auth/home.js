import React, {Component} from "react"
import {Link} from "react-router-dom"

export default class Login extends Component {
    render(){
        return(
            <div className="container">
                <div className="row justify-content-center mt-5">
                    <div className="col-md-7 col-sm-12 mt-5">
                        <div className="row">
                            <div className="col-12">
                                <div className="header">
                                    <div className="header-body text-center">
                                        <h1 className="header-title">
                                        Welcome to Spincrow
                                        </h1>
                                        <h5 className="header-subtitle">
                                        Take a tour
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 text-center">
                               <Link className="text-center" to="/signup">
                               <button className="btn btn-primary text-center">Take a Tour <i className="fe fe-arrow-right"></i></button>
                               </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}