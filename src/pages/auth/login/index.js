import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import gql from 'graphql-tag'
import {autoLogin, login} from "../../../store/Actions/Auth"


const SIGN_IN = gql`
    mutation loginStructure($email:String!, $password:String!) {
        login (input: {email: $email, password: $password}) {
            token 
            user {
                first_name
                last_name
                profile_image
                phone
                email
                wallet {
                    id
                    amount
                }
                home_tour
                email_verified
                contract_tour
                single_contract_tour
                settings_tour
                wallet_tour
            }
        }
    }
`;


const mapStateToProps = (state) => ({
    signInMutations: SIGN_IN,
    redirectUrl: state.redirectUrl
})

const mapDispatchToProps = {
    autoLogin: autoLogin,
    login: login
}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))