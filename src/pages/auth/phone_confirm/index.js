import React, {Component} from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Layout from "./layout"
import { withToastManager } from 'react-toast-notifications';
import {setRedirectUrl} from "../../../store/Actions/Auth"

const mapStateToProps = (state => {
    return {}
});
const mapDispatchToProps = {setRedirectUrl}

export default withToastManager(
    withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout)
))