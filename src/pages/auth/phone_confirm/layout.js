import React, {Component, useState, useEffect} from "react"
import {Link, useHistory, withRouter} from "react-router-dom"
 import {withToastManager} from "react-toast-notifications"
 import {useMutation} from "@apollo/react-hooks"
import Sidebar from "../../../components/sidebar"
import "./confirm.scss"
import CodeInput from "../components/code-input"
import gql from "graphql-tag"
import {useQuery} from "../../../utils/helpers"

const Layout = (props) => {

    let query = useQuery()
    let phone = query.get("phone")
    
    const {toastManager, setRedirectUrl} = props;

    setRedirectUrl('phoneconfirmation')
    const [state, updateState] = useState({
        params: {},
        loading: false,
        links: [
            {
                anchor: "/signup",
                name: "Enter personal information"
            },
            {
                anchor: "/emailconfirmation",
                name: "Verify email"
            },
            {
                anchor: "/addphone",
                name: "Add phone number"
            },
            {
                active: true,
                anchor: "/phoneconfirmation",
                name: "Verify phone number"
            },
        ]
    })
    let history = useHistory()
    const [verifyCode, {loading: verifyLoading}] = useMutation(gql`
        mutation ($code:String!){
            verifySMSCode(input:{
                code:$code
            }){
                state
            }
        }
    `, {onCompleted: (data) => {
        
        if(data.verifySMSCode.state == true){
            toastManager.add("Code matches successfully", {
                appearance: "success"
            })
            history.push("/home")
        }else{
            toastManager.add("Code is incorrect", {
                appearance: "error"
            })
        }
    }})


    const updateConfirmCode = (e) => {
        updateState({
            ...state,
            params: {
                code: e
            }
        })
    }

    const confirmCode = async () => {
        await verifyCode({
            variables: {
                code: state.params.code
            }
        })
    }

    const skip =() =>{
        history.push("/home")
    }


    

    return (
        <div className="container-fluid">
            <div className="row full-height bg-body-alt">
                <Sidebar image={require('../../../assets/img/header-phone-verify.svg')} links={state.links} />
                <div className="col-md-8 col-12 h-100">
                    <div className="container-fluid pt-5">
                        <div className="row mt-5 justify-content-center">
                        <div className="px-md-auto col-md-7 col-12 mt-5">
                                <div className="row mt-5">
                                    <div className="col-12">
                                        <h1 className="display-4">Verify phone number</h1>
                                        <p className="text-secondary">Kindly enter the code sent to <span className="text-primary">+{phone}</span> to verify your account</p>
                                    </div>
                                </div>
                        </div>
                        <div className="px-md-auto col-md-7 col-12">
                                <div className="row mt-4 mb-4">
                                        <div className="col-12">
                                            <CodeInput onChange={ updateConfirmCode} type="number" fields={6} className="row justify-content-center" inputClassName="confirm-input form-control"/>
                                        </div>
                                </div>
                                <div className="row mt-4 mb-4">
                                        <div className="col-auto">
                                            <button className="btn btn-light" onClick={skip}>Skip</button>
                                        </div>
                                        <div className="col">
                                            
                                        </div>
                                        <div className="col-auto">
                                            <button className="btn btn-primary btn-block" disabled={verifyLoading} onClick={confirmCode} >
                                                {
                                                    verifyLoading ? <div className="spinner-grow" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>: <span>Verify</span>
                                                }
                                            </button>
                                        </div>
                                </div>
                        </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withToastManager(withRouter(Layout))