import React, {Component, useState, useEffect} from "react"
import {Link, useHistory} from "react-router-dom"
import Sidebar from "../../../components/sidebar"
import PhoneInput from 'react-phone-input-2'
import {useMutation} from "@apollo/react-hooks"
// import '../../../assets/styles/phone-input.css'
import gql from "graphql-tag"
import { withToastManager } from "react-toast-notifications"
// import {addToast} from "react-toast-notifications"
// import "./confirm.scss"

const Layout = (props) => {
    
    const [state, updateState] = useState({
        params: {},
        loading: false,
        links: [
            {
                anchor: "/signup",
                name: "Enter personal information"
            },
            {
                anchor: "/emailconfirmation",
                name: "Verify email"
            },
            {
                anchor: "/addphone",
                active: true,
                name: "Add phone number"
            },
            {
                anchor: "/phoneconfirmation",
                name: "Verify phone number"
            },
        ]
    })

    const {toastManager,setRedirectUrl} = props
    
    setRedirectUrl('addphone')
    let history = useHistory()

    const [sendPhoneMutation, {loading: sendPhoneLoading}] = useMutation(gql`mutation ($phone: String) {
        sendPhoneConfirmation(input:$phone){
          state
        }
      }`, {onCompleted: async () => {
          toastManager.add("Check your mobile for SMS Code", {
            appearance: "success"
        })
          history.push(`/phoneconfirmation?phone=${state.params.phone}`)
      }})

    const confirmCode = async () => {
        await sendPhoneMutation({
            variables: {
                phone: `+${state.params.phone}`
            }
        })
    }

    const skip =() =>{
        history.push("/phoneconfirmation")
    }

    return(
        <div className="container-fluid">
            <div className="row full-height bg-body-alt">
                <Sidebar image={require('../../../assets/img/header-phone.svg')} links={state.links} />
                <div className="col-md-8 col-12 h-100">
                    <div className="container-fluid pt-5">
                        <div className="row mt-5 justify-content-center">
                        <div className="px-md-auto col-md-7 col-12 mt-5">
                                <div className="row mt-5">
                                    <div className="col-12">
                                        <h1 className="display-4">Add phone</h1>
                                        <p className="text-secondary">Please enter your phone number to complete your registration</p>
                                    </div>
                                </div>
                        </div>
                        <div className="px-md-auto col-md-7 col-12">
                                <div className="row mt-4 mb-4">
                                        <div className="col-12">
                                            <div className="row flex-row justify-content-center">
                                                <div className="col-12">
                                                    <PhoneInput
                                                        country={'ng'}
                                                        inputStyle={{width: '100%'}}
                                                        value={state.params.phone}
                                                        onChange={phone => updateState({ ...state, params: {
                                                            ...state.params,
                                                            phone: phone
                                                        } })}
                                                    />
                                                </div>
                                                {/* <div className="col-3">
                                                    <div className="form-group">
                                                        <label>Country</label>
                                                        <input type="text" className="form-control" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                                    </div>
                                                </div>
                                                <div className="col-9">
                                                    <div className="form-group">
                                                        <label>Phone number</label>
                                                        <input type="text" className="form-control" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                                    </div>
                                                </div>                                              */}
                                            </div>
                                        </div>
                                </div>
                                <div className="row mt-4 mb-4">
                                        <div className="col-auto">
                                            <button className="btn btn-light" onClick={skip}>Skip</button>
                                        </div>
                                        <div className="col">
                                            
                                        </div>
                                        <div className="col-auto">
                                            <button className="btn btn-primary" onClick={confirmCode} disabled={sendPhoneLoading}>
                                                <span>Verify </span>
                                                {
                                                    sendPhoneLoading ? <div className="spinner-grow" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div> : null
                                                }
                                            </button>
                                        </div>
                                </div>
                        </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default withToastManager(Layout)