import Signup from "./auth/signup"
import Confirm from "./auth/confirm"
import Addphone from "./auth/phone"
import Index from "./auth/home"
import PhoneConfirm from "./auth/phone_confirm"
import Login from "./auth/login"
import Home from "./dashboard/home"
import Wallet from "./dashboard/wallet"
import Contract from "./dashboard/contracts"
import SingleContract from "./dashboard/contracts/single"
import NewContract from "./dashboard/contracts/new_contract"
import Settings from "./dashboard/settings"
import ExternalSingleContract from "./dashboard/contracts/single-external"
import FundContract from "./dashboard/contracts/fund-contract"

import ForgotPassword from "./auth/forgot_password"
import ResetPassword from "./auth/reset_password"

// ONboarding pages
import OnboardingAddBank from "./onboarding/add-account"
import OnboardingAddCard from "./onboarding/add-card"
import OnboardingCreateContract from "./onboarding/add-contract"
import NoMatch from "./404"
// import Login from "./auth/login"
// import Onboarding from "./onboarding"
// import Dashboard from "./dashboard/home"
// import Settings from "./dashboard/settings"
// import Reports from "./dashboard/reports"

// export {Signup, Login, Onboarding, Dashboard, Index, Settings, Reports}
export {
    NoMatch,
    Signup, 
    Wallet,  
    Index, 
    Confirm, 
    Addphone, 
    PhoneConfirm, 
    Login, 
    Home, 
    Contract, 
    NewContract, 
    FundContract,
    Settings, 
    SingleContract, 
    ExternalSingleContract,
    ForgotPassword,
    ResetPassword,
    OnboardingAddBank,
    OnboardingAddCard,
    OnboardingCreateContract
}