// import scenes here and make routes of them
import React from 'react';
import {Route, Redirect, useHistory, Switch} from 'react-router-dom';
import {
	Signup, 
	Index, 
	Confirm, 
	Addphone, 
	PhoneConfirm, 
	Login, 
	Home, 
	Contract, 
	NewContract,
	FundContract,
	Wallet,
	SingleContract,
	Settings,
	ExternalSingleContract,
	OnboardingAddBank,
	OnboardingAddCard,
	OnboardingCreateContract,
	ForgotPassword,
	ResetPassword,
	NoMatch
} from "../pages"
import Header from "../components/header"
import ExternalHeader from "../components/external-header"
import AppCheck from './app-check';
import EnsureLoggedInContainer from './ensure-loggedIn-container';
import EnsureVisitorOnlyContainer from './ensure-visitoronly-conainer';
import EnsureEventLogInContainer from './ensure-event-login-container';

import { onError } from "apollo-link-error";
import { ToastProvider, useToasts } from 'react-toast-notifications'
import { ApolloClient } from 'apollo-client';
import {InMemoryCache, ObjectCache} from "apollo-boost";
import { ApolloProvider} from '@apollo/react-hooks';
import { HttpLink} from 'apollo-link-http'
import {ApolloLink , concat} from "apollo-link"
import cookies from "js-cookie"
import {createUploadLink} from "apollo-upload-client"


const externalPages = [
	'/',
	'/login',
	'/signup',
	'/forgotpassword',
	'/resetpassword',
	'/client/:id'
];

const internalPages = [
	'/emailconfirmation',
	'/addphone',
	'/phoneconfirmation',
	'/wallet',

	'/onboarding/addbank',
	'/onboarding/addcard',
	'/onboarding/create-contract',

	'/wallet/withdrawal',
	'/wallet/topup',
	'/home',
	'/contracts',
	'/contracts/new',
	'/settings'
];

const Routes = () => {

	const {addToast} = useToasts();
	let history = useHistory()
  
	const errorLink = onError(({ graphQLErrors, networkError }) => {
	  if (graphQLErrors)
		graphQLErrors.map(({ message, locations, path }) => {
			if (message.includes("Not Authorised")) {
				addToast(message, {
					appearance: 'error',
					autoDismiss: true
				})
				setTimeout(() => {
					cookies.remove('uat')
					history.push("/login")
				},500)
			} else {
				addToast(message, {
					appearance: 'error',
					autoDismiss: true
				})
				// snackbarStore.dispatch.snackbar.handleOpen(message);
			}
	  });
	  if (networkError) {
		addToast("Network error, Make sure you have internet access", {
			appearance: 'warning',
			autoDismiss: true
		})
	  }
	});
  
	const authMiddleware = new ApolloLink((operation, forward) => {
	  operation.setContext({
		headers: {
		  authorization: `Bearer ${cookies.get('uat')}`
		}
	  })
	
	  return forward(operation)
	})
  
	const link = createUploadLink({
	  uri: process.env.REACT_APP_GRAPHQL_API
	})
  
	const linker = ApolloLink.from([
	  authMiddleware,
	  errorLink,
	  link
	])

	const client = new ApolloClient({
		cache: new InMemoryCache(),
		// cache: new ObjectCache(),
		link: concat(authMiddleware, linker)
	});
	// const externalClient = new ApolloClient({
	// 	cache: new InMemoryCache(),
	// 	// cache: new ObjectCache(),
	// 	link: link
	// });
	// import 'react-dates/initialize';
	
	return(
		<React.Fragment>
			{/* <ApolloProvider client={externalClient}>
			</ApolloProvider> */}
			<ApolloProvider client={client}>
				<>
					<AppCheck>
						{/* <Header /> */}
						
						<EnsureVisitorOnlyContainer pages={externalPages}>
							<Switch>
								{/* <Redirect path="/" to="/login"/> */}
								<Route exact path="/" render={()=>(
								 <Redirect to="/login" />        
								)} />
								<Route  path="/login" component={Login} />
								<Route  path="/signup" component={Signup} />
								<Route  path="/forgotpassword" component={ForgotPassword} />
								<Route  path="/resetpassword" component={ResetPassword} />
								<Route exact path="/client/:id" component={ExternalSingleContract}></Route>
								<Route render={() => (
									<>
										<ExternalHeader />
										<NoMatch path="/login"/>
									</>
								)} />
							</Switch>
						</EnsureVisitorOnlyContainer>

						<EnsureLoggedInContainer pages={internalPages}>
							<Header />
							<Switch>
								<Route exact path="/onboarding/addbank" component={OnboardingAddBank}/>
								<Route exact path="/onboarding/addcard" component={OnboardingAddCard}/>
								<Route path="/onboarding/create-contract" component={OnboardingCreateContract}/>

								<Route exact path="/emailconfirmation" component={Confirm} />
								<Route exact path="/addphone" component={Addphone} />
								<Route exact path="/phoneconfirmation" component={PhoneConfirm} />
								<Route exact path="/home" component={Home} />
								<Route path="/contracts/fund-contract/:id" component={FundContract}></Route>
								<Route path="/contracts/new" component={NewContract} />
								<Route exact path="/contracts/:id" component={SingleContract} />

								{/* <Switch>
								</Switch> */}
								<Route path="/wallet" component={Wallet} />
								<Route path="/settings" component={Settings} />
								<Route exact path="/contracts" component={Contract} />
								<Route render={() => <NoMatch path="/home"/> }/>
							</Switch>
						</EnsureLoggedInContainer>

					</AppCheck>
				</>
			</ApolloProvider>
		</React.Fragment>
	)
};

export default Routes;